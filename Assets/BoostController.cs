﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoostController : MonoBehaviour
{
    [SerializeField] private Text boostNum;
    [SerializeField] private Text boostTime;
    [SerializeField] private int boostId;

    private Timer timer;
    private bool IsChecked = true;

    private void OnEnable()
    {
        DataCenter.onBoostTimeChanged += UpdateInfo;
    }

    private void OnDisable()
    {
        DataCenter.onBoostTimeChanged -= UpdateInfo;
    }

    void Start()
    {
        UpdateInfo();
    }

    public void UpdateInfo()
    {
        Debug.Log("HI boost id         -   " + boostId + "       boost time  " + DataCenter.Instance.GetBoostTimeSeconds(boostId));
        boostNum.text = DataCenter.GetBoost(boostId).ToString();
        if (DataCenter.Instance.CheckIsBoostActive(boostId))
        {
            StartDurationTimer(DataCenter.Instance.GetBoostTimeSeconds(boostId));
        }

        else if (!DataCenter.Instance.CheckIsBoostActive(boostId))
        {
            boostTime.text = "00:00:00";

            if (timer != null)
            {
                timer.Stop();
                timer = null;
            }
        }
    }


    void StartDurationTimer(float time)
    {

        if (timer != null)
        {
            timer.Stop();
            timer = null;
        }


        timer = new Timer(time, OnDurationTimerComplete, OnDurationTimerUpdate);
    }

    void OnDurationTimerComplete()
    {
        SaveData.Instance.SetCurrentBoostId(-1);
        SaveData.Instance.GetBoostTime(boostId);
        Debug.Log("END");
        timer.Stop();
        timer = null;
    }

    void OnDurationTimerUpdate(float remainedTime, float totalTime)
    {

        if (boostTime != null)
            boostTime.text = remainedTime.ToTimeFormat();

        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != "GameScene")
        {
            if (timer != null)
                timer.Stop();
            timer = null;
        }


    }
    public void OnButtonClick()
    {
        if (DataCenter.GetCurrentBoostId() == boostId && DataCenter.Instance.CheckIsBoostActive(boostId))
        {
            SaveData.Instance.SetBoostTime(boostId, DataCenter.GetBoostTime(boostId).ToDateTime().AddSeconds(20).Ticks);
        }

        else if (DataCenter.GetCurrentBoostId() != boostId)
        {
            DataCenter.SetBoostTime(DataCenter.GetCurrentBoostId(), 0);
            StartCoroutine(CleanData(DataCenter.GetCurrentBoostId()));

            SaveData.Instance.SetBoostTime(boostId, DataCenter.GetNetworkTime().AddSeconds(20).Ticks);
            SaveData.Instance.SetCurrentBoostId(boostId);
        }

        else if (DataCenter.GetCurrentBoostId() == boostId && !DataCenter.Instance.CheckIsBoostActive(boostId))
        {
            SaveData.Instance.SetBoostTime(boostId, DataCenter.GetNetworkTime().AddSeconds(20).Ticks);
        }

    }

    IEnumerator CleanData(int boostId)
    {
        yield return new WaitForSeconds(2);
        SaveData.Instance.SetBoostTime(boostId, 0);
    }
}
