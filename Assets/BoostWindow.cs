﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BoostWindow : Window
{
    public System.Action onClickCloseButton;

    public override void Hide(bool animation = true)
    {
        base.Hide(animation);

        onClickCloseButton = null;
    }

    public override void OnClickCloseButton()
    {
        if (onClickCloseButton != null)
            onClickCloseButton();
    }

}