﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChestButton : MonoBehaviour
{
    [SerializeField] private E_ChestTypeId chesType;
    [SerializeField] private ChestController chestSpace;

    public void OnButtonClick()
    {
        chestSpace.gameObject.SetActive(true);
        chestSpace.Init(chesType);
    }
}
