﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoreButton : MonoBehaviour
{
    [SerializeField] RectTransform content;
    [SerializeField] ScrollRect scrollview;
    [SerializeField] float positionY;
    
    public void OnButtonClick()
    {
        scrollview.decelerationRate = 0;
        content.transform.position = new Vector3(content.transform.position.x, positionY, content.transform.position.z);

        StartCoroutine(OnInertia());
    }
    IEnumerator OnInertia()
    {
        yield return new WaitForSeconds(0.1f);
        scrollview.decelerationRate = 0.135f;
    }

}
