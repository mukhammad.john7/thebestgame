﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestController : MonoBehaviour
{
    [SerializeField] private ChestAnimation chest;
    private float[] chanceCards;
    E_WorkerTypeId cardType;
    E_ChestTypeId chestType;
    private string workerType;
    private int workerId;

    public void Init(E_ChestTypeId chestType)
    {
        this.chestType = chestType;
        switch (chestType)
        {
            case E_ChestTypeId.SimpleChest:
                chanceCards = new float[4] { 0.85f, 0.1f, 0.04f, 0.01f};
                break;

            case E_ChestTypeId.Chest:
                chanceCards = new float[4] { 0.45f, 0.45f, 0.08f, 0.02f };
                break;

            case E_ChestTypeId.SilverChest:
                chanceCards = new float[4] { 0.15f, 0.7f, 0.11f, 0.04f };
                break;

            case E_ChestTypeId.GoldenChest:
                chanceCards = new float[4] { 0.0f, 0.2f, 0.7f, 0.1f };
                break;

            case E_ChestTypeId.EpicChest:
                chanceCards = new float[4] { 0.0f, 0.0f, 0.6f, 0.4f };
                break;

            case E_ChestTypeId.LegendaryChest:
                chanceCards = new float[4] { 0.0f, 0.0f, 0.0f, 1.0f };
                break;
        }
        chest.Init(chestType.GetHashCode());
        SelectCard();
    }

    private void SelectCard()
    {
        float rnd = Random.value;
        float sum = 0.0f;

        for (int loop = 0; loop < chanceCards.Length; loop++)
        {
            sum += chanceCards[loop];

            if (sum >= rnd)
            {
                cardType = (E_WorkerTypeId)loop;
                Debug.Log("Card:  " + cardType);
                Debug.Log("Chest: " + chestType);
                if (cardType.ToString() == "Legendary")
                {
                    int brokerOrManager = Random.Range(0, 2);
                    if (brokerOrManager == 0)
                    {
                        int brokerId = Random.Range(0, 3);
                        Debug.Log("BROKER: " + brokerId);
                    }
                    if (brokerOrManager == 1)
                    {
                        int managerId = Random.Range(0, 2);
                        Debug.Log("MANAGER: " + managerId);
                    }
                }
                break;
            }
        }
    }

}
