﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour
{
    [SerializeField] Animator tutorialAnimator;
    [SerializeField] Animator dialogAnimator;
    [SerializeField] GameObject dialog;

    int[] slideNum = new int[] {1,2,3};

    public void OnButtonClick()
    {
        dialogAnimator.SetTrigger("Hide");

        if (slideNum[0] > 0)
        {
            dialogAnimator.SetTrigger("Show");
            slideNum[0]--;
        }
        else
        {
            tutorialAnimator.SetTrigger("Hide");
        }
    }

    public void ShowDialog()
    {
        dialog.SetActive(true);
    }

    public void OnEndOfTutorial()
    {
        dialog.SetActive(false);
        gameObject.SetActive(false);
    }
}
