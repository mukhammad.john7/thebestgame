﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorkerInfo : MonoBehaviour
{
    [SerializeField] private GameObject[] stars;
    [SerializeField] private Image image;
    [SerializeField] private GameObject upgradeButton;
    [SerializeField] private GameObject upgradeButtonPlace;
    [SerializeField] private GameObject selectButtonPlace;
    [SerializeField] private GameObject selectButton;
    [SerializeField] private Text cardCount;

    public static event Action onSelectWorkerChanged;

    public int StarsCount;

    private int typeId;
    private int id;
    private string worker;
    private int CityId;
    private int BuildingId;
    private int FloorId;
    private int BrokerId;
    void Start()
    {
    }
    private void OnEnable()
    {
        DataCenter.onWorkersChanged += InfoChanged;
        onSelectWorkerChanged += InfoChanged;
    }

    private void OnDisable()
    {
        DataCenter.onWorkersChanged -= InfoChanged;
        onSelectWorkerChanged -= InfoChanged;
    }

    public void Init(string worker, int workerTypeId, int workerId, bool isSelectActive, bool isUpgradeActive, int cityId, int buildingId, int floorId, int brokerId)
    {
        BrokerId = brokerId;
        CityId = cityId;
        BuildingId = buildingId;
        FloorId = floorId;

        selectButton.SetActive(isSelectActive);

        if (isUpgradeActive)
            selectButtonPlace.SetActive(isSelectActive);

        upgradeButtonPlace.SetActive(isUpgradeActive);
        upgradeButton.SetActive(isUpgradeActive);
        typeId = workerTypeId;
        id = workerId;
        this.worker = worker;
        InfoChanged();
    }

    private void InfoChanged()
    {
        int starsNum = 0;

        for (int i = 0; i < 5; i++)
            stars[i].SetActive(false);

        if (worker == "Broker")
        {
            starsNum = DataCenter.GetBrokerStars(typeId, id);
            for (int i = 0; i < starsNum; i++)
            {
                if (starsNum < 6)
                    stars[i].SetActive(true);
            }

            cardCount.text = DataCenter.GetBrokerCards(typeId, id) + "/" + Config.STAR_COST(starsNum);
            if (starsNum >= 5)
            {
                cardCount.text = DataCenter.GetBrokerCards(typeId, id).ToString();
                upgradeButton.SetActive(false);
            }
        }

        if (worker == "Manager")
        {
            starsNum = DataCenter.GetManagerStars(typeId, id);
            for (int i = 0; i < starsNum; i++)
            {
                if (starsNum < 6)
                    stars[i].SetActive(true);
            }

            cardCount.text = DataCenter.GetManagerCards(typeId, id) + "/" + Config.STAR_COST(starsNum);
            if (starsNum >= 5)
            {
                cardCount.text = DataCenter.GetManagerCards(typeId, id).ToString();
                upgradeButton.SetActive(false);
            }
        }

        StarsCount = starsNum;

        if (starsNum != 0)
            image.sprite = GameResources.GetWorkerSprite(typeId, 1, worker, id);
        else
        {
            image.sprite = GameResources.GetWorkerSprite(typeId, 0, worker, id);
            selectButton.SetActive(false);
        }
    }

    public void OnButtonClick()
    {
        if (worker == "Manager")
        {
            if (DataCenter.GetManagerCards(typeId, id) >= Config.STAR_COST(DataCenter.GetManagerStars(typeId, id)))
            {
                SaveData.Instance.AddManagerCards(typeId, id, -Config.STAR_COST(DataCenter.GetManagerStars(typeId, id)));
                SaveData.Instance.AddManagerStars(typeId, id);
            }
            Debug.Log(typeId + "  TYPEEE  " + id + "  IDDD  " + worker + " stars  " + DataCenter.GetManagerStars(typeId, id));
        }
        else if (worker == "Broker")
        {
            if (DataCenter.GetBrokerCards(typeId, id) >= Config.STAR_COST(DataCenter.GetBrokerStars(typeId, id)))
            {
                SaveData.Instance.AddBrokerCards(typeId, id, -Config.STAR_COST(DataCenter.GetBrokerStars(typeId, id)));
                SaveData.Instance.AddBrokerStars(typeId, id);
            }
            Debug.Log(typeId + "  TYPEEE  " + id + "  IDDD  " + worker + " stars  " + DataCenter.GetBrokerStars(typeId, id));
        }
        

    }

    public void OnSelectButtonClick()
    {
        if (worker == "Broker")
        {
            DataCenter.SetCurrentBrokerType(CityId, BuildingId, FloorId, BrokerId, typeId);
        }

        if (worker == "Manager")
        {
            DataCenter.SetCurrentManagerType(CityId, BuildingId, FloorId, typeId);
        }
        onSelectWorkerChanged?.Invoke();
    }

    public void OnButtonClickAddCards()
    {
        if (worker == "Manager")
        {
            SaveData.Instance.AddManagerCards(typeId, id, 10);
        }
        else if (worker == "Broker")
        {
            SaveData.Instance.AddBrokerCards(typeId, id, 10);
        }


    }
}
