﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardAnimation : MonoBehaviour
{
    [SerializeField] private Button getButton;
    [SerializeField] private GameObject chest;
    public void ShowGetButton()
    {
        getButton.gameObject.SetActive(true);
        getButton.onClick.AddListener(GetCard);
    }

    private void GetCard()
    {
        getButton.onClick.RemoveAllListeners();
        getButton.gameObject.SetActive(false);
        chest.SetActive(false);
    }
}
