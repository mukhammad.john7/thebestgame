﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LoginWindow : Window
{
    public System.Action onClickCloseButton;

    //[SerializeField] UiTabs uiTabs;
    [SerializeField] Text eventPointText;
    [SerializeField] Text crystalsText;
    [SerializeField] Text goldText;
    [SerializeField] Text potionsText;
    [SerializeField] Text fragmentsText;

    private void Start()
    {
    }

    public override void Hide(bool animation = true)
    {
        base.Hide(animation);

        onClickCloseButton = null;
    }

    public override void OnClickCloseButton()
    {
        //AudioManager.Instance.PlaySound("Click");


        if (onClickCloseButton != null)
            onClickCloseButton();
    }

}