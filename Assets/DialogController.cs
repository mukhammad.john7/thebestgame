﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogController : MonoBehaviour
{
    [SerializeField] Text text;

    string[] wolfText = new string[] { "Hi, my name is Wolf!", "Are you ready to get a lot of money?" };
    int textIndex = 0;

    public void ChangeText()
    {
        text.text = wolfText[textIndex];
        textIndex++;
    }
}
