﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorkersWindow : Window
{
    public System.Action onClickCloseButton;

    [SerializeField] private RectTransform workersPlace;
    [SerializeField] private List<WorkerInfo> workers;
    private WorkerInfo worker;

    private void Start()
    {
        Init();
    }

    public override void Hide(bool animation = true)
    {
        base.Hide(animation);

        onClickCloseButton = null;
    }

    public override void OnClickCloseButton()
    {
        //AudioManager.Instance.PlaySound("Click");


        if (onClickCloseButton != null)
            onClickCloseButton();
    }

    public void Init()
    {
        int brokerTypeId = 0;
        int brokerId = 0;
        int managerTypeId = 1;
        int managerId = 0;
        for (int i = 0; i < 12; i++)
        {

            if (i % 3 == 0 && i != 0)
            {
                brokerTypeId++;
                brokerId = 0;
            }
            worker = Instantiate(GameResources.GetWorkerPrefab(), workersPlace);
            worker.Init("Broker", brokerTypeId, brokerId, false, true, 0, 0 ,0, 0);
            workers.Add(worker);
            brokerId++;
        }

        for (int i = 0; i < 12; i++)
        {
            worker = Instantiate(GameResources.GetWorkerPrefab(), workersPlace);
            worker.Init("Manager", 0, i, false, true, 0, 0, 0, 0);
            workers.Add(worker);
        }

        for (int i = 0; i < 6; i++)
        {
            if (i % 2 == 0 && i != 0)
            {
                managerTypeId++;
                managerId = 0;
            }
            worker = Instantiate(GameResources.GetWorkerPrefab(), workersPlace);
            worker.Init("Manager", managerTypeId, managerId, false, true, 0, 0, 0, 0);
            workers.Add(worker);
            managerId++;
        }

    }
}
