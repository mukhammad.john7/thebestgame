﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrivacyWindow : Window
{
    public System.Action onClickOkButton;

    public void OnClickOkButton()
    {
        //AudioManager.Instance.PlaySound("Click");

        onClickOkButton?.Invoke();
    }

    public void OnClickPrivacyButton()
    {
        //AudioManager.Instance.PlaySound("Click");


        Application.OpenURL(Config.PRIVACY_POLICY_URL);
    }
}
