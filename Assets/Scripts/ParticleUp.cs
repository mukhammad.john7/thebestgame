﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class ParticleUp : MonoBehaviour
{


    Particle[] particles;
    Vector2[] pos;
    public ParticleSystem partSys;
    float progress;
    float step = 0.00005f;


    private void Awake()
    {
        particles = new ParticleSystem.Particle[partSys.main.maxParticles];

        //for (int i = 0; i < particles.Length; i++)
        //{
        //    pos[i] = particles[i].position;
        //}

        partSys.Play();

        Debug.Log(particles[0].position);

    }
    //private void Awake()
    //{

    //    //var emitParams = new ParticleSystem.EmitParams();
    //    //emitParams.applyShapeToPosition = true;
    //    //emitParams.position = new Vector3(0f, 0f, 0.0f);
    //    //emitParams.velocity = new Vector3(500, 500f, 0f);
    //    //emitParams.ResetPosition();

    //    //partSys.Emit(emitParams, 50);
    //    //partSys.
    //}


    void Update()
    {

        int numParticlesAlive = partSys.GetParticles(particles);

        // Change only the particles that are alive


        // Apply the particle changes to the Particle System


        StartCoroutine(ChangeDirection());


    }

    IEnumerator ChangeDirection()
    {
        bool isUp = true;
        Vector3 target = new Vector3(0f, 900f, -1f);
        if (isUp)
            yield return new WaitForSeconds(0.4f);
        int numParticlesAlive = partSys.GetParticles(particles);

        for (int i = 0; i < numParticlesAlive; i++)
        {
            target.z = Random.Range(-1f, -10f);
            particles[i].position = Vector3.Lerp(particles[i].position, target, progress);
            //particles[i].position = target;
        }
        partSys.SetParticles(particles, numParticlesAlive);
        progress += step;
        isUp = false;
        //yield return new WaitForSeconds(3f);
        //Debug.Log("BBBBBBBB");
    }

    void OnMouseDown()
    {
        Debug.Log("AAAAAAA");
        partSys.Play();
    }
}
