﻿using CodeStage.AntiCheat.ObscuredTypes;
using CodeStage.AntiCheat.Detectors;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatController : SingletonMonoBehaviour<CheatController>
{
    public static event System.Action onCheatDetection;

    float prevRealTime;
    int prevReceivedTime;
    int speedHackDetected;


    float deltaRealTime;
    float deltaReceivedTime;


    public static bool IsSuspect
    {
        get { return ObscuredPrefs.GetBool("is_suspect", false); }
        set { ObscuredPrefs.SetBool("is_suspect", value); }
    }

    public static double SpeedhackAmount
    {
        get { return ObscuredPrefs.GetDouble("speedhack_amount", 0d); }
        set { ObscuredPrefs.SetDouble("speedhack_amount", value); }
    }

    public static bool IsCheat
    {
        get { return ObscuredPrefs.GetBool("is_cheat", false); }
        set { ObscuredPrefs.SetBool("is_cheat", value); }
    }

    public static bool IsSync
    {
        get { return ObscuredPrefs.GetBool("is_sync_cheat", false); }
        set { ObscuredPrefs.SetBool("is_sync_cheat", value); }
    }

    public static int ObscuredCheatCount
    {
        get { return ObscuredPrefs.GetInt("obscured_cheat_count", 0); }
        set { ObscuredPrefs.SetInt("obscured_cheat_count", value); }
    }

    public static int InjectionCount
    {
        get { return ObscuredPrefs.GetInt("injection_count", 0); }
        set { ObscuredPrefs.SetInt("injection_count", value); }
    }

    public static int SpeedHackCount
    {
        get { return ObscuredPrefs.GetInt("speed_hack_count", 0); }
        set { ObscuredPrefs.SetInt("speed_hack_count", value); }
    }


    void OnEnable()
    {
        //ChatController.onTimeReceived += OnTimeReceived;
    }

    void OnDisable()
    {
        //ChatController.onTimeReceived -= OnTimeReceived;

        ObscuredCheatingDetector.Dispose();
        InjectionDetector.Dispose();
        //SpeedHackDetector.Dispose();
    }

    void Start()
    {
        ObscuredCheatingDetector.StartDetection(OnObscuredDetected);
        //InjectionDetector.StartDetection(OnInjectionDetected);
        //SpeedHackDetector.StartDetection(OnSpeedHackDetected, 1f, 5, 30);
    }

    void OnObscuredDetected()
    {
        Debug.LogError("!!! OnObscuredDetected");

        OnDetection(E_CheatType.ObscuredCheat);
    }

    void OnInjectionDetected(string value)
    {
        Debug.LogError("!!! OnInjectionDetected: " + value);

        OnDetection(E_CheatType.Injection);
    }

    void OnSpeedHackDetected()
    {
        Debug.LogError("!!! OnSpeedHackDetected");

        OnDetection(E_CheatType.SpeedHack);
    }

    void OnDetection(E_CheatType cheatType)
    {
        switch (cheatType)
        {
            case E_CheatType.ObscuredCheat:
                ObscuredCheatCount++;
                break;

            case E_CheatType.Injection:
                InjectionCount++;
                break;

            case E_CheatType.SpeedHack:
                SpeedHackCount++;
                break;
        }

        IsCheat = true;

        IsSync = false;

        PlayerPrefs.Save();

        Debug.Log(string.Format("!!!!!!!!!!! IsCheat:{0}, O:{1}, I:{2}, S:{3}", IsCheat, ObscuredCheatCount, InjectionCount, SpeedHackCount));

        if (onCheatDetection != null)
            onCheatDetection();
    }

    void OnTimeReceived(int unixTime)
    {
        if (Math.Abs(TimeManager.Instance.NetworkDateTime.Subtract(unixTime.UnixTimeToDateTime()).TotalSeconds) < GameConfig.Instance.differenceNetworkTime)
        {
            TimeManager.Instance.IsPause = false;
            Time.timeScale = 1f;
        }
        else
        {
            WindowManager.Instance.GetWindow<LoadingWindow>().Hide();

            TimeManager.Instance.IsPause = true;
            Time.timeScale = 0f;
        }

        if (prevRealTime != 0f && prevReceivedTime != 0)
        {
            deltaRealTime = Time.fixedTime - prevRealTime;
            deltaReceivedTime = unixTime - prevReceivedTime;

            if (deltaRealTime - deltaReceivedTime > GameConfig.Instance.differenceRealTime || Time.timeScale > GameConfig.Instance.maxTimeScale)
                speedHackDetected++;
            else
                speedHackDetected = 0;

            if (speedHackDetected >= GameConfig.Instance.maxSpeedHackDetected)
            {
                speedHackDetected = 0;
                OnDetection(E_CheatType.SpeedHack);
            }
        }
        else
        {
            speedHackDetected = 0;
        }

        prevRealTime = Time.fixedTime;
        prevReceivedTime = unixTime;
    }
}