﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerWindow : Window
{
    public System.Action onClickCloseButton;
    public System.Action onClickBuyButton;

    //[SerializeField] UiTabs uiTabs;
    [SerializeField] Text bonusText;
    [SerializeField] Text workerNameText;
    [SerializeField] Text levelText;
    [SerializeField] Text potionsText;
    [SerializeField] Text fragmentsText;
    [SerializeField] Button upgradeButton;
    [SerializeField] Text upGradeText;
    [SerializeField] WorkerInfo selectedWorker;
    [SerializeField] List<WorkerInfo> allWorkers;
    [SerializeField] List<Floor> floors;
    private int CityId;
    private int BuildingId;
    private int FloorId;


    private void OnEnable()
    {
        WorkerInfo.onSelectWorkerChanged += UpgradeInfo;
        DataCenter.onWorkerLevelChanged += UpgradeInfo;
    }

    private void OnDisable()
    {
        WorkerInfo.onSelectWorkerChanged -= UpgradeInfo;
        DataCenter.onWorkerLevelChanged -= UpgradeInfo;
    }

    public void Show(int cityId, int buildingId, int floorId)
    {
        E_WorkerTypeId workerTypeId = (E_WorkerTypeId)2;

        CityId = cityId;
        BuildingId = buildingId;
        FloorId = floorId;

        UpgradeInfo();
        base.Show();
    }

    public void UpgradeInfo()
    {
        selectedWorker.Init("Manager", DataCenter.GetCurrentManagerType(CityId, BuildingId, FloorId), FloorId, false, false, CityId, BuildingId, FloorId, 0);

        if (DataCenter.GetManagerLevel(CityId, BuildingId, FloorId, DataCenter.GetCurrentManagerType(CityId, BuildingId, FloorId)) == 0)
            upGradeText.text = "HIRE";

        else
            upGradeText.text = "UPGRADE";

        for (int i = 0; i < 4; i++)
        {
            
            if (i == DataCenter.GetCurrentManagerType(CityId, BuildingId, FloorId))
                allWorkers[i].Init("Manager", i, FloorId, false, false, CityId, BuildingId, FloorId, 0);

            else
                allWorkers[i].Init("Manager", i, FloorId, true, false, CityId, BuildingId, FloorId, 0);
        }
        levelText.text = DataCenter.GetManagerLevel(CityId, BuildingId, FloorId, DataCenter.GetCurrentManagerType(CityId, BuildingId, FloorId)) + " level";
        bonusText.text = "+" + Config.WORKER_BONUS((E_WorkerTypeId)DataCenter.GetCurrentManagerType(CityId, BuildingId, FloorId)) * selectedWorker.StarsCount + "%";

        if (DataCenter.GetCurrentManagerType(CityId, BuildingId, FloorId) == 0)
            workerNameText.text = Config.JUNIOR_MANAGER_NAME(FloorId);

        else if (DataCenter.GetCurrentManagerType(CityId, BuildingId, FloorId) == 1)
            workerNameText.text = Config.EXPERIENCED_MANAGER_NAME(FloorId);

        else if (DataCenter.GetCurrentManagerType(CityId, BuildingId, FloorId) == 2)
            workerNameText.text = Config.MASTER_MANAGER_NAME(FloorId);

        else if (DataCenter.GetCurrentManagerType(CityId, BuildingId, FloorId) == 3)
            workerNameText.text = Config.LEGENDARY_MANAGER_NAME(FloorId);

        if (FloorId >= 2)
        {
            for (int i = 1; i < 4; i++)
                allWorkers[i].gameObject.SetActive(false);
        }
        else
        {
            for (int i = 1; i < 4; i++)
                allWorkers[i].gameObject.SetActive(true);
        }
    }

    public override void Hide(bool animation = true)
    {
        base.Hide(animation);

        onClickCloseButton = null;
        onClickBuyButton = null;
    }

    //void UpdateContent()
    //{
    //    crystalsText.text = DataCenter.Instance.Crystals.ToDecimalDisplay();
    //    eventPointText.text = DataCenter.Instance.EventPoint.ToDecimalDisplay();
    //    goldText.text = DataCenter.Instance.Gold.ToDecimalDisplay();
    //    potionsText.text = DataCenter.Instance.TimeTravelPotion.ToString();
    //    fragmentsText.text = DataCenter.Instance.Fragments.ToDecimalDisplay();
    //}

    public override void OnClickCloseButton()
    {
        //AudioManager.Instance.PlaySound("Click");

        //if (DataCenter.Instance.IsItemPurchased)
        //    StartCoroutine(SaveProcess());
        if (onClickCloseButton != null)
            onClickCloseButton();
    }

    public void OnClickBuyButton()
    {

        if (onClickBuyButton != null)
        {
            onClickBuyButton();
            OnClickCloseButton();
        }

        SaveData.Instance.AddManagerLevel(CityId, BuildingId, FloorId, DataCenter.GetCurrentManagerType(CityId, BuildingId, FloorId), 1);

        if (DataCenter.GetManagerLevel(CityId, BuildingId, FloorId, 1) == 0)
        {
            SaveData.Instance.AddManagerLevel(CityId, BuildingId, FloorId, 1, 1);
            DataCenter.AddManagerLevel(CityId, BuildingId, FloorId, 1, 1);
        }


        if (DataCenter.GetManagerLevel(CityId, BuildingId, FloorId, 2) == 0)
        {
            SaveData.Instance.AddManagerLevel(CityId, BuildingId, FloorId, 2, 1);
            DataCenter.AddManagerLevel(CityId, BuildingId, FloorId, 2, 1);
        }


        if (DataCenter.GetManagerLevel(CityId, BuildingId, FloorId, 3) == 0)
        {
            SaveData.Instance.AddManagerLevel(CityId, BuildingId, FloorId, 3, 1);
            DataCenter.AddManagerLevel(CityId, BuildingId, FloorId, 3, 1);
        }

        if (floors[FloorId].timer != null)
            floors[FloorId].timer.TotalTime = Calculations.GetFloorEarnTime(floors[FloorId].FloorData.EarnTime, DataCenter.GetManagerLevel(CityId, BuildingId, FloorId, DataCenter.GetCurrentManagerType(CityId, BuildingId, FloorId)), Config.WORKER_BONUS((E_WorkerTypeId)DataCenter.GetCurrentManagerType(CityId, BuildingId, FloorId)) * DataCenter.GetManagerStars(DataCenter.GetCurrentManagerType(CityId, BuildingId, FloorId), FloorId));

        InterfaceController.Instance.ShowCoinParticle(upgradeButton.gameObject.transform.position);

    }
    //IEnumerator SaveProcess()
    //{
    //    LoadingWindow loadingWindow = WindowManager.Instance.GetWindow<LoadingWindow>();
    //    loadingWindow.Show();

    //    yield return GameController.Instance.RequestSaveProgress();

    //    loadingWindow.Hide();

    //    DataCenter.Instance.IsItemPurchased = false;

    //    if (onClickCloseButton != null)
    //        onClickCloseButton();
    //}


    //void OnSkinBought(Skin skin)
    //{
    //    UpdateContent();
    //}
}