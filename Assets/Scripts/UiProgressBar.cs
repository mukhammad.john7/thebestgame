﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiProgressBar : MonoBehaviour
{
    [SerializeField] Image filledImage;

    public float Progress
    {
        get { return filledImage.fillAmount; }
        set { filledImage.fillAmount = value; }
    }
}
