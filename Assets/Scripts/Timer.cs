﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer
{
    Action onComplete;
    Action<float, float> onUpdate;
    Action onEverySecondUpdate;

    float everySecondFactor = 1f;
    bool isPause = false;

    public bool IsCounting { get; private set; }

    public bool IsLooped { get; private set; }

    public bool IsPauseAvailable { get; private set; }

    public float TotalTime { get; set; }

    public float PassedTime { get; set; }

    public float RemainedTime { get { return TotalTime - PassedTime; } }

    public float Progress { get { return PassedTime / TotalTime; } }


    public Timer(float duration, Action onComplete, Action<float, float> onUpdate = null, Action onEverySecondUpdate = null, bool isLooped = false, bool isPaused = false)
    {
        TotalTime = duration;
        this.onComplete = onComplete;
        this.onUpdate = onUpdate;
        IsLooped = isLooped;
        IsPauseAvailable = isPaused;
        this.onEverySecondUpdate = onEverySecondUpdate;

        Restart();
    }

    void UpdateTime(float deltaTime)
    {
        if (!IsCounting || isPause)
            return;

        PassedTime += deltaTime;

        if (PassedTime >= TotalTime)
        {
            PassedTime = TotalTime;

            if (IsLooped)
                Restart();
            else
            {
                IsCounting = false;

                Stop();
            }

            if (onComplete != null)
                onComplete();
        }
        else
        {
            if (onUpdate != null)
                onUpdate(RemainedTime, TotalTime);

            everySecondFactor += deltaTime;

            if (everySecondFactor >= 1f)
            {
                everySecondFactor = 0f;

                if (onEverySecondUpdate != null)
                    onEverySecondUpdate();
            }
        }
    }

    public void Restart()
    {
        PassedTime = 0f;

        ClearTimer();

        Start();
    }

    public void Start()
    {
        if (IsCounting)
            return;

        IsCounting = true;

        TimeManager.onFarmeUpdate += UpdateTime;
        TimeManager.onApplicationPause += SetPause;
    }

    public void Stop()
    {
        if (!IsCounting)
            return;

        ClearTimer();
    }

    public void SetPause(bool isPause)
    {
        if (IsPauseAvailable && PassedTime < TotalTime)
            this.isPause = isPause;
    }

    public void Resume()
    {
        IsCounting = true;
    }

    public void Fire()
    {
        PassedTime = TotalTime;
    }

    void ClearTimer()
    {
        IsCounting = false;
        isPause = false;

        TimeManager.onFarmeUpdate -= UpdateTime;
        TimeManager.onApplicationPause -= SetPause;
    }

    public void ChangeDuration(float time)
    {
        TotalTime = Mathf.Max(0f, time);
    }
}
