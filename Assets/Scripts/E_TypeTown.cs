﻿public enum E_TypeTown
{
    None = 0,
    PoorSlums = 1, //Бедные трущобы
    Favelas = 2, //Фавеллы лучше звучит чем просто трущобы
    MiddleclassDistrict = 3, // Район среднего класса
    WealthyDistrict = 4, //Состоятельный район
    RichDistrict = 5, //Богатый район
    EliteDistrict = 6 //Элитный район
}
