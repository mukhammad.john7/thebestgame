﻿using UnityEngine;

public class PageWithPageTabGroup : Page
{
    [SerializeField] protected PageTabGroup pageTabGroup;


    private void OnEnable()
    {
        pageTabGroup.onActiveTabChanged += TabController_OnActiveTabChanged;
    }

    private void OnDisable()
    {
        pageTabGroup.onActiveTabChanged -= TabController_OnActiveTabChanged;
    }

    public override void Init()
    {
        base.Init();

        pageTabGroup.Init();
    }

    public override void Show()
    {
        if (IsInit)
        {
            base.Show();

            pageTabGroup.ActivePage.Show();
        }
        else
        {
            // pageTabGroup.Init(); вызовет Show();
            base.Show();
        }
    }

    public override void Hide()
    {
        base.Hide();

        pageTabGroup.ActivePage.Hide();
    }

    public override void UpdateContent()
    {
        base.UpdateContent();

        pageTabGroup.ActivePage.UpdateContent();
    }

    public override void OnFocus()
    {
        base.OnFocus();

        pageTabGroup.ActivePage.OnFocus();
    }

    public override void OnUnFocus()
    {
        if (pageTabGroup.ActiveTabIndex != pageTabGroup.DefaultTabIndex)
            pageTabGroup.SetActiveTab(pageTabGroup.DefaultTabIndex, false);

        base.OnUnFocus();

        pageTabGroup.ActivePage.OnUnFocus();
    }

    public void ShowTabPage(int index)
    {
        pageTabGroup.SetActiveTab(index);
    }

    public void ShowNextTabPage()
    {
        int nextTabIndex = pageTabGroup.ActiveTabIndex + 1;

        if (nextTabIndex >= pageTabGroup.Pages.Count)
            nextTabIndex = 0;

        if (!pageTabGroup.TabButtons[nextTabIndex].IsAvailable)
            nextTabIndex = pageTabGroup.DefaultTabIndex;

        pageTabGroup.SetActiveTab(nextTabIndex);
    }

    //

    private void TabController_OnActiveTabChanged(int tabIndex)
    {
        if (pageTabGroup.PrevActiveTabIndex == tabIndex)
            return;

        pageTabGroup.PrevPage.OnUnFocus();
        pageTabGroup.ActivePage.OnFocus();
    }
}
