﻿using UnityEngine;
using UnityEngine.UI;

public class Room
{
    public int Id;
    public string name;

    public Image icon; 

    public int MaxBroker { get; set; }

    public int PriceUpgrade { get; set; }
}
