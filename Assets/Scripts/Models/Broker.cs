﻿using UnityEngine.UI;

public class Broker
{
    public int Id;
    public string name;

    private Image icon;

    private E_TypeBroker Type;

    public int Level { get; set; }

    private int CollectedCount { get; set; }

    private int PriceUpgrade { get; set; }
}
