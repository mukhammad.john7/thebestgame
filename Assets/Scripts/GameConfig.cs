﻿using CodeStage.AntiCheat.ObscuredTypes;
using System;
using UnityEngine;

[CreateAssetMenu(fileName = "GameConfig", menuName = "ScriptableObject/GameConfig", order = 1)]
public class GameConfig : SingletonScriptableObject<GameConfig>
{
    [Header("Config")]

    public ObscuredString apiUrl;

    public ObscuredString bundleUrl;

    public ObscuredString apiKey;

    public ObscuredString networkCode;

    public ObscuredFloat differenceNetworkTime;

    public ObscuredFloat differenceRealTime;

    public ObscuredInt maxSpeedHackDetected;

    public ObscuredFloat maxTimeScale;

    public ObscuredFloat networkUpdateTime;

    public ObscuredInt captchaRotationAngle;

    public ObscuredFloat inventoryChangeSendTime;

    public ObscuredInt nicknameCharactersLimit;

    public ObscuredInt autoSaveSeconds;

    public ObscuredInt autoSaveMinSeconds;

    [Header("Premium")]

    public ObscuredFloat premiumFragmentBonus;

    public ObscuredFloat premiumDPSBonus;

    public ObscuredInt minEventGettingRewardTime;

    public ObscuredInt maxEventGettingRewardTime;

    [Header("Basic")]

    public int supporterCount;

    public int enemyCount;

    [Space]

    public int inventoryCellsCount;

    public int achievementCount;

    public int achievementMaxLevel;

    public ObscuredInt[] achievementRewards;

    public int activeSkillCount;

    public int artifactCount;

    public int specialArtifactCount;

    public ObscuredInt tapAttackMax;

    public ObscuredInt slashAttackMax;

    public ObscuredFloat baseBonusGoldChance;

    public ObscuredFloat baseBossTime;

    public ObscuredFloat baseChestChance;

    public ObscuredFloat baseCriticalChance;

    public ObscuredFloat baseCriticalDamageModifier;

    public ObscuredFloat baseSupporterDeathChance;

    public ObscuredFloat baseWeakPointDamageModifier;

    public ObscuredFloat baseWeakPointDuration;

    public ObscuredFloat baseWeakPointProbability;

    public ObscuredFloat baseWeakPointRadius;

    public ObscuredFloat baseWeakPointSize;

    public ObscuredFloat baseWeakPointSpeed;

    public ObscuredFloat baseGainPointDamageModifier;

    public float[] bossHpModifier;

    public int chestId;

    public int failEnemyCount;

    public ObscuredInt dropFragmentsAfterStage;

    public ObscuredInt dropFragmentsCount;

    public ObscuredInt dropFragmentsEvery_X_Stage;

    public ObscuredInt enemyCountInStage;

    public int[] evolveLevel;

    public ObscuredFloat evolveModifier;

    public ObscuredFloat goldBonusModifier;

    public ObscuredInt supporterEvolveRequiredLevel;

    public ObscuredFloat maxCriticalChance;

    public ObscuredInt maxStage;

    public ObscuredInt maxStageRework;

    public ObscuredInt minEnemyCountInStage;

    public int passiveSkillCount;

    public ObscuredInt[] passiveUnlockLevel;

    public ObscuredInt timeTravelRequirementLevel;

    public ObscuredInt timeTravelRequirementMinSupporterId;

    public double recoverDiscount;

    public int stageInLocation;

    public int specialBossStage;

    public int normalLocationCount;

    public int scrollsLogLenght;

    public float skinSellModificator;

    [Header("Notifications")]

    public int[] notificationOfflineBonusTimes;

    public int[] notificationComeBackTimes;

    [Header("Offline Gold")]

    public int offlineTimeLimit;

    public int offlineMinModificator;

    public int offlineMaxModificator;

    [Header("Event")]

    public ObscuredFloat likeTimeOutMin;

    public ObscuredFloat likeTimeOutMax;

    [Header("Interstitial Rewards")]

    //public E_CurrencyType[] interstitialRewards;

    [Header("Cheater")]

    public ObscuredInt speedhackEndTime;

    public ObscuredInt speedhackMinLimit;

    public ObscuredInt speedhackMaxLimit;

    public ObscuredInt crystalsAmount;

    public ObscuredInt cheaterMaxStage;

    [Header("Chat Settings")]

    public ObscuredInt chatMessageCharacterLimit;

    public ObscuredInt chatMessagesMax;

    public ObscuredFloat chatUpdateTime;

    public ObscuredFloat chatPremiumUpdateTime;

    public ObscuredInt chatWorldMessagePrice;

    public ObscuredInt chatWorldMessagePrice2;

    public ObscuredFloat chatPrice2Time;

    [Header("Siege Settings")]

    public ObscuredFloat siegeAttakTime;

    public ObscuredFloat siegeAttakWaitTime;

    public ObscuredInt siegeAttakLimit;

    public int[] siegeLocationIds;

    [Header("Clan Settings")]

    public ObscuredInt clanNameCharacterLimit;

    public ObscuredInt clanDescriptionCharacterLimit;

    public ObscuredInt clanShortNameCharacterMin;

    public ObscuredInt clanShortNameCharacterMax;

    public ObscuredInt clanCreatePrice;

    public Color[] clanColors;

    public ObscuredFloat clanSiegeJoinClanPointsModificator;

    [Header("Dungeon")]

    public ObscuredFloat dungeonWaitTime;

    public ObscuredInt dungeonAdsMax;

    public ObscuredInt dungeonStageCount;

    public ObscuredInt dungeonTutorialLenght;

    public ObscuredFloat[] dungeonScrollDropChance;

    public ObscuredInt dungeonEnemyCountInStage;

    [Header("Event")]

    public ObscuredFloat[] eventDropItemChance;

    public int eventBossId;

    public ObscuredInt eventBossHp;

    public ObscuredInt eventPointAmount;

    public ObscuredInt eventLeaderboardUpdateTime;

    public int[] winterLocation;

    [Header("Quests Settings")]

    public ObscuredFloat minQuestDropItemTime;

    public ObscuredFloat maxQuestDropItemTime;

    [Header("Clan Dungeon")]

    public ObscuredInt clanDungeonMinBossHp;

    public ObscuredInt clanDungeonAttackTimeLimit;

    public ObscuredInt clanDungeonAttackLimit;

    public ObscuredFloat clanDungeonAttackTime;

    public ObscuredInt clanDungeonDamageReward;

    public ObscuredInt[] clanDungeonsLocationId;

    [Header("Alliance")]

    public ObscuredInt allianceCreatePrice;
    public ObscuredInt allianceMembersCount;

    [Header("Robot")]

    public ObscuredFloat robotFrequency;

    public ObscuredFloat robotMinTime;

    public ObscuredFloat robotTimeout;

    public ObscuredInt robotBonusDuration;

    public ObscuredInt robotSupporterUpgradeCountPerPortal;

    public ObscuredInt robotRubiesPerPortal;

    public ObscuredFloat robotDpsPerPortal;

    [Header("Tutorial")]

    public int tutorialShowScrolls;

    public int tutorialShowRobot;

    public int tutorialFragmentsReward;

    public int tutorialFragmentsLevel;

    public int tutorialShowWeakPoint;

    public int tutorialPassiveSkillsReminderLevel;

    public int tutorial3ScrollsReminder;

    public int tutorialEndStage;

    [Header("Positions")]

    public Vector3 digitHeroPosition;

    public Vector3 digitTurretPosition;

    public Vector3 enemyPosition;

    public Vector3 inscriptionPosition;

    [Header("Colors")]

    public Color iconActiveColor;

    public Color iconDisableColor;

    public Color digitDamageColor;

    public Color digitCriticalColor;

    public Color digitTurretColor;

    public Color digitGoldColor;

    public Color digitCrystalColor;

    public Color digitFragmentColor;

    public Color profileClanColor;

    [Header("Bundle")]

    public ObscuredString bundlePrefabs;

    public ObscuredString bundleEnemies;

    public ObscuredString bundleUi;

    public ObscuredString bundleSupporters;

    public ObscuredString bundleSounds;

    public ObscuredString bundleSkins;

    public ObscuredString bundleBackgrounds;

    public ObscuredString bundleEvent;

    public ObscuredString bundleLocalization;

    public int AssetBundleVersion
    {
        get
        {
            return ObscuredPrefs.GetInt("asset_bundle_version");
        }
        set
        {
            ObscuredPrefs.SetInt("asset_bundle_version", value);
        }
    }


    public float GetEventDropChance
    {
        get
        {
#if UNITY_EDITOR
            return 1f;
#else
             return 1f;
            //float chance = (float)(14 - 2 * (Statistics.HighestStage / 500)) / 100;
            //return (chance != 0f ? chance : 0.02f) * 0.7f;          
#endif
        }
    }

    public long GetArtifactReturnValue(int level, int count, float costX, float costY)
    {
        if (level < 1)
            return 0L;

        long nextArtifactPrice = GetNextArtifactPrice(count - 1);

        for (int i = 1; i < level; i++)
            nextArtifactPrice += GetArtifactUpgradePrice(i, costX, costY);

        return nextArtifactPrice;
    }

    public long GetArtifactSellPrice(int count)
    {
        return MathExtensions.CeilToLong(Math.Min(75.0, Math.Pow(1.5, (double)count)));
    }

    public long GetArtifactUpgradePrice(int level, float costCoX, float costCoY)
    {
        return MathExtensions.CeilToLong(costCoX * Math.Pow((double)(level + 1), (double)costCoY));
    }

    public float GetBossGoldModifier(int stage)
    {
        return bossHpModifier[(stage - 1) % bossHpModifier.Length] * 0.5f;
    }

    public float GetBossHpModifier(int stage)
    {
        return bossHpModifier[(stage - 1) % bossHpModifier.Length] * (((stage % 100) != 0) ? 1f : 10f);
    }

    public double GetChestGold(double enemyGold)
    {
        return enemyGold * 10.0;
    }

    public double GetEnemyGold(int stage, double enemyHp)
    {
#if !IS_REWORK
        return Math.Ceiling((double)((enemyHp * (0.015 + (0.005 * Math.Max(10 - stage, 0)))) * Math.Pow(0.8912509381, (double)Math.Max(0, stage - 2800))));
#else
        return Math.Ceiling((double)((enemyHp * (0.015 + (0.005 * Math.Max(10 - stage, 0)))) * Math.Pow(0.89, (double)Math.Max(0, stage - 2250))));
#endif
    }

    public double GetEnemyHp(int stage)
    {
#if !IS_REWORK
        return Math.Ceiling((double)((5.0 * Math.Pow(1.57, (double)Math.Min(stage, 156))) * Math.Pow(1.2, (double)Math.Max(stage - 156, 0))));
#else
        return Math.Ceiling((double)((5.0 * Math.Pow(1.5, (double)Math.Min(stage, 156))) * Math.Pow(1.1407, (double)Math.Max(stage - 156, 0))));
#endif
    }

    public long GetNextArtifactPrice(int count)
    {
        return MathExtensions.CeilToLong((count + 2) * Math.Pow(1.25, (double)(count + 1)) * (1f /*+ DataCenter.Instance.SkinManager.ArtifactsBuyPriceModificator*/));
    }

    public long GetTimeTravelSupporterLevelBonus(int totalLevel)
    {
        return MathExtensions.CeilToLong((double)(((float)totalLevel) / 100f));
    }

    public long GetTimeTravelStageCompletionBonus(int stage)
    {
#if !IS_REWORK
        return MathExtensions.CeilToLong(Math.Pow(Math.Ceiling((double)(((float)stage) / 15f)), 1.7000000476837158));
#else
        return MathExtensions.CeilToLong(Math.Pow(Math.Ceiling((double)(((float)stage) / 15f)), 1.74946));
#endif
    }

    public long GetRecoverPrice(float remainTime, double discount = 0d)
    {
        long price = MathExtensions.CeilToLong((double)(remainTime / 180f * (1d - discount)));

        if (price > 0)
            return price;
        else return 0;
    }

    public int GetRecoverTime(int supporterId)
    {
        return (supporterId + 1) * 1800;
    }

    public int GetPassiveSkillUnlockedCount(int level)
    {
        for (int loop = (passiveUnlockLevel.Length - 1); loop >= 0; loop--)
            if (level >= passiveUnlockLevel[loop])
                return loop + 1;

        return 0;
    }

    public double GetTotalStageEnemyGold(double enemyGold, double chestGold, double bossGold, float treasureChance, float totalArtifactBonusGoldChance)
    {
        return (((enemyCountInStage * treasureChance * chestGold) + (enemyCountInStage * (1f - treasureChance) * enemyGold)) + bossGold) * ((totalArtifactBonusGoldChance * 10f) + (1f - totalArtifactBonusGoldChance));
    }

}