﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollButton : MonoBehaviour
{
    [SerializeField] private float posY;

    public event Action<float> onButtonClicked;

    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(Button_OnClick);
    }

    public void Button_OnClick()
    {
        onButtonClicked?.Invoke(posY);
    }
}
