﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class TabGroup : MonoBehaviour
{
    [Serializable]
    protected class IntTabButtonDictionary : SerializableDictionary<int, TabButton> { }


    public event Action<int> onActiveTabChanged;

    public const int UNACTIVE_INDEX = -1;


    [SerializeField] [Tooltip("False when use PageWithPageTabGroup")] private bool isAutoInit = true;
    [SerializeField] protected int defaultTabIndex;
    [SerializeField] protected IntTabButtonDictionary tabButtons = new IntTabButtonDictionary();


    public bool IsInit { get; protected set; }

    public int DefaultTabIndex
    {
        get { return defaultTabIndex; }
        set { defaultTabIndex = value; }
    }

    public int ActiveTabIndex { get; protected set; }

    public int PrevActiveTabIndex { get; protected set; }

    public IDictionary<int, TabButton> TabButtons
    {
        get { return tabButtons; }
        protected set { tabButtons.CopyFrom(value); }
    }


    protected virtual void Start()
    {
        if (isAutoInit)
            Init();
    }

    public virtual void Init()
    {
        Init(defaultTabIndex);
    }

    public virtual void Init(int defaultActiveTabIndex)
    {
        if (IsInit)
            return;

        foreach (var keyValuePair in TabButtons)
        {
            keyValuePair.Value.Index = keyValuePair.Key;
            keyValuePair.Value.onClick += TabButton_OnClick;
        }

        IsInit = true;

        ActiveTabIndex = PrevActiveTabIndex = defaultActiveTabIndex;

        SetActiveTab(defaultActiveTabIndex, false);
    }

    public virtual void AddTabButton(int index, TabButton tabButton)
    {
        tabButton.Index = index;
        tabButton.onClick += TabButton_OnClick;

        tabButtons.Add(index, tabButton);
    }

    public void SetFirstActiveTab(bool withNotify = true)
    {
        SetActiveTab(TabButtons.Keys.First(), withNotify);
    }

    public virtual void SetActiveTab(int index, bool withNotify = true)
    {
        PrevActiveTabIndex = ActiveTabIndex;

        ActiveTabIndex = index;

        foreach (var keyValuePair in TabButtons)
            keyValuePair.Value.IsOn = keyValuePair.Key == index;

        if (withNotify)
            onActiveTabChanged?.Invoke(index);
    }

    public virtual void UpdateAllContent()
    {
        foreach (var keyValuePair in TabButtons)
            keyValuePair.Value.UpdateContent();
    }

    //

    protected virtual void TabButton_OnClick(int index)
    {
        if (ActiveTabIndex == index || !tabButtons[index].IsAvailable)
            return;

        //AudioManager.Instance.PlaySound("Sounds/Click2");

        SetActiveTab(index);
    }
}