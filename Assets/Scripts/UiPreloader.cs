﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UiPreloader : MonoBehaviour
{
    public event System.Action onEndInAnimation;
    public event System.Action onEndOutAnimation;

    [SerializeField] Image image;
    [SerializeField] Color inColor;
    [SerializeField] Color outColor;
    [SerializeField] float speed;

    Coroutine animationCoroutine;


    void Awake()
    {
        gameObject.SetActive(false);
    }

    public bool IsAnimation()
    {
        return animationCoroutine != null;
    }

    public bool IsActive()
    {
        return gameObject.activeSelf;
    }

    public void InAnimation()
    {
        gameObject.SetActive(true);

        if (animationCoroutine != null)
            StopCoroutine(animationCoroutine);

        animationCoroutine = StartCoroutine(StartInAnimation());
    }

    public void OutAnimation()
    {
        gameObject.SetActive(true);

        if (animationCoroutine != null)
            StopCoroutine(animationCoroutine);

        animationCoroutine = StartCoroutine(StartOutAnimation());
    }

    IEnumerator StartInAnimation()
    {
        float factor = 0f;

        while (factor < 1f)
        {
            factor += speed * Time.fixedDeltaTime;

            image.color = Color.Lerp(outColor, inColor, factor);

            yield return null;
        }

        animationCoroutine = null;

        if (onEndInAnimation != null)
            onEndInAnimation();

        onEndInAnimation = null;

        yield break;
    }

    IEnumerator StartOutAnimation()
    {
        float factor = 0f;

        while (factor < 1f)
        {
            factor += speed * Time.fixedDeltaTime;

            image.color = Color.Lerp(inColor, outColor, factor);

            yield return null;
        }

        animationCoroutine = null;

        if (onEndOutAnimation != null)
            onEndOutAnimation();

        onEndOutAnimation = null;

        gameObject.SetActive(false);

        yield break;
    }
}