﻿public enum E_CheatType
{
    ObscuredCheat,
    Injection,
    SpeedHack
}