﻿using CodeStage.AntiCheat.ObscuredTypes;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Calculations
{
    public static readonly string[] units = new string[]
    {
            "K",  "M",  "B",  "T",  "aa", "ab", "ac", "ad", "ae", "af",
            "ag", "ah", "ai", "aj", "ak", "al", "am", "an", "ao", "ap",
            "aq", "ar", "as", "at", "au", "av", "aw", "ax", "ay", "az"
    };

    public static string ConvertForShow(this double number)
    {
        int unitsIndex = (((int)Math.Log10(number)) / 3) - 1;
        if (unitsIndex < units.Length && unitsIndex >= 0)
        {
            double num = Math.Round(number / Math.Pow(10, (unitsIndex + 1) * 3), 1);
            return num + units[unitsIndex];
        }
            
        return number.ToString(); 
    }

    public static DateTime ToDateTime(this long ticks)
    {
        return new DateTime(ticks);
    }

    public static DateTime UnixTimeToDateTime(this int unixTime)
    {
        return (new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc).AddSeconds(unixTime));
    }


    public static string ToTimeFormat(this double time)
    {
        return ToTimeFormat((int)time);
    }

    public static string ToTimeFormat(this int time)
    {
        if (time > 3600)
            return string.Format("{0:00}:{1:00}:{2:00}", time / 3600, time % 3600 / 60, time % 60);

        return string.Format("{0:00}:{1:00}", time / 60, time % 60);
    }

    public static string ToTimeFormat(this float time)
    {
        return ToTimeFormat((int)time);
    }

    public static double CalculateOfflineBonus(int offlineDuration, int stage)
    {
        if (offlineDuration < GameConfig.Instance.notificationOfflineBonusTimes[0])
            return 0d;

        float percent = offlineDuration > GameConfig.Instance.offlineTimeLimit ? 1f : (float)((float)offlineDuration / (float)GameConfig.Instance.offlineTimeLimit);

        return offlineDuration * 1;
        //return TotalStageMonsterGold(stage) * GameConfig.Instance.offlineMinModificator * ((float)GameConfig.Instance.offlineMaxModificator * percent);
    }

    public static double GetMoneyPerSecondCount(List<Floor> floors)
    {
        double money = 0;
        for (int i = 0; i < Config.FLOOR_NUMBER; i++)
        {
            money += GetFloorCoins(floors[i].FloorData.BrokersPower, DataCenter.GetBrokersLevel(floors[i].CityId, floors[i].BuildingId, floors[i].FloorData.Id), Config.WORKER_BONUS((E_WorkerTypeId)DataCenter.GetCurrentBrokerType(floors[i].CityId, floors[i].BuildingId, floors[i].FloorData.Id, 0)) * DataCenter.GetBrokerStars(DataCenter.GetCurrentBrokerType(floors[i].CityId, floors[i].BuildingId, floors[i].FloorData.Id, 0), 0), Config.WORKER_BONUS((E_WorkerTypeId)DataCenter.GetCurrentBrokerType(floors[i].CityId, floors[i].BuildingId, floors[i].FloorData.Id, 1)) * DataCenter.GetBrokerStars(DataCenter.GetCurrentBrokerType(floors[i].CityId, floors[i].BuildingId, floors[i].FloorData.Id, 1), 1), Config.WORKER_BONUS((E_WorkerTypeId)DataCenter.GetCurrentBrokerType(floors[i].CityId, floors[i].BuildingId, floors[i].FloorData.Id, 2)) * DataCenter.GetBrokerStars(DataCenter.GetCurrentBrokerType(floors[i].CityId, floors[i].BuildingId, floors[i].FloorData.Id, 2), 2));
        }

        return 0;
    }

    public static double GetFloorCoins(double floorCoins, double brokersLevel, double firstBrokerBonus, double secondBrokerBonus, double thirdBrokerBonus)
    {
        Debug.Log("fffff   + " + floorCoins + "     ssss   + " + brokersLevel + "     ttttt   + " + firstBrokerBonus + "     for   + " + secondBrokerBonus + "     for   + " + thirdBrokerBonus);


        if (brokersLevel < 11)
        {
            Debug.Log(((floorCoins * Math.Pow(1.35, brokersLevel - 1)) * ((firstBrokerBonus + secondBrokerBonus + thirdBrokerBonus + 100) / 100) * Config.BOOST_NUM(DataCenter.GetCurrentBoostId())) + "    HERERERERE");

            return ((floorCoins * Math.Pow(1.35, brokersLevel - 1)) * ((firstBrokerBonus + secondBrokerBonus + thirdBrokerBonus + 100) / 100) * Config.BOOST_NUM(DataCenter.GetCurrentBoostId()));
        }

        if (brokersLevel >= 11 && brokersLevel < 21)
        {
            double tenthFloor = floorCoins * Math.Pow(1.35, 10 - 1);
            Debug.Log(((tenthFloor * Math.Pow(1.25, brokersLevel - 11)) * ((firstBrokerBonus + secondBrokerBonus + thirdBrokerBonus + 100) / 100) * Config.BOOST_NUM(DataCenter.GetCurrentBoostId())) + "    HERERERERE");
            return ((tenthFloor * Math.Pow(1.25, brokersLevel - 11)) * ((firstBrokerBonus + secondBrokerBonus + thirdBrokerBonus + 100) / 100) * Config.BOOST_NUM(DataCenter.GetCurrentBoostId()));
        }

        if (brokersLevel >= 21 && brokersLevel < 41)
        {
            double tenthFloor = floorCoins * Math.Pow(1.35, 10 - 1);
            double twentiethFloor = tenthFloor * Math.Pow(1.25, 10 - 1);

            Debug.Log(((twentiethFloor * Math.Pow(1.15, brokersLevel - 21)) * ((firstBrokerBonus + secondBrokerBonus + thirdBrokerBonus + 100) / 100) * Config.BOOST_NUM(DataCenter.GetCurrentBoostId())) + "    HERERERERE");
            return ((twentiethFloor * Math.Pow(1.15, brokersLevel - 21)) * ((firstBrokerBonus + secondBrokerBonus + thirdBrokerBonus + 100) / 100) * Config.BOOST_NUM(DataCenter.GetCurrentBoostId()));
        }

        if (brokersLevel >= 41)
        {
            double tenthFloor = floorCoins * Math.Pow(1.35, 10 - 1);
            double twentiethFloor = tenthFloor * Math.Pow(1.25, 10 - 1);
            double fortieth = twentiethFloor * Math.Pow(1.15, 10 - 1);

            Debug.Log(((fortieth * Math.Pow(1.03, brokersLevel - 41)) * ((firstBrokerBonus + secondBrokerBonus + thirdBrokerBonus + 100) / 100) * Config.BOOST_NUM(DataCenter.GetCurrentBoostId())) + "    HERERERERE");
            return ((fortieth * Math.Pow(1.03, brokersLevel - 41)) * ((firstBrokerBonus + secondBrokerBonus + thirdBrokerBonus + 100) / 100) * Config.BOOST_NUM(DataCenter.GetCurrentBoostId()));
        }

        return 0;
    }

    public static float GetFloorEarnTime(double earnTime, double managerLevel, double managerBonus)
    {
        float sds = Convert.ToInt32(earnTime * Math.Pow(0.992, managerLevel - 1 + managerBonus));
        Debug.Log(earnTime * Math.Pow(0.992, managerLevel - 1 + managerBonus) + "    earnTime     " + managerLevel + "      managerLevel     " + managerBonus + "      managerBonus     " + sds + "        TIME");

        return sds;
    }


}
