﻿using UnityEngine;

public class MainPage : Page
{
    [SerializeField] private BottomMenu bottomMenu;
    [SerializeField] private PaginalScrollRect paginalScrollRect;
    [SerializeField] private Page[] pages;
    [SerializeField] private HomePage homePage;


    private void OnEnable()
    {
        Screen.sleepTimeout = SleepTimeout.SystemSetting;

        bottomMenu.onTabButtonClicked += BottomMenu_OnTabButtonClicked;
        paginalScrollRect.onPageStoped += PaginalScrollRect_OnPageStoped;

    }

    private void OnDisable()
    {
        bottomMenu.onTabButtonClicked -= BottomMenu_OnTabButtonClicked;
        paginalScrollRect.onPageStoped -= PaginalScrollRect_OnPageStoped;

    }

    public override void Init()
    {
        base.Init();
    }

    public override void Show()
    {
        base.Show();

        for (int loop = 0; loop < pages.Length; loop++)
        {
            pages[loop].Show();
            pages[loop].OnUnFocus();
        }

        bottomMenu.Show();
    }

    public override void Hide()
    {
        base.Hide();

        bottomMenu.Hide();
    }

    public override void UpdateContent()
    {
        base.UpdateContent();

        pages[paginalScrollRect.CurrPageIndex].UpdateContent();
    }

    public override void OnFocus()
    {
        base.OnFocus();

        pages[paginalScrollRect.CurrPageIndex].OnFocus();
    }

    public override void OnUnFocus()
    {
        base.OnUnFocus();

        pages[paginalScrollRect.CurrPageIndex].OnUnFocus();
    }

    //

    private void BottomMenu_OnTabButtonClicked(int index)
    {
        if (paginalScrollRect.CurrPageIndex == index)
            (pages[index] as PageWithPageTabGroup)?.ShowNextTabPage();
    }

    private void PaginalScrollRect_OnPageStoped(int pageIndex)
    {
        if (paginalScrollRect.PrevPageIndex == pageIndex)
            return;

        pages[paginalScrollRect.PrevPageIndex].OnUnFocus();
        pages[paginalScrollRect.CurrPageIndex].OnFocus();
    }


    private void HomePage_OnBattleSeasonButtonClicked()
    {
        paginalScrollRect.ShowPage(4);

        (pages[4] as PageWithPageTabGroup).ShowTabPage(2);
    }
}