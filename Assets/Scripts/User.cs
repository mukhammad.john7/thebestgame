﻿using CodeStage.AntiCheat.ObscuredTypes;

public class User
{
    public static int Id
    {
        get { return ObscuredPrefs.GetInt("user_id", 0); }
        set { ObscuredPrefs.SetInt("user_id", value); }
    }

    public static string SessionKey
    {
        get { return ObscuredPrefs.GetString("user_session_key", ""); }
        set { ObscuredPrefs.SetString("user_session_key", value); }
    }

    public static string Nickname
    {
        get { return ObscuredPrefs.GetString("user_name", ""); }
        set { ObscuredPrefs.SetString("user_name", value); }
    }

    public static string SocialName
    {
        get { return ObscuredPrefs.GetString("user_social_name", ""); }
        set { ObscuredPrefs.SetString("user_social_name", value); }
    }

    public static bool IsGuest
    {
        get { return ObscuredPrefs.GetBool("user_is_guest", false); }
        set { ObscuredPrefs.SetBool("user_is_guest", value); }
    }

    public static string SocialType
    {
        get { return ObscuredPrefs.GetString("user_social_type", ""); }
        set { ObscuredPrefs.SetString("user_social_type", value); }
    }

    public static string SocialToken
    {
        get { return ObscuredPrefs.GetString("user_social_token", ""); }
        set { ObscuredPrefs.SetString("user_social_token", value); }
    }

    public static string Email
    {
        get { return ObscuredPrefs.GetString("user_email", ""); }
        set { ObscuredPrefs.SetString("user_email", value); }
    }

    public static string LastAppVersion
    {
        get { return ObscuredPrefs.GetString("last_app_version", ""); }
        set { ObscuredPrefs.SetString("last_app_version", value); }
    }

    public static int RateApp
    {
        get { return ObscuredPrefs.GetInt("rate_app", 0); }
        set { ObscuredPrefs.SetInt("rate_app", value); }
    }

    public static long NextRateUsShowTime
    {
        get { return ObscuredPrefs.GetLong("next_rate_us_show_time", 0); }
        set { ObscuredPrefs.SetLong("next_rate_us_show_time", value); }
    }

    public static bool IsCollectStarterPack
    {
        get { return ObscuredPrefs.GetBool("is_collect_starter_pack", false); }
        set { ObscuredPrefs.SetBool("is_collect_starter_pack", value); }
    }

    public static bool IsChatBanned
    {
        get { return ObscuredPrefs.GetBool("chat_banned"); }
        set { ObscuredPrefs.SetBool("chat_banned", value); }
    }

    public static bool IsPrivacyAccepted
    {
        get { return ObscuredPrefs.GetBool("privacy_accepted"); }
        set { ObscuredPrefs.SetBool("privacy_accepted", value); }
    }
}
