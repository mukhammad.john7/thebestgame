﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WindowManager : MonoBehaviour
{
    Window[] windows = new Window[0];

    List<Window> turnWindows = new List<Window>();


    public static WindowManager Instance { get; private set; }


    void OnDisable()
    {
        Instance = null;
    }

    void Awake()
    {
        Instance = this;

        windows = Resources.FindObjectsOfTypeAll<Window>();

        for (int loop = 0; loop < windows.Length; loop++)
            windows[loop].gameObject.SetActive(false);
    }

    public T GetWindow<T>() where T : Window
    {
        for (int loop = 0; loop < windows.Length; loop++)
            if (windows[loop] is T)
                return (T)windows[loop];

        Debug.LogError("Window not found: " + default(T));
        return default(T);
    }

    public void AddWindowToTurn(Window value)
    {
        if (!turnWindows.Contains(value))
            turnWindows.Add(value);
    }

    public void RemoveWindowOfTurn(Window value)
    {
        if (turnWindows.Contains(value))
            turnWindows.Remove(value);
    }

    public int TurnCount()
    {
        return turnWindows.Count;
    }

    public Window CurrentWindow()
    {
        return (turnWindows.Count > 0) ? turnWindows[turnWindows.Count - 1] : null;
    }

    public void HideAllWindows()
    {
        for (int loop = 0; loop < turnWindows.Count; loop++)
            if (turnWindows[loop].gameObject != null)
                turnWindows[loop].Hide(false);

        turnWindows.Clear();
    }
}