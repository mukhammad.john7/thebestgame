﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingButton : MonoBehaviour
{
    public event Action<int,int> onButtonClicked;

    [SerializeField] private int cityId;
    [SerializeField] private int buildingId;

    private void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(Button_OnClick);
    }

    public void Button_OnClick()
    {
        DataCenter.CurrentCity = cityId;
        DataCenter.CurrentBuilding = buildingId;
        SceneController.Instance.LoadScene(Config.SCENE_GAME_PATH);
    }
}
