﻿using CodeStage.AntiCheat.ObscuredTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class DataCenter : SingletonResources<DataCenter>
{
    public static event Action<long, string, string, string> onCrystalsChangedAnalytics;
    public static event Action<long, string, string, string> onFragmetsChangedAnalytics;
    public static event Action onStuffChanged;
    public static event Action onWorkersChanged;
    public static event Action onManagerChanged;
    public static event Action onWorkerLevelChanged;
    public static event Action onBoostTimeChanged;
    public static event Action onCurrentBoostChanged;


    public static bool IsCheckOfflineBonus { get; set; }

    public static double OfflineBonusGold { get; set; }

    public static int CurrentCity
    {
        get { return ObscuredPrefs.GetInt("current_city", 0); }
        set { ObscuredPrefs.SetInt("current_city", value); }
    }

    public static int CurrentBuilding
    {
        get { return ObscuredPrefs.GetInt("current_building", 0); }
        set { ObscuredPrefs.SetInt("current_building", value); }
    }

    public static bool GetBuildingStatusWithId(int cityId, int buildingId)
    {
        return ObscuredPrefs.GetBool("city_" + cityId + "building_" + buildingId, false);
    }

    public static void SetBuildingActiveWithId(int cityId, int buildingId)
    {
        ObscuredPrefs.SetBool("city_" + cityId + "building_" + buildingId, true);
    }

    // money

    public static double GetMoneyNum(int cityId, int buildingId)
    {
        return ObscuredPrefs.GetDouble("city_" + cityId + "building_" + buildingId + "money", 0);
    }

    public static void AddMoney(int cityId, int buildingId, double count)
    {
        ChangeMoney(cityId, buildingId, count);
    }

    public static void SubtractMoney(int cityId, int buildingId, double count)
    {
        ChangeMoney(cityId, buildingId, -count);
    }

    private static void ChangeMoney(int cityId, int buildingId, double count)
    {
        ObscuredPrefs.SetDouble("city_" + cityId + "building_" + buildingId + "money", GetMoneyNum(cityId, buildingId) + count);
        onStuffChanged?.Invoke();
    }

    // stuff

    public static double GetStuffNum(E_StuffTypeId stuffTypeId)
    {
        return ObscuredPrefs.GetDouble("stuff_" + stuffTypeId, 0);
    }

    public static void AddStuff(E_StuffTypeId stuffTypeId, double count)
    {
        ChangeStuff(stuffTypeId, count);
    }

    public static void SubtractStuff(E_StuffTypeId stuffTypeId, double count)
    {
        ChangeStuff(stuffTypeId, -count);
    }

    private static void ChangeStuff(E_StuffTypeId stuffTypeId, double count)
    {
        ObscuredPrefs.SetDouble("stuff_" + stuffTypeId, GetStuffNum(stuffTypeId) + count);
        onStuffChanged?.Invoke();
    }

    // Boost 

    public static double GetBoost(int boostId)
    {
        return ObscuredPrefs.GetDouble("stuff_" + "_boost_" + "_boostId_" + boostId, 0);
    }

    public static void SetLocalBoost(int boostId, double num)
    {
        ObscuredPrefs.SetDouble("stuff_" + "_boost_" + "_boostId_" + boostId, num);
    }

    public static void AddBoost(int boostId, double num)
    {
        ObscuredPrefs.SetDouble("stuff_" + "_boost_" + "_boostId_" + boostId, GetBoost(boostId) + num);
    }


    // Boost Time

    public static long GetBoostTime(int boostId)
    {
        return ObscuredPrefs.GetLong("stuff_" + "_boostTime_" + "_boostId_" + boostId, 0);
    }

    public static void SetBoostTime(int boostId, long time)
    {
        ObscuredPrefs.SetLong("stuff_" + "_boostTime_" + "_boostId_" + boostId, time);
        onBoostTimeChanged?.Invoke();
    }

    // Boost id

    public static int GetCurrentBoostId()
    {
        return ObscuredPrefs.GetInt("stuff_" + "_boostId", 0);
    }

    public static void SetCurrentBoostId(int id)
    {
        ObscuredPrefs.SetInt("stuff_" + "_boostId", id);
        onCurrentBoostChanged?.Invoke();
    }

    // Diamonds

    public static double GetDiamonds()
    {
        return ObscuredPrefs.GetDouble("stuff_" + "_diamonds", 0);
    }

    public static void SetLocalDiamonds(double num)
    {
        ObscuredPrefs.SetDouble("stuff_" + "_diamonds", num);
    }

    public static void AddDiamonds(double num)
    {
        ObscuredPrefs.SetDouble("stuff_" + "_diamonds", GetDiamonds() + num);
        onStuffChanged?.Invoke();
    }

    // Coins


    public static double GetCoins(int cityId, int buildingId)
    {
        return ObscuredPrefs.GetDouble("stuff_" + "_coins_" + "_cityId_" + cityId + "_buildingId_" + buildingId, 0);
    }

    public static void SetLocalCoins(int cityId, int buildingId, double num)
    {
        ObscuredPrefs.SetDouble("stuff_" + "_coins_" + "_cityId_" + cityId + "_buildingId_" + buildingId, num);
        onStuffChanged?.Invoke();
    }

    public static void AddCoins(int cityId, int buildingId, double num)
    {
        ObscuredPrefs.SetDouble("stuff_" + "_coins_" + "_cityId_" + cityId + "_buildingId_" + buildingId, GetCoins(cityId, buildingId) + num);
        onStuffChanged?.Invoke();
    }

    // Floor state

    public static int GetFloorStateWithId(int city, int building, int floorNumber)
    {
        return ObscuredPrefs.GetInt("city_" + city + "building_" + building + "_floor_" + floorNumber, 1);
    }

    public static void SetFloorStateWithId(int city, int building, int floorNumber, int floorState)
    {
        ObscuredPrefs.SetInt("city_" + city + "building_" + building + "_floor_" + floorNumber, floorState);
    }

    // Current worker

    public static int GetCurrentManagerType(int city, int building, int floorNumber)
    {
        return ObscuredPrefs.GetInt("currentManagerType" + "city_" + city + "building_" + building + "_floor_" + floorNumber, 0);
    }

    public static void SetCurrentManagerType(int city, int building, int floorNumber, int managerType)
    {
        ObscuredPrefs.SetInt("currentManagerType" + "city_" + city + "building_" + building + "_floor_" + floorNumber, managerType);
    }



    public static int GetCurrentBrokerType(int city, int building, int floorNumber, int brokerId)
    {
        return ObscuredPrefs.GetInt("currentBrokerType" + "city_" + city + "building_" + building + "_floor_" + floorNumber + "_brokerId_" + brokerId, 0);
    }

    public static void SetCurrentBrokerType(int city, int building, int floorNumber, int brokerId, int brokerType)
    {
        ObscuredPrefs.SetInt("currentBrokerType" + "city_" + city + "building_" + building + "_floor_" + floorNumber + "_brokerId_" + brokerId, brokerType);
    }


    // BrokerLevel


    public static double GetBrokersLevel(int cityId, int buildingId, int floorId)
    {
        return ObscuredPrefs.GetDouble("brokerLevel" + "_cityId_" + cityId + "_buildingId_" + buildingId + "_floorId_" + floorId, 0);
    }

    public static void SetLocalBrokersLevel(int cityId, int buildingId, int floorId, double num)
    {
        ObscuredPrefs.SetDouble("brokerLevel" + "_cityId_" + cityId + "_buildingId_" + buildingId + "_floorId_" + floorId, num);
    }

    public static void AddBrokersLevel(int cityId, int buildingId, int floorId, double num)
    {
        ObscuredPrefs.SetDouble("brokerLevel" + "_cityId_" + cityId + "_buildingId_" + buildingId + "_floorId_" + floorId, GetBrokersLevel(cityId, buildingId, floorId) + num);
        onWorkerLevelChanged?.Invoke();
    }

    // ManagerLevel

    public static double GetManagerLevel(int cityId, int buildingId, int floorId, int typeId)
    {
        return ObscuredPrefs.GetDouble("managerLevel" + "_cityId_" + cityId + "_buildingId_" + buildingId + "_floorId_" + floorId + "_typeId_" + typeId, 0);
    }

    public static void SetLocalManagerLevel(int cityId, int buildingId, int floorId, int typeId, double num)
    {
        ObscuredPrefs.SetDouble("managerLevel" + "_cityId_" + cityId + "_buildingId_" + buildingId + "_floorId_" + floorId + "_typeId_" + typeId, num);
        onManagerChanged?.Invoke();
    }

    public static void AddManagerLevel(int cityId, int buildingId, int floorId, int typeId, double num)
    {
        ObscuredPrefs.SetDouble("managerLevel" + "_cityId_" + cityId + "_buildingId_" + buildingId + "_floorId_" + floorId + "_typeId_" + typeId, GetManagerLevel(cityId, buildingId, floorId, typeId) + num);
        onWorkerLevelChanged?.Invoke();
        onManagerChanged?.Invoke();
    }


    // Manager state

    public static bool GetManagerState(int typeId, int managerId)
    {
        return ObscuredPrefs.GetBool("typeId_" + typeId + "managerId_" + managerId, false);
    }

    public static int GetManagerLevelWithId(int cityId, int buildingId, int managerId)
    {
        return ObscuredPrefs.GetInt("cityId_" + cityId + "building_" + buildingId + "_manager_" + managerId + "_level", 0);
    }

    public static void AddManagerLevelWithId(int cityId, int buildingId, int managerId, int levelCount)
    {
        ObscuredPrefs.SetInt("cityId_" + cityId + "building_" + buildingId + "_manager_" + managerId + "_level", GetManagerLevelWithId(cityId, buildingId, managerId) + levelCount);
        onManagerChanged?.Invoke();
    }

    public static int GetManagerStars(int typeId, int managerId)
    {
        return ObscuredPrefs.GetInt("typeId_" + typeId + "managerId_" + managerId, 1);
    }

    public static void SetManagerStars(int typeId, int managerId, int num)
    {
        ObscuredPrefs.SetInt("typeId_" + typeId + "managerId_" + managerId, num);
    }

    public static void AddManagerStars(int typeId, int managerId)
    {
        ObscuredPrefs.SetInt("typeId_" + typeId + "managerId_" + managerId, GetManagerStars(typeId, managerId) + 1);
        onWorkersChanged?.Invoke();

    }

    public static bool GetBrokerState(int typeId, int brokerId)
    {
        return ObscuredPrefs.GetBool("typeId_" + typeId + "brokerId" + brokerId, false);
    }

    public static int GetBrokerStars(int typeId, int brokerId)
    {
        return ObscuredPrefs.GetInt("typeId_" + typeId + "_brokerId_" + brokerId, 0);
    }


    public static void SetLocalBrokerStars(int typeId, int brokerId, int num)
    {
        ObscuredPrefs.SetInt("typeId_" + typeId + "_brokerId_" + brokerId, num);
    }

    public static void AddBrokerStars(int typeId, int brokerId)
    {
        ObscuredPrefs.SetInt("typeId_" + typeId + "_brokerId_" + brokerId, GetBrokerStars(typeId, brokerId) + 1);
        onWorkersChanged?.Invoke();
    }

    //Broker Cards

    public static double GetBrokerCards(int typeId, int brokerId)
    {
        return ObscuredPrefs.GetDouble("brokerCards_typeId_" + typeId + "_brokerId_" + brokerId, 0);
    }


    public static void SetLocalBrokerCards(int typeId, int brokerId, double num)
    {
        ObscuredPrefs.SetDouble("brokerCards_typeId_" + typeId + "_brokerId_" + brokerId, num);
    }

    public static void SetBrokerCards(int typeId, int brokerId, double num)
    {
        ObscuredPrefs.SetDouble("brokerCards_typeId_" + typeId + "_brokerId_" + brokerId, num);
        SaveData.Instance.SetBrokerCards(typeId, brokerId, num);
        onWorkersChanged?.Invoke();
    }


    public static void AddBrokerCards(int typeId, int brokerId, double cardNum)
    {
        ObscuredPrefs.SetDouble("brokerCards_typeId_" + typeId + "_brokerId_" + brokerId, GetBrokerCards(typeId, brokerId) + cardNum);
        onWorkersChanged?.Invoke();
    }


    //Manager Cards

    public static double GetManagerCards(int typeId, int managerId)
    {
        return ObscuredPrefs.GetDouble("managerCards_typeId_" + typeId + "_managerId_" + managerId, 0);
    }


    public static void SetLocalManagerCards(int typeId, int managerId, double num)
    {
        ObscuredPrefs.SetDouble("managerCards_typeId_" + typeId + "_managerId_" + managerId, num);
    }

    public static void SetManagerCards(int typeId, int managerId, double num)
    {
        ObscuredPrefs.SetDouble("managerCards_typeId_" + typeId + "_managerId_" + managerId, num);
        SaveData.Instance.SetManagerCards(typeId, managerId, num);
        onWorkersChanged?.Invoke();
    }


    public static void AddManagerCards(int typeId, int managerId, double cardsNum)
    {
        ObscuredPrefs.SetDouble("managerCards_typeId_" + typeId + "_managerId_" + managerId, GetManagerCards(typeId, managerId) + cardsNum);
        onWorkersChanged?.Invoke();
    }


    //


    public static long OfflineTime
    {
        get { return ObscuredPrefs.GetLong("offline_time", 0L); }
        set { ObscuredPrefs.SetLong("offline_time", value); }
    }

    public bool CheckIsBoostActive(int boostId)
    {
        if (GetBoostTimeSeconds(boostId) > 0)
        {
            return true;
        }

        else
        {
            return false;
        }
    }

    public int GetBoostTimeSeconds(int boostId)
    {
        if (GetBoostTime(boostId) > 0)
        {
            TimeSpan difference = GetBoostTime(boostId).ToDateTime().Subtract(GetNetworkTime());

            int timeInSeconds = (int)difference.TotalSeconds;

            if (timeInSeconds > 0)
            {
                // TODO проблема преобразования double в int когда привышает макс
                if (timeInSeconds == int.MinValue)
                    timeInSeconds = int.MaxValue;

                return timeInSeconds;
            }

            else
            {
                return 0;
            }

        }

        else
        {
            return 0;
        }
        
    }
    public int GetOfflineTimeSeconds()
    {
        TimeSpan difference = GetNetworkTime().Subtract(OfflineTime.ToDateTime());

        int timeInSeconds = (int)difference.TotalSeconds;

        // TODO проблема преобразования double в int когда привышает макс
        if (timeInSeconds == int.MinValue)
            timeInSeconds = int.MaxValue;

        return timeInSeconds;
    }


    public static DateTime GetNetworkTime()
    {
        const string ntpServer = "time.windows.com";
        var ntpData = new byte[48];
        ntpData[0] = 0x1B;

        var addresses = Dns.GetHostEntry(ntpServer).AddressList;
        var ipEndPoint = new IPEndPoint(addresses[0], 123);
        using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
        {
            socket.Connect(ipEndPoint);
            for (int i = 1; i < 48; i++)
                ntpData[i] = 0;

            socket.Send(ntpData);
            socket.Receive(ntpData);

            byte offsetTransmitTime = 40;
            ulong intpart = 0;
            ulong fractpart = 0;

            for (int i = 0; i <= 3; i++)
                intpart = 256 * intpart + ntpData[offsetTransmitTime + i];

            for (int i = 4; i <= 7; i++)
                fractpart = 256 * fractpart + ntpData[offsetTransmitTime + i];

            ulong milliseconds = (intpart * 1000 + (fractpart * 1000) / 0x100000000L);
            socket.Close();

            TimeSpan timeSpan = TimeSpan.FromTicks((long)milliseconds * TimeSpan.TicksPerMillisecond);

            DateTime dateTime = new DateTime(1900, 1, 1);
            dateTime += timeSpan;

            TimeSpan offsetAmount = TimeZone.CurrentTimeZone.GetUtcOffset(dateTime);
            DateTime networkDateTime = (dateTime + offsetAmount);

            return networkDateTime;
        }
    }

}
