﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameResources : MonoBehaviour
{
    public static Sprite GetFloorSprite(int cityId, int buildingId, int spriteId)
    {
        return Resources.Load<Sprite>(string.Format("Sprites/Floors/City_{0}/Building_{1}/Floor_Background_{2}", cityId, buildingId, spriteId));
    }


    public static FloorData GetFloorData(int cityId, int buildingId, int floorId)
    {
        return Resources.Load<FloorData>(string.Format("ScriptableObjects/City_{0}/Building_{1}/FloorData_{2}", cityId, buildingId, floorId));
    }

    public static Sprite GetDirectionSprite(int spriteId)
    {
        return Resources.Load<Sprite>(string.Format("Sprites/Floors/Direction_{0}", spriteId));
    }

    public static Manager GetManagerPrefab(int typeId, int managerId)
    {
        return Resources.Load<Manager>(string.Format("Prefabs/Managers/Type_{0}/Manager_{1}", typeId, managerId));
    }

    public static GameObject GetBrokerPrefab(int typeId, int brokerId)
    {
        return Resources.Load<GameObject>(string.Format("Prefabs/Brokers/Type_{0}/Broker_{1}", typeId, brokerId));
    }

    public static WorkerInfo GetWorkerPrefab()
    {
        return Resources.Load<WorkerInfo>("Prefabs/Card/Worker");
    }

    public static Sprite GetWorkerSprite(int workerTypeId, int workerState, string worker, int workerId)
    {
        return Resources.Load<Sprite>(string.Format("Sprites/Cards/Type_{0}/{1}/{2}_{3}", workerTypeId, workerState, worker, workerId));
    }

    public static Sprite GetTableSprite(int cityId, int buildingId)
    {
        return Resources.Load<Sprite>(string.Format("Sprites/Floors/City_{0}/Building_{1}/Table", cityId, buildingId));
    }

    public static Sprite GetChestSprite(int chestId, int stateId)
    {
        return Resources.Load<Sprite>(string.Format("Sprites/Chests/Chest_{0}/State_{1}", chestId, stateId));
    }

    public static Sprite GetBoostSprite(int boostId)
    {
        return Resources.Load<Sprite>(string.Format("Sprites/BoostIcon/boostIcon_{0}", boostId));
    }
}
