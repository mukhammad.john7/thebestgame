﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{
    private int cityId;
    private int buildingId;
    private int floorId;
    [SerializeField] private Image[] images;
    [SerializeField] private Animator animator;

    public static event Action onShowLeftBar;
    public static event Action onSkipLeftBar;

    private void OnEnable()
    {
        DataCenter.onManagerChanged += UpdateManagerInfo;
    }

    private void OnDisable()
    {
        DataCenter.onManagerChanged -= UpdateManagerInfo;
    }

    public void Init(int cityId, int buildingId, int floorId)
    {
        this.cityId = cityId;
        this.buildingId = buildingId;
        this.floorId = floorId;
        UpdateManagerInfo();
    }

    private void UpdateManagerInfo()
    {
        //managerLevelText.text = "Level " + DataCenter.GetManagerLevelWithId(AreaId, FloorData.Id);

        if (DataCenter.GetManagerLevel(cityId, buildingId, floorId, DataCenter.GetCurrentManagerType(cityId, buildingId, floorId)) == 0)
        {
            animator.enabled = false;
            for (int i = 0; i < images.Length; i++)
            {
                images[i].color = new Vector4(0f, 0f, 0f, 255f);
            }
        }

        else
        {
            animator.enabled = true;
            for (int i = 0; i < images.Length; i++)
            {
                images[i].color = new Vector4(255f, 255f, 255f, 255f);
            }
        }
    }

    public void OnButtonClick()
    {
        onSkipLeftBar?.Invoke();

        ManagerWindow managerWindow = WindowManager.Instance.GetWindow<ManagerWindow>();

        managerWindow.onClickCloseButton = () =>
        {
            managerWindow.Hide();
            onShowLeftBar?.Invoke();
        };

        managerWindow.Show(cityId, buildingId, floorId);
        
    }
}
