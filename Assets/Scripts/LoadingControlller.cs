﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LoadingControlller : SingletonMonoBehaviour<LoadingControlller>
{
    private const float SOCIAL_TIMEOUT = 120f;

    [SerializeField] UiProgressBar uiProgressBar;
    [SerializeField] Text dowloadingText;

    [SerializeField] GameObject saveData;

    public static event Action<string> onResourcesUnpacked;
    public static event Action<string> onGameLoggined;
    public static event Action<string> onGameEntered;
    Firebase.Auth.FirebaseAuth auth;
    Firebase.Auth.FirebaseUser user;
    private void Start()
    {


        InitializeFirebase();
    }

    void InitializeFirebase()
    {
        Debug.Log("Setting up Firebase Auth");
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        auth.StateChanged += AuthStateChanged;
        //auth.SignOut();
        AuthStateChanged(this, null);
    }

    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        Debug.Log("vvvvvvvvv  " + user);

        if (auth.CurrentUser != user)
        {

            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;

            if (!signedIn)
            {
                Debug.Log("Signed out " + user.UserId);

            }
            user = auth.CurrentUser;
            if (signedIn)
            {
                Debug.Log("Signed шт " + user.UserId);
                StartSaveData();
            }

        }
        else if (auth.CurrentUser == null)
        {
            LoginWindow loginWindow = WindowManager.Instance.GetWindow<LoginWindow>();

            loginWindow.onClickCloseButton = () =>
            {
                loginWindow.Hide();
            };
            loginWindow.Show();
        }
    }

    public void StartSaveData()
    {
        saveData.SetActive(true);
        SceneController.Instance.LoadScene(Config.SCENE_GAME_PATH);
    }

    void OnDestroy()
    {
        auth.StateChanged -= AuthStateChanged;
        auth = null;
    }

}
