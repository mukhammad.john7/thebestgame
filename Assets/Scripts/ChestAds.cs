﻿using CodeStage.AntiCheat.ObscuredTypes;
using System;
using UnityEngine;
using GoogleMobileAds.Api;
using System.Collections;

public class ChestAds : SingletonMonoBehaviour<ChestAds>
{
    [SerializeField] private E_ChestTypeId chesType;
    [SerializeField] private ChestController chestSpace;

    private RewardedAd rewardedAd;

    public void Start()
    {
        MobileAds.Initialize(initStatus => { });
        CreateAndLoadRewardedAd();
    }

    public void CreateAndLoadRewardedAd()
    {
    #if UNITY_ANDROID
        string adUnitId = "ca-app-pub-8631920722470278/1965358879";
#elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-8631920722470278/7189765397";
#else
            string adUnitId = "unexpected_platform";
#endif

        this.rewardedAd = new RewardedAd(adUnitId);

        this.rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
        this.rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
        this.rewardedAd.OnAdClosed += HandleRewardedAdClosed;

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded ad with the request.
        this.rewardedAd.LoadAd(request);
    }

    public void ShowRewardedAd()
    {
        if (this.rewardedAd.IsLoaded())
        {
            this.rewardedAd.Show();
        }
    }

    public void HandleRewardedAdClosed(object sender, EventArgs args)
    {
        this.CreateAndLoadRewardedAd();
    }
    public void HandleRewardedAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdLoaded event received");
    }

    public void HandleRewardedAdFailedToLoad(object sender, AdErrorEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardedAdFailedToLoad event received with message: "
                             + args.Message);
    }

    public void HandleRewardedAdOpening(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdOpening event received");
    }

    public void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardedAdFailedToShow event received with message: "
                             + args.Message);
    }


    public void HandleUserEarnedReward(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        MonoBehaviour.print(
            "HandleRewardedAdRewarded event received for "
                        + amount.ToString() + " " + type);

        StartCoroutine(ShowChest());
    }

    IEnumerator ShowChest()
    {
        yield return new WaitForSeconds(0.2f);
        chestSpace.gameObject.SetActive(true);
        chestSpace.Init(chesType);
    }

}