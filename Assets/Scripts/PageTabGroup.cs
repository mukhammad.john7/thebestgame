﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class PageTabGroup : TabGroup
{
    [Serializable]
    protected class IntPageDictionary : SerializableDictionary<int, Page> { }

    [SerializeField] protected IntPageDictionary pages;


    public IDictionary<int, Page> Pages
    {
        get { return pages; }
        private set { pages.CopyFrom(value); }
    }

    public Page ActivePage => Pages[ActiveTabIndex];

    public Page PrevPage => Pages[PrevActiveTabIndex];


    public override void SetActiveTab(int index, bool withNotify = true)
    {
        if (!TabButtons[index].IsAvailable)
        {
            Debug.LogWarning("Enabled TabButton index: " + index);
            return;
        }

        foreach (var keyValuePair in Pages)
            if (keyValuePair.Key == index)
            {
                if (keyValuePair.Value.IsInit && keyValuePair.Value.IsShow)
                    continue;

                keyValuePair.Value.Show();
            }
            else
            {
                if (keyValuePair.Value.IsShow)
                    keyValuePair.Value.Hide();
            }

        base.SetActiveTab(index, withNotify);
    }
}