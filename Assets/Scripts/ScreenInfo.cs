﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenInfo : Singleton<ScreenInfo>
{
    public const float MAIN_WIDTH = 1080f;
    public const float MAIN_HEIGHT = 1920f;
    public const float MAIN_ASPECT = 0.562f;


    public float OrthographicSize { get; private set; }

    public bool IsPhone { get; private set; }

    public bool IsSuperWide { get; private set; }

    public float Margin { get; private set; }

    public float ScreenAspect { get; private set; }

    public float ScreenScale { get; private set; }


    public ScreenInfo()
    {
        ScreenAspect = (float)System.Math.Round((double)Screen.width / (double)Screen.height, 3);

        if (ScreenAspect > MAIN_ASPECT)
        {
            IsPhone = false;
            IsSuperWide = false;

            ScreenScale = Screen.height / MAIN_HEIGHT;
            Margin = 0f;
            OrthographicSize = MAIN_HEIGHT / 2f;
        }
        else if (ScreenAspect < MAIN_ASPECT)
        {
            IsPhone = true;
            IsSuperWide = true;

            ScreenScale = Screen.width / MAIN_WIDTH;
            Margin = (Screen.height / ScreenScale - MAIN_HEIGHT) / 2f;
            OrthographicSize = Screen.height / ScreenScale / 2f;
        }
        else
        {
            IsPhone = true;
            IsSuperWide = false;

            ScreenScale = Screen.height / MAIN_HEIGHT;
            Margin = 0f;
            OrthographicSize = MAIN_HEIGHT / 2f;
        }

#if UNITY_EDITOR || DEVELOPMENT_BUILD
        Debug.LogFormat("ScreenAspect: {0} IsPhone: {1} IsSuperWide: {2} Margin: {3} ScreenAspect: {4} ScreenScale: {5}", ScreenAspect, IsPhone, IsSuperWide, Margin, ScreenAspect, ScreenScale);
#endif
    }
}
