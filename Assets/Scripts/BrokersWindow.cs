﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrokersWindow : Window
{
    public System.Action onClickCloseButton;
    public System.Action onClickBuyButton;

    //[SerializeField] UiTabs uiTabs;
    [SerializeField] Text firstBonusText;
    [SerializeField] Text firstWorkerNameText;
    [SerializeField] Text firstLevelText;

    [SerializeField] Text secondBonusText;
    [SerializeField] Text secondWorkerNameText;
    [SerializeField] Text secondLevelText;

    [SerializeField] Text thirdBonusText;
    [SerializeField] Text thirdWorkerNameText;
    [SerializeField] Text thirdLevelText;

    [SerializeField] Text potionsText;
    [SerializeField] Text fragmentsText;
    [SerializeField] Button upgradeButton;
    [SerializeField] Text upGradeText;
    [SerializeField] WorkerInfo firstSelectedWorker;
    [SerializeField] WorkerInfo secondSelectedWorker;
    [SerializeField] WorkerInfo thirdSelectedWorker;

    [SerializeField] List<WorkerInfo> firstAllWorkers;
    [SerializeField] List<WorkerInfo> secondAllWorkers;
    [SerializeField] List<WorkerInfo> thirdAllWorkers;

    [SerializeField] GameObject firstPage;
    [SerializeField] GameObject secondPage;
    [SerializeField] GameObject thirdPage;

    private int CityId;
    private int BuildingId;
    private int FloorId;


    private void OnEnable()
    {
        WorkerInfo.onSelectWorkerChanged += UpgradeInfo;
        DataCenter.onWorkerLevelChanged += UpgradeInfo;
    }

    private void OnDisable()
    {
        WorkerInfo.onSelectWorkerChanged -= UpgradeInfo;
        DataCenter.onWorkerLevelChanged -= UpgradeInfo;
    }

    public void Show(int cityId, int buildingId, int floorId)
    {
        E_WorkerTypeId workerTypeId = (E_WorkerTypeId)2;

        CityId = cityId;
        BuildingId = buildingId;
        FloorId = floorId;

        UpgradeInfo();
        base.Show();
    }


    public void UpgradeInfo()
    {
        firstSelectedWorker.Init("Broker", DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 0), 0, false, false, CityId, BuildingId, FloorId, 0);
        secondSelectedWorker.Init("Broker", DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 1), 1, false, false, CityId, BuildingId, FloorId, 1);
        thirdSelectedWorker.Init("Broker", DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 2), 2, false, false, CityId, BuildingId, FloorId, 2);

        for (int i = 0; i < 4; i++)
        {

            if (i == DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 0))
                firstAllWorkers[i].Init("Broker", i, 0, false, false, CityId, BuildingId, FloorId, 0);
            else
                firstAllWorkers[i].Init("Broker", i, 0, true, false, CityId, BuildingId, FloorId, 0);

            if (i == DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 1))
                secondAllWorkers[i].Init("Broker", i, 1, false, false, CityId, BuildingId, FloorId, 1);
            else
                secondAllWorkers[i].Init("Broker", i, 1, true, false, CityId, BuildingId, FloorId, 1);

            if (i == DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 2))
                thirdAllWorkers[i].Init("Broker", i, 2, false, false, CityId, BuildingId, FloorId, 2);
            else
                thirdAllWorkers[i].Init("Broker", i, 2, true, false, CityId, BuildingId, FloorId, 2);

        }
        firstLevelText.text = DataCenter.GetBrokersLevel(CityId, BuildingId, FloorId) + " level";
        secondLevelText.text = DataCenter.GetBrokersLevel(CityId, BuildingId, FloorId) + " level";
        thirdLevelText.text = DataCenter.GetBrokersLevel(CityId, BuildingId, FloorId) + " level";

        firstBonusText.text = "+" + Config.WORKER_BONUS((E_WorkerTypeId)DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 0)) * firstSelectedWorker.StarsCount + "%";
        secondBonusText.text = "+" + Config.WORKER_BONUS((E_WorkerTypeId)DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 1)) * secondSelectedWorker.StarsCount + "%";
        thirdBonusText.text = "+" + Config.WORKER_BONUS((E_WorkerTypeId)DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 2)) * thirdSelectedWorker.StarsCount + "%";

        if (DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 0) == 0)
            firstWorkerNameText.text = Config.JUNIOR_BROKER_NAME(0);

        else if (DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 0) == 1)
            firstWorkerNameText.text = Config.EXPERIENCED_BROKER_NAME(0);

        else if (DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 0) == 2)
            firstWorkerNameText.text = Config.MASTER_BROKER_NAME(0);

        else if (DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 0) == 3)
            firstWorkerNameText.text = Config.LEGENDARY_BROKER_NAME(0);


        if (DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 1) == 0)
            secondWorkerNameText.text = Config.JUNIOR_BROKER_NAME(1);

        else if (DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 1) == 1)
            secondWorkerNameText.text = Config.EXPERIENCED_BROKER_NAME(1);

        else if (DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 1) == 2)
            secondWorkerNameText.text = Config.MASTER_BROKER_NAME(1);

        else if (DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 1) == 3)
            secondWorkerNameText.text = Config.LEGENDARY_BROKER_NAME(1);


        if (DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 2) == 0)
            thirdWorkerNameText.text = Config.JUNIOR_BROKER_NAME(2);

        else if (DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 2) == 1)
            thirdWorkerNameText.text = Config.EXPERIENCED_BROKER_NAME(2);

        else if (DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 2) == 2)
            thirdWorkerNameText.text = Config.MASTER_BROKER_NAME(2);

        else if (DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorId, 2) == 3)
            thirdWorkerNameText.text = Config.LEGENDARY_BROKER_NAME(2);

    }

    public override void Hide(bool animation = true)
    {
        base.Hide(animation);

        onClickCloseButton = null;
        onClickBuyButton = null;
    }

    //void UpdateContent()
    //{
    //    crystalsText.text = DataCenter.Instance.Crystals.ToDecimalDisplay();
    //    eventPointText.text = DataCenter.Instance.EventPoint.ToDecimalDisplay();
    //    goldText.text = DataCenter.Instance.Gold.ToDecimalDisplay();
    //    potionsText.text = DataCenter.Instance.TimeTravelPotion.ToString();
    //    fragmentsText.text = DataCenter.Instance.Fragments.ToDecimalDisplay();
    //}

    public override void OnClickCloseButton()
    {
        //AudioManager.Instance.PlaySound("Click");

        //if (DataCenter.Instance.IsItemPurchased)
        //    StartCoroutine(SaveProcess());
        if (onClickCloseButton != null)
            onClickCloseButton();
    }

    public void OnClickFirstPageButton()
    {
        firstPage.SetActive(true);
        secondPage.SetActive(false);
        thirdPage.SetActive(false);
    }

    public void OnClickSecondPageButton()
    {
        firstPage.SetActive(false);
        secondPage.SetActive(true);
        thirdPage.SetActive(false);
    }

    public void OnClickThirdPageButton()
    {
        firstPage.SetActive(false);
        secondPage.SetActive(false);
        thirdPage.SetActive(true);
    }

    public void OnClickBuyButton()
    {
        SaveData.Instance.AddBrokersLevel(CityId, BuildingId, FloorId, 1);

        InterfaceController.Instance.ShowCoinParticle(upgradeButton.gameObject.transform.position);
    }
    //IEnumerator SaveProcess()
    //{
    //    LoadingWindow loadingWindow = WindowManager.Instance.GetWindow<LoadingWindow>();
    //    loadingWindow.Show();

    //    yield return GameController.Instance.RequestSaveProgress();

    //    loadingWindow.Hide();

    //    DataCenter.Instance.IsItemPurchased = false;

    //    if (onClickCloseButton != null)
    //        onClickCloseButton();
    //}


    //void OnSkinBought(Skin skin)
    //{
    //    UpdateContent();
    //}
}