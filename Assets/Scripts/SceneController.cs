﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    [SerializeField] Camera preloaderCamera;
    [SerializeField] UiPreloader uiPreloader;

    Coroutine loadSceneAsyncCoroutine = null;

    public string PrevScenePath { get; private set; }
    public string CurrScenePath { get; set; }

    public static SceneController Instance { get; private set; }

    public UiPreloader Preloader
    {
        get { return uiPreloader; }
    }

    public bool IsLoad
    {
        get { return loadSceneAsyncCoroutine != null; }
    }


    void Awake()
    {
        Instance = this;

        SceneManager.sceneLoaded += (Scene scene, LoadSceneMode loadSceneMode) =>
        {
            SceneManager.SetActiveScene(scene);
        };
    }

    public int LoadedSceneCount()
    {
        return SceneManager.sceneCount;
    }

    public string GetScenePath(int index)
    {
        if (index < SceneManager.sceneCount)
            return SceneManager.GetSceneAt(index).path;

        return null;
    }

    public void LoadScene(string scenePath, bool async = true, bool manualOut = false, bool manualIn = false)
    {
        if (loadSceneAsyncCoroutine != null)
            return;

        PrevScenePath = CurrScenePath;
        CurrScenePath = scenePath;

        if (async)
            loadSceneAsyncCoroutine = StartCoroutine(LoadSceneAsync(manualOut, manualIn));
        else
            LoadScene();
    }

    void LoadScene()
    {
        SceneManager.LoadScene(CurrScenePath, LoadSceneMode.Additive);
    }

    IEnumerator LoadSceneAsync(bool manualOut = false, bool manualIn = false)
    {
        AsyncOperation asyncOperation;

        if (!string.IsNullOrEmpty(PrevScenePath))
        {
            if (!manualIn)
            {
                uiPreloader.InAnimation();

                while (uiPreloader.IsAnimation())
                    yield return null;
            }

            // Удаляем текущую сцену
            asyncOperation = SceneManager.UnloadSceneAsync(PrevScenePath);

            while (!asyncOperation.isDone)
                yield return null;

            Resources.UnloadUnusedAssets();
            System.GC.Collect();
        }

        yield return new WaitForSecondsRealtime(0.5f);

        // Загружаем новую сцену
        asyncOperation = SceneManager.LoadSceneAsync(CurrScenePath, LoadSceneMode.Additive);

        while (!asyncOperation.isDone)
            yield return null;

        Resources.UnloadUnusedAssets();
        System.GC.Collect();

        yield return new WaitForSecondsRealtime(7f);

        if (!manualOut)
        {
            uiPreloader.OutAnimation();
        }


        loadSceneAsyncCoroutine = null;

        yield break;
    }
}