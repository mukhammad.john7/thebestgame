﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using CodeStage.AntiCheat.ObscuredTypes;

public partial class NetworkController : SingletonMonoBehaviour<NetworkController>
{
    public static System.Action<bool> onInternetConnection;
    public static event Action<DateTime> onLastRequestDateTimeChanged;

    DateTime lastRequestDateTime;
    long networkCode;

    public bool IsInternetConnection { get; private set; }

    public DateTime LastRequestDateTime
    {
        get
        {
            return lastRequestDateTime;
        }
        set
        {
            lastRequestDateTime = value;

            onLastRequestDateTimeChanged?.Invoke(lastRequestDateTime);
        }
    }

    //string Code
    //{
    //    get
    //    {
    //        networkCode++;

    //        return (networkCode.ToString().HashMD5() + ((string)GameConfig.Instance.networkCode).HashMD5()).HashMD5().Substring(0, 15);
    //    }
    //}

    public void SetNetworkCode(long code)
    {
        networkCode = code;
    }


    protected override void Awake()
    {
        base.Awake();

        DontDestroyOnLoad(gameObject);
    }

    IEnumerator Request(UnityWebRequest request, System.Action callback)
    {
        yield return request.SendWebRequest();

        IsInternetConnection = true;

        // Aborted
        if ((request.isNetworkError || request.isHttpError) && request.responseCode == 0L)
        {
            if (request.error == "Cannot resolve destination host")
                IsInternetConnection = false;
            else if (request.error == "Request aborted")
            {
                yield break;
            }
        }

        Debug.LogFormat("Request: {0}\nCode: {1} {2}\nData: {3}\nNetworkCode: {4}",
            request.url,
            request.responseCode,
            request.error,
            request.downloadHandler.text,
            networkCode
        );

        if (onInternetConnection != null)
            onInternetConnection(false);

        if (request.error == "Request aborted")
        {
            yield break;
        }

        Dictionary<string, string> responseHeaders = request.GetResponseHeaders();

        if (IsInternetConnection)
        {
            if (responseHeaders != null && responseHeaders.ContainsKey("Date"))
                LastRequestDateTime = DateTime.Parse(responseHeaders["Date"]).ToUniversalTime();
        }

        if (callback != null)
            callback();

        yield break;
    }
}