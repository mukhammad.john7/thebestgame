﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SettingsWindow : Window
{
    public System.Action onClickCloseButton;

    //[SerializeField] UiTabs uiTabs;
    [SerializeField] Text eventPointText;
    [SerializeField] Text crystalsText;
    [SerializeField] Text goldText;
    [SerializeField] Text potionsText;
    [SerializeField] Text fragmentsText;


    //void OnEnable()
    //{
    //    GameController.onCrystalsChanged += OnCrystalsChanged;
    //    GameController.onEventPointChanged += OnEventPointChanged;
    //    GameController.onFragmentsChanged += OnFragmentsChanged;
    //    GameController.onGoldChanged += OnGoldChanged;
    //    GameController.onSkinBought += OnSkinBought;
    //}

    //void OnDisable()
    //{
    //    GameController.onCrystalsChanged -= OnCrystalsChanged;
    //    GameController.onEventPointChanged -= OnEventPointChanged;
    //    GameController.onFragmentsChanged -= OnFragmentsChanged;
    //    GameController.onGoldChanged -= OnGoldChanged;

    //    GameController.onSkinBought -= OnSkinBought;
    //}

    //void OnCrystalsChanged(long value)
    //{
    //    UpdateContent();
    //}

    //void OnEventPointChanged(int value)
    //{
    //    UpdateContent();
    //}

    //void OnFragmentsChanged(long value)
    //{
    //    UpdateContent();
    //}

    //void OnGoldChanged(double value)
    //{
    //    UpdateContent();
    //}

    //public override void Show(bool animation = true)
    //{
    //    base.Show(animation);

    //    if (!uiTabs.IsInit)
    //        uiTabs.Init();
    //    else
    //        uiTabs.SetActiveTabIndex(uiTabs.DefaultActiveTabIndex);

    //    DataCenter.Instance.StoreItemManager.IsStoreOpened = true;

    //    UpdateContent();
    //}

    public override void Hide(bool animation = true)
    {
        base.Hide(animation);

        onClickCloseButton = null;
    }

    //void UpdateContent()
    //{
    //    crystalsText.text = DataCenter.Instance.Crystals.ToDecimalDisplay();
    //    eventPointText.text = DataCenter.Instance.EventPoint.ToDecimalDisplay();
    //    goldText.text = DataCenter.Instance.Gold.ToDecimalDisplay();
    //    potionsText.text = DataCenter.Instance.TimeTravelPotion.ToString();
    //    fragmentsText.text = DataCenter.Instance.Fragments.ToDecimalDisplay();
    //}

    public override void OnClickCloseButton()
    {
        //AudioManager.Instance.PlaySound("Click");

        //if (DataCenter.Instance.IsItemPurchased)
        //    StartCoroutine(SaveProcess());
        if (onClickCloseButton != null)
            onClickCloseButton();
    }

    //IEnumerator SaveProcess()
    //{
    //    LoadingWindow loadingWindow = WindowManager.Instance.GetWindow<LoadingWindow>();
    //    loadingWindow.Show();

    //    yield return GameController.Instance.RequestSaveProgress();

    //    loadingWindow.Hide();

    //    DataCenter.Instance.IsItemPurchased = false;

    //    if (onClickCloseButton != null)
    //        onClickCloseButton();
    //}


    //void OnSkinBought(Skin skin)
    //{
    //    UpdateContent();
    //}
}