﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class Window : MonoBehaviour
{
    public System.Action onEndShowAnimation;
    public System.Action onEndHideAnimation;

    [SerializeField] Image bgImage;
    [SerializeField] Animator animator;
    [SerializeField] Transform panelTransform;
    [SerializeField] bool disableRender;

    int showHash = Animator.StringToHash("ShowAnimation");
    int hideHash = Animator.StringToHash("HideAnimation");
    int fullScreenHash = Animator.StringToHash("FullScreenAnimation");

    Vector3 panelPosition = new Vector3(0f, 0f, 0f);

    int cullingMask;
    Camera currentCamera;


    public virtual bool IsShow()
    {
        if (gameObject.activeSelf)
            return true;

        return false;
    }

    public virtual void Show(bool animation = true)
    {
        if (gameObject.activeSelf)
            return;

        WindowManager.Instance.AddWindowToTurn(this);

        gameObject.SetActive(true);

        if (animator != null)
            animator.enabled = false;
        else
            animation = false;

        if (animation)
        {
            if (disableRender)
            {
                animator.enabled = true;
                animator.Play(fullScreenHash, -1, 0.0f);
                animator.Update(0.0f);
            }
            else
            {
                animator.enabled = true;
                animator.Play(showHash, -1, 0.0f);
                animator.Update(0.0f);
            }
        }
        else
        {
            OnEndShowAnimation();
        }
    }

    public virtual void Hide(bool animation = true)
    {
        if (disableRender)
        {
            currentCamera.cullingMask = cullingMask;
        }

        if (animator == null)
            animation = false;

        if (animation)
        {
            animator.enabled = true;
            animator.Play(hideHash, -1, 0.0f);
            animator.Update(0.0f);
        }
        else
        {
            OnEndHideAnimation();
        }

        onEndShowAnimation = null;
    }

    public virtual void OnEndShowAnimation()
    {
        if (bgImage != null)
            bgImage.color = new Color32(0, 0, 0, 200);

        if (panelTransform != null)
            panelTransform.localPosition = panelPosition;

        if (animator != null)
            animator.enabled = false;

        if (disableRender)
        {
            currentCamera = Camera.allCameras[0];
            cullingMask = currentCamera.cullingMask;

            currentCamera.cullingMask = (1 << LayerMask.NameToLayer("UI Windows"));
        }

        if (onEndShowAnimation != null)
            onEndShowAnimation();
    }

    public virtual void OnEndHideAnimation()
    {
        WindowManager.Instance.RemoveWindowOfTurn(this);

        if (animator != null)
            animator.enabled = false;

        gameObject.SetActive(false);

        if (onEndHideAnimation != null)
            onEndHideAnimation();

        onEndHideAnimation = null;
    }

    public virtual void OnClickCloseButton()
    {
    }
}