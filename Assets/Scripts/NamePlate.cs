﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NamePlate : MonoBehaviour
{
    [SerializeField] private Text index_0;
    [SerializeField] private Text index_1;
    [SerializeField] private Text index_2;
    [SerializeField] private Image direction_0;
    [SerializeField] private Image direction_1;
    [SerializeField] private Image direction_2;

    private Timer timer;

    void Start()
    {
        StartDurationTimer(5);
    }

    void StartDurationTimer(float time)
    {

        if (timer != null)
            timer.Stop();

        timer = new Timer(time, OnDurationTimerComplete, OnDurationTimerUpdate);

        float random = Random.value;

        index_0.text = Random.Range(10, 99).ToString();
        if (random > 0.5f)
        {
            direction_0.sprite = GameResources.GetDirectionSprite(0);
            direction_0.color = Color.green;
            index_0.color = Color.green;
        }
        else
        {
            direction_0.color = Color.red;
            direction_0.sprite = GameResources.GetDirectionSprite(1);
            index_0.color = new Color(255f, 0f, 0f, 255f);
        }

        random = Random.value;
        index_1.text = Random.Range(10, 99).ToString();
        if (random > 0.5f)
        {
            direction_1.color = Color.green;
            direction_1.sprite = GameResources.GetDirectionSprite(0);
            index_1.color = Color.green;
        }
        else
        {
            direction_1.color = Color.red;
            direction_1.sprite = GameResources.GetDirectionSprite(1);
            index_1.color = new Color(255f, 0f, 0f, 255f);
        }

        random = Random.value;
        index_2.text = Random.Range(10, 99).ToString();
        if (random > 0.5f)
        {
            direction_2.color = Color.green;
            direction_2.sprite = GameResources.GetDirectionSprite(0);
            index_2.color = Color.green;
        }
        else
        {
            direction_2.color = Color.red;
            direction_2.sprite = GameResources.GetDirectionSprite(1);
            index_2.color = new Color(255f, 0f, 0f, 255f);
        }
    }

    void OnDurationTimerComplete()
    {
        timer.Stop();
        timer = null;
        StartDurationTimer(5);
    }

    void OnDurationTimerUpdate(float remainedTime, float totalTime)
    {
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != "GameScene")
        {
            if (timer != null)
                timer.Stop();
            timer = null;
        }
    }

}
