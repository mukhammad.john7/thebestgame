﻿using System;

[System.Serializable]
public class Progress
{
    // DataCenter

    public double gold;
    public long crystals;
    public long fragments;
    public int timeTravelPotion;
    public int eventPoint;

    public float storeDamageModifier;
    public float storeDamagePermanentModifier;
    public float storeGoldPermanentModifier;
    public int storeEnemyCountModifier;
    public float storeFragmentsPermanentModifier;
    public float robotDamageModifier;
    public int robotRubiesGiven;
    public bool isGuardianShield;
    public int instakillMobCount;

    public bool isShowAds;
    public int currentTutorialQuest;

    public long lastDungeonEnterTime;

    public long lastDailyRewardTime;

    public int collectedDailyRewardCount;

    public int collectActivityBonusCount;

    public long lastActivityBonusTime;

    public float inGameTodayTime;

    public int lastTimeTravelTime;

    public int todayTimeTravelCount;

    public int robotSupUpgradeCount;

    public bool isLeagueButtonShowed;

    public User user;

    public Stage stage;

    public Hero hero;

    public Supporter[] supporters;

    public ActiveSkill[] activeSkills;

    public Artifact[] artifacts;

    public Artifact[] specialArtifacts;

    public Skin[] oldSkins;

    public Letter[] letters;

    public LimitedReward[] limitedRewards;

    public FreeItem freeItem;

    public Statistics statistic;


    public bool CheckProgress()
    {
        return supporters != null && supporters.Length > 0;
    }


    public override string ToString()
    {
        string result = string.Format(
            "[Progress] gold:{0} crystals:{1} fragments:{2} timeTravelPotion:{3} eventPoint:{4} ",
            gold,
            crystals,
            fragments,
            timeTravelPotion,
            eventPoint);

        result += string.Format(
            "[Progress] storeDamageModifier:{0} storeDamagePermanentModifier:{1} storeGoldPermanentModifier:{2} storeEnemyCountModifier:{3} storeFragmentsPermanentModifier:{4} storeFragmentsPermanentModifier:{5} robotDamageModifier:{6} isGuardianShield:{7} isShowAds:{8}",
            storeDamageModifier,
            storeDamagePermanentModifier,
            storeGoldPermanentModifier,
            storeEnemyCountModifier,
            storeFragmentsPermanentModifier,
            storeFragmentsPermanentModifier,
            robotDamageModifier,
            isGuardianShield,
            isShowAds);

        result += string.Format(
            "user:{0} stage:{1} hero:{2} statistic:{3}",
            user,
            stage,
            hero,
            statistic);

        if (supporters != null)
            for (int loop = 0; loop < supporters.Length; loop++)
                result += supporters[loop];

        return result;
    }


    [System.Serializable]
    public class User
    {
        public int rateApp;
        public long nextRateUpTime;
        public bool privacyPolicyAccepted;
        public bool isCollectStarterPack;

        public override string ToString()
        {
            return string.Format(
                "[User] rateApp:{0} isCollectStarterPack:{1}",
                rateApp,
                isCollectStarterPack);
        }

        public int purchasedScrollsCount;
        public bool isPremiumPurchased;
        public bool isClanCreationPurchased;
        public bool isChangeNicknamePurchased;
        public int NicknameChangeCount;
        public bool isMadpoolPurchased;

        public bool isNewbie;
        public float newbieBonus;
        public long newbieStartBonusTime;

        public int tutorialStarterStep;
        public int tutorialQuestStep;
        public bool tutorialTipsQuest;
        public bool tutorialTipsEvent;
        public long tutorialEndTime;
    }

    [System.Serializable]
    public class Stage
    {
        public int currentStage;
        public int currentEnemyNumber;
        public int currentLocationId;

        public override string ToString()
        {
            return string.Format(
                "[Stage] currentStage:{0} currentEnemyNumber:{1} currentLocationId:{2}",
                currentStage,
                currentEnemyNumber,
                currentLocationId);
        }
    }

    [System.Serializable]
    public class Hero
    {
        public int level;
        public int headSkinId;
        public int bodySkinId;
        public int weaponSkinId;
        public int armSkinId;
        public int legSkinId;

        public override string ToString()
        {
            return string.Format(
                "[Hero] level:{0} headSkinId:{1} bodySkinId:{2} weaponSkinId:{3}",
                level,
                headSkinId,
                bodySkinId,
                weaponSkinId);
        }
    }

    [System.Serializable]
    public class Supporter
    {
        public int level;
        public int evolveCount;
        public int gainPoint;
        public long recoverDateTime;
        public bool[] isOpenPassiveSkills;

        public override string ToString()
        {
            return string.Format(
                "[Supporter] level:{0} evolveCount:{1} gainPoint:{2} recoverDateTime:{3} isOpenPassiveSkills:array",
                level,
                evolveCount,
                gainPoint,
                recoverDateTime);
        }
    }

    [System.Serializable]
    public class ActiveSkill
    {
        public int level;
        public long rollbackDateTime;

        public override string ToString()
        {
            return string.Format(
                "[ActiveSkill] level:{0} rollbackDateTime:{1}",
                level,
                rollbackDateTime);
        }
    }

    [System.Serializable]
    public class Artifact
    {
        public int level;

        public override string ToString()
        {
            return string.Format(
                "[Artifact] level:{0}",
                level);
        }
    }

    [System.Serializable]
    public class Skin
    {
        public bool isOpen;

        public override string ToString()
        {
            return string.Format(
                "[Skin] isOpen:{0}",
                isOpen);
        }
    }

    [System.Serializable]
    public class Letter
    {
        public int Count;
    }

    [System.Serializable]
    public class LimitedReward
    {
        public bool isCollected;

        public override string ToString()
        {
            return string.Format(
                "[LimitedReward] isCollected:{0}",
                isCollected);
        }
    }

    [System.Serializable]
    public class FreeItem
    {
        public bool changeNickname;
    }

    [System.Serializable]
    public class Statistics
    {
        public double collectedGold;
        public long collectedCrystals;
        public long collectedFragments;
        public long collectedTimeTravelPotions;
        public long usedTimeTravelPotions;
        public long timeTravelFromGifts;
        public long timeTravelFromReward;
        public long timeTravelFromDrop;
        public float maxDps;
        public int highestStage;
        public int highest_Stage;
        public double highestSupportersDamage;
        public int timeTravelCount;
        public long tapAllTimes;
        public int killedEnemy;
        public int killedBoss;
        public int killedChest;
        public int levelUpSupporters;
        public int criticalHit;
        public int weakPointHit;
        public int usedSkills;
        public int recoveredSupporters;
        public int ownedArtifacts;
        public int selledArtifacts;
        public int collectedRobotBonus;
        public int dungeonCount;
        public int highestSupportersCount;

        public override string ToString()
        {
            return string.Format(
                "[Statistics] collectedGold:{0} collectedCrystals:{1} collectedFragments:{2} highestStage:{3} highestSupportersDamage:{4} timeTravelCount:{5} tapAllTimes:{6} killedEnemy:{7} killedBoss:{8} killedChest:{9} levelUpSupporters:{10} ..",
                collectedGold,
                collectedCrystals,
                collectedFragments,
                highest_Stage,
                highestSupportersDamage,
                timeTravelCount,
                tapAllTimes,
                killedEnemy,
                killedBoss,
                killedChest,
                levelUpSupporters);
        }
    }
}