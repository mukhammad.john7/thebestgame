﻿using System;
using UnityEngine;


public class Page : MonoBehaviour
{
    public static event Action<Page> onPageShowed;
    public static event Action<Page> onPageFocused;

    public bool IsInit { get; private set; }

    public bool IsShow => gameObject.activeSelf;


    public virtual void Init()
    {
        //Debug.LogWarning(this + " Init");

        IsInit = true;
    }

    public virtual void Show()
    {
        gameObject.SetActive(true);

        if (!IsInit)
            Init();

        //Debug.LogWarning(this + " Show");

        onPageShowed?.Invoke(this);
    }

    public virtual void Hide()
    {
        //Debug.LogWarning(this + " Hide");

        gameObject.SetActive(false);
    }

    public virtual void UpdateContent()
    {
        //Debug.LogWarning(this + " UpdateContent");
    }

    public virtual void OnFocus()
    {
        //Debug.LogWarning(this + " OnFocus");

        onPageFocused?.Invoke(this);
    }

    public virtual void OnUnFocus()
    {
        //Debug.LogWarning(this + " OnUnFocus");
    }
}
