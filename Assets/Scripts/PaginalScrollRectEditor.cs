﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.UI;


[CustomEditor(typeof(PaginalScrollRect), true)]
[CanEditMultipleObjects]
public class PaginalScrollRectEditor : ScrollRectEditor
{
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(serializedObject.FindProperty("isAutoInit"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("pages"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("homePageIndex"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("lerpSpeed"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("swipeEnabled"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("fastSwipeEnabled"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("fastSwipeFactorLimit"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("fastSwipeDistanceLimit"));

        serializedObject.ApplyModifiedProperties();

        EditorGUILayout.Space();

        base.OnInspectorGUI();
    }
}


#endif