﻿using System;
using UnityEngine;

public class BottomMenu : MonoBehaviour
{
    public event Action<int> onTabButtonClicked;


    [SerializeField] private TabButton[] tabButtons;
    [SerializeField] private PaginalScrollRect paginalScrollRect;


    private Vector3 selectBgPosition;


    private void Start()
    {
        for (int loop = 0; loop < tabButtons.Length; loop++)
        {
            tabButtons[loop].Index = loop;
            tabButtons[loop].onClick += TabButton_OnClick;
        }

        paginalScrollRect.onPageChanged += PaginalScrollRect_OnPageChanged;
        paginalScrollRect.onValueChanged.AddListener(PaginalScrollRect_OnValueChanged);


        PaginalScrollRect_OnValueChanged(paginalScrollRect.normalizedPosition);
    }

    private void SelectButton(int index)
    {
        for (int loop = 0; loop < tabButtons.Length; loop++)
            if (loop != index)
                tabButtons[loop].IsOn = false;

        tabButtons[index].IsOn = true;
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    // 

    private void TabButton_OnClick(int index)
    {
        //AudioManager.Instance.PlaySound("Sounds/Click2");

        onTabButtonClicked?.Invoke(index);

        paginalScrollRect.ShowPage(index);
    }

    private void PaginalScrollRect_OnPageChanged(int pageIndex)
    {
        SelectButton(pageIndex);
    }

    private void PaginalScrollRect_OnValueChanged(Vector2 value)
    {
        selectBgPosition.x = Mathf.LerpUnclamped(-400f, 400f, value.x);
    }
}
