﻿using System;
using UnityEngine;
using UnityEngine.UI;


public class TabButton : MonoBehaviour
{
    public event Action<int> onClick;


    [SerializeField] protected Button button;
    [SerializeField] protected bool isOn;
    [SerializeField] protected bool isAvailable = true;


    public int Index { get; set; }

    public bool IsOn
    {
        get { return isOn; }
        set
        {
            if (isOn == value)
                return;

            isOn = value;

            ApplyState();
        }
    }

    public bool IsAvailable
    {
        get { return isAvailable; }
        set
        {
            if (isAvailable == value)
                return;

            isAvailable = value;

            ApplyState();
        }
    }

    protected virtual void Start()
    {
        button.onClick.AddListener(Button_OnClick);

        ApplyState();
    }

    protected virtual void ApplyState()
    {
    }

    public virtual void UpdateContent()
    {
        ApplyState();
    }

    //

    protected virtual void Button_OnClick()
    {
        onClick?.Invoke(Index);
    }
}
