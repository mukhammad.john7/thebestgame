﻿using UnityEngine.UI;

public class CustomCanvasScaller : CanvasScaler
{
    protected override void Awake()
    {
#if UNITY_EDITOR
        if (UnityEditor.EditorApplication.isPlaying)
#endif
            GetComponent<CanvasScaler>().matchWidthOrHeight = ScreenInfo.Instance.IsSuperWide ? 0f : 1f;

        base.Awake();
    }
}
