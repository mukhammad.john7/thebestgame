﻿using UnityEngine;

/* Defines
 * DISABLED_LOG - отключает логирование Debug.Log()
 * IS_DEBUG - для тестирования
 * IS_BIRTHDAY_EVENT - с некоторым шансем с мобов выпадение плюшек
 * IS_WINTER_EVENT - с некоторым шансем появление собого босса
 * IS_WINTER_BOSS - убийство пуджа по точкам
 * IS_VALENTINE - выпадение букв с боссов (нужна иконка и бандлы)
 * IOS_PREMIUM_REMOVE - убрать прем с аойса
 */

public class Config
{
    public const string PRIVACY_POLICY_URL = "https://privacy.azurgames.com";
    public const string TERMS_OF_USE_URL = "https://privacy.azurgames.com";

#if UNITY_ANDROID
    public const string UNITY_ADS_GAME_ID = "3310749";
    public const string STORE_APP_URL = "market://details?id=tap.lords.royale#details-reviews";
    public const string APPLOVIN_REWARD_ADS_KEY = "18c347b54ce930ef";
    public const string APPLOVIN_INTERTITIAL_KEY = "966ac2249f69d394";
    public const string APP_BUNDLE_ID = "tap.lords.royale";
    //public static readonly E_StoreType STORE_TYPE = E_StoreType.GooglePlay;
#else
    public const string STORE_APP_URL = "http://itunes.apple.com/app/id1483035336";
    public const string APPLOVIN_REWARD_ADS_KEY = "b3836549640d5671";
    public const string APPLOVIN_INTERTITIAL_KEY = "c8722b445833e086";
    public const string APP_BUNDLE_ID = "1483035336";
	//public static readonly E_StoreType STORE_TYPE = E_StoreType.AppStore;
#endif

    public const string APPLOVIN_SDK_KEY = "6AQkyPv9b4u7yTtMH9PT40gXg00uJOTsmBOf7hDxa_-FnNZvt_qTLnJAiKeb5-2_T8GsI_dGQKKKrtwZTlCzAR";
    public const string APPLOVIN_REPORT_KEY = "n8fL7GkI44b0rHORunfWQfRGReKg--Ca9G2Ca6WUOTZQ8FnciF3b-KvHA2TN4lzgBaWry8V6spv48tiD-NSE_C";

    public const string APPSFLYER_DEV_KEY = "r9vNC83N8nYpCzYGigyjUh";
    public const string APPMETRICA_KEY = "ee7e597e-17e6-420f-bb5d-b2b77148de62";

    public const string DEVELOPER_SITE_URL = "http://taptics.pro";
    public const string AZUR_SITE_URL = "https://www.instagram.com/azurgameslive/";
    public const string FACEBOOK_GROUP_URL = "https://www.facebook.com/lordsroyalegame";

    public const string SCENE_MAIN_PATH = "Scenes/MainScene";
    public const string SCENE_COMICS_PATH = "Scenes/ComicsScene";
    public const string SCENE_LOADING_PATH = "Scenes/LoadingScene";
    public const string SCENE_GAME_PATH = "Scenes/GameScene";
    public const string SCENE_MAP_PATH = "Scenes/MapScene";

    public const int ADS_LIMIT_MAX = 30;
    public const int ADS_LIMIT_INTERVAL = 1 * 60 * 60; // 1 часа

    public const int FLOOR_BUYED_STATE = 0;
    public const int FLOOR_SALE_STATE = 1;
    public const int FLOOR_BROKEN_STATE = 2;
    public const int FLOOR_SALE_BROKEN_STATE = 3;

    public const int MANAGER_NOT_PURCHASED_STATE = 0;
    public const int MANAGER_PURCHASED_STATE = 1;

    public const int FLOOR_NUMBER = 12;

    public const int FIRST_STAR_COST = 12;
    public const int SECOND_STAR_COST = 12;
    public const int THIRD_STAR_COST = 12;
    public const int FOURTH_STAR_COST = 12;
    public const int FIFTH_STAR_COST = 12;

    public static double STAR_COST(int starNum)
    {
        double starsCost = 0;

        switch (starNum)
        {
            case 0:
                starsCost = 5;
                break;

            case 1:
                starsCost = 15;
                break;
            case 2:
                starsCost = 30;
                break;
            case 3:
                starsCost = 55;
                break;
            case 4:
                starsCost = 90;
                break;
            case 5:
                starsCost = 100;
                break;
        }

        return starsCost;
    }


    public static double BOOST_NUM(int boostId)
    {
        double boostNum = 0;

        switch (boostId)
        {
            case -1:
                boostNum = 1;
                break;

            case 0:
                boostNum = 5;
                break;

            case 1:
                boostNum = 10;
                break;
            case 2:
                boostNum = 20;
                break;
            case 3:
                boostNum = 40;
                break;
            case 4:
                boostNum = 80;
                break;
            case 5:
                boostNum = 160;
                break;
        }

        return boostNum;
    }

    public static double WORKER_BONUS(E_WorkerTypeId workerType)
    {
        double bonusNum = 0;

        switch (workerType)
        {
            case E_WorkerTypeId.Junior:
                bonusNum = 5;
                break;
            case E_WorkerTypeId.Experienced:
                bonusNum = 10;
                break;
            case E_WorkerTypeId.Master:
                bonusNum = 15;
                break;
            case E_WorkerTypeId.Legendary:
                bonusNum = 20;
                break;
        }

        return bonusNum;
    }

    public static string JUNIOR_MANAGER_NAME(int id)
    {
        string workerName = "ERROR 404";

        switch (id)
        {
            case 0:
                workerName = "TONEY";
                break;
            case 1:
                workerName = "AMANDA";
                break;
            case 2:
                workerName = "FRANK";
                break;
            case 3:
                workerName = "JESSIE";
                break;
            case 4:
                workerName = "CLARK";
                break;
            case 5:
                workerName = "BRITNEY";
                break;
            case 6:
                workerName = "CAROL";
                break;
            case 7:
                workerName = "JAKE";
                break;
            case 8:
                workerName = "TOM";
                break;
            case 9:
                workerName = "COURTENEY";
                break;
            case 10:
                workerName = "ROSE";
                break;
            case 11:
                workerName = "GREG";
                break;
        }

        return workerName;
    }

    public static string EXPERIENCED_MANAGER_NAME(int id)
    {
        string workerName = "ERROR 404";

        switch (id)
        {
            case 0:
                workerName = "ANTONIO";
                break;
            case 1:
                workerName = "BRIDGET";
                break;
        }

        return workerName;
    }

    public static string MASTER_MANAGER_NAME(int id)
    {
        string workerName = "ERROR 404";

        switch (id)
        {
            case 0:
                workerName = "CHLOE";
                break;
            case 1:
                workerName = "WENDY";
                break;
        }

        return workerName;
    }

    public static string LEGENDARY_MANAGER_NAME(int id)
    {
        string workerName = "ERROR 404";

        switch (id)
        {
            case 0:
                workerName = "MICHELLE";
                break;
            case 1:
                workerName = "SEBASTIAN";
                break;
        }

        return workerName;
    }

    public static string JUNIOR_BROKER_NAME(int id)
    {
        string workerName = "ERROR 404";

        switch (id)
        {
            case 0:
                workerName = "ALEX";
                break;
            case 1:
                workerName = "FREDDIE";
                break;
            case 2:
                workerName = "GEORGE";
                break;
        }

        return workerName;
    }

    public static string EXPERIENCED_BROKER_NAME(int id)
    {
        string workerName = "ERROR 404";

        switch (id)
        {
            case 0:
                workerName = "ANGEL";
                break;
            case 1:
                workerName = "SAM";
                break;
            case 2:
                workerName = "ANDRES";
                break;
        }

        return workerName;
    }

    public static string MASTER_BROKER_NAME(int id)
    {
        string workerName = "ERROR 404";

        switch (id)
        {
            case 0:
                workerName = "ZOEY";
                break;
            case 1:
                workerName = "MICHAEL";
                break;
            case 2:
                workerName = "LARI";
                break;
        }

        return workerName;
    }

    public static string LEGENDARY_BROKER_NAME(int id)
    {
        string workerName = "ERROR 404";

        switch (id)
        {
            case 0:
                workerName = "DIANA";
                break;
            case 1:
                workerName = "CHAZ";
                break;
            case 2:
                workerName = "ANDREW";
                break;
        }

        return workerName;
    }
}