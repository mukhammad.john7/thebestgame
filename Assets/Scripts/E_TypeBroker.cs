﻿public enum E_TypeBroker
{
    None = 0,
    Junior = 1,
    Middle = 2,
    Master = 3,
    Legendary = 4
}
