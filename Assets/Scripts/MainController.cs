﻿//using CodeStage.AntiCheat.ObscuredTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MainController : MonoBehaviour
{
    void Awake()
    {
        GameSettings.AppStartCount++;

        if (GameSettings.UnlockFrameRate)
            Application.targetFrameRate = 60;
        else
            Application.targetFrameRate = 30;

        Input.multiTouchEnabled = false;
        QualitySettings.vSyncCount = 0;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

#if DEVELOPMENT_BUILD
        Debug.unityLogger.logEnabled = true;
#elif UNITY_EDITOR
        Debug.unityLogger.logEnabled = true;
#else
        Debug.unityLogger.logEnabled = false;
#endif
    }

    //private void OnEnable()
    //{
    //    NetworkController.onLastRequestDateTimeChanged += ReceivingNetworkDateTime;
    //}

    //private void OnDisable()
    //{
    //    NetworkController.onLastRequestDateTimeChanged -= ReceivingNetworkDateTime;
    //}

    void Start()
    {
        // Load start scene
        if (SceneController.Instance.LoadedSceneCount() > 1)
            SceneController.Instance.CurrScenePath = SceneController.Instance.GetScenePath(0);
        else
            SceneController.Instance.LoadScene(Config.SCENE_LOADING_PATH, false);
    }

    //    void ReceivingNetworkDateTime(DateTime dateTime)
    //    {
    //        TimeManager.Instance.NetworkDateTime = dateTime;
    //    }

    //    // Helpers
    //#if UNITY_EDITOR
    //    [ContextMenu("SetTest_Nikita")]
    //    void SetTestMe()
    //    {
    //        User.SocialType = E_SocialType.vk.ToString();
    //        User.Id = 1810450;
    //        User.SessionKey = "BX234IQVMdOAytyW5ejO5fuJ50eHyP6m1556006128";
    //        User.SocialToken = "91f806728b885becdd82f44e40494f4e355753c17914052949cc0ac1b1ddf19a6aa02a5148602a396a1c6";
    //    }

    //    [ContextMenu("SetTestUser_Mihail")]
    //    void SetTestUser1()
    //    {
    //        User.SocialType = E_SocialType.vk.ToString();
    //        User.Id = 21;
    //        User.SessionKey = "9PQkg6JRRlMBY1OkiZjS1yI_9FGs0Woj1553089881";
    //        User.SocialToken = "bd4d68dda5a54b749c11d0e390bf7e7b812438794e978564936a5b6274d3ddc9224167fa369358eaf8583";
    //    }

    //    [ContextMenu("SetTestUser_Taptics")]
    //    void SetTestUser2()
    //    {
    //        User.SocialType = E_SocialType.vk.ToString();
    //        User.Id = 2218190;
    //        User.SessionKey = "fNYgQQe0g8oI4M8gRa3MMLmQoUpUi3zR1556006488";
    //        User.SocialToken = "aa6559761e72787be497ccbc2fd449e59bb45a9245b0945dd4fd11ea8083efe7e6e5624ffc82a4620ca1c";
    //    }

    //    [ContextMenu("SetTestUser_Damir")]
    //    void SetTestUser3()
    //    {
    //        User.SocialType = E_SocialType.vk.ToString();
    //        User.Id = 2218440;
    //        User.SessionKey = "vTG0C9URgCy3DJyXlfMQbAk6H-9hnHWq1553607095";
    //        User.SocialToken = "61bda8a19c417852c96746d20f7f01bcd0534b3628e91f6738a40b174e550a619c9a71bda425f18cc11b9";
    //    }

    //    [ContextMenu("SetTestUser_NicolaiSharhov")]
    //    void SetTestUser4()
    //    {
    //        User.SocialType = E_SocialType.vk.ToString();
    //        User.Id = 1441759;
    //        User.SessionKey = "_V-q2PrVSuxHpzZ_5LWrBvNoIpUARgdt1547204972";
    //        User.SocialToken = "7aa240008e889904552c2dae79b783cd81501872d56f315d66cebd0fa4615d85a328d668e0525adbafaa1";
    //    }

    //    [ContextMenu("SetTestUser_Kasugin")]
    //    void SetTestUser5()
    //    {
    //        User.SocialType = E_SocialType.vk.ToString();
    //        User.Id = 807722;
    //        User.SessionKey = "v6QY9RXKM8bTyx4jda34_Rz4W0-95CfM1558009605";
    //        User.SocialToken = "8a910707a3834c18356f7e6a5b7a67a1377a2d5d648f3033c9badd3e8e17f953b5342c9c37a180b978aa0";
    //    }

    [ContextMenu("PlayerPrefs.DeleteAll")]
    void WipeAll()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
    }

    //    [ContextMenu("Clear Cache")]
    //    void ClearCache()
    //    {
    //        Caching.ClearCache();
    //    }
    //#endif
}
