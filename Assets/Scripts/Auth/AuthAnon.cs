﻿using Firebase;
using Firebase.Auth;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

public class AuthAnon : MonoBehaviour
{
    // Start is called before the first frame update
    public bool Starting = false;
    public void GuestLogin()
    {
        Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        auth.SignInAnonymouslyAsync().ContinueWith(task => {
            if (task.IsCanceled) {
                Debug.LogError("SignInAnonymouslyAsync was canceled.");
                return;
            }
            if (task.IsFaulted) {
                Debug.LogError("SignInAnonymouslyAsync encountered an error: " + task.Exception);
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            Starting = true;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
            
        });
        StartCoroutine(WaitingStartingCoroutine());
        /*Debug.Log("Checking Dependencies");
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(fixTask =>
        {
            Assert.IsNull(fixTask.Exception);
            Debug.Log("Authenticating");
            var auth = FirebaseAuth.DefaultInstance;
            auth.SignInAnonymouslyAsync().ContinueWith(authTask =>
            {
                Assert.IsNull(authTask.Exception);
                Debug.Log("Signed in!");
                var successes = PlayerPrefs.GetInt("Successes", 0);
                PlayerPrefs.SetInt("Successes", ++successes);
                Debug.Log($"Successes: {successes}");
                auth.SignOut();
                Debug.Log("Signed Out");
            });
        });*/
    }

    IEnumerator WaitingStartingCoroutine()
    {
        yield return new WaitUntil(() => Starting);
        Starting = false;
        LoadingControlller.Instance.StartSaveData();
    }
}