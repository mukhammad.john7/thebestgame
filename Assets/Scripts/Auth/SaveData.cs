﻿using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System;
using UnityEngine.UI;
using System.Collections;
using CodeStage.AntiCheat.ObscuredTypes;
using UnityEngine.SceneManagement;

public class SaveData : SingletonResources<SaveData>
{
    private Timer timer;

    private bool IsAllChecked = false;

    public static string Id;

    public int BrokerStars;
    public double BrokersLevel;
    public double BrokerCards;

    public int ManagerStars;
    public double ManagerLevel;
    public double ManagerCards;

    public bool getBrokerStarsReady = false;
    public bool addBrokerStarsReady = false;
    public int getBrokerStarsComplete = -1;
    public int addBrokerStarsComplete = -1;

    public bool getBrokerCardsReady = false;
    public bool addBrokerCardsReady = false;
    public int getBrokerCardsComplete = -1;
    public int addBrokerCardsComplete = -1;

    public bool getBrokerLevelReady = false;
    public bool addBrokerLevelReady = false;
    public int getBrokerLevelComplete = -1;
    public int addBrokerLevelComplete = -1;

    public bool getManagerLevelReady = false;
    public bool addManagerLevelReady = false;
    public int getManagerLevelComplete = -1;
    public int addManagerLevelComplete = -1;

    public bool getManagerCardsReady = false;
    public bool addManagerCardsReady = false;
    public int getManagerCardsComplete = -1;
    public int addManagerCardsComplete = -1;

    public bool getManagerStarsReady = false;
    public bool addManagerStarsReady = false;
    public int getManagerStarsComplete = -1;
    public int addManagerStarsComplete = -1;

    public bool getCoinsReady = false;
    public bool addCoinsReady = false;
    public int getCoinsComplete = -1;
    public int addCoinsComplete = -1;

    public double Coins;

    public bool getDiamondsReady = false;
    public bool addDiamondsReady = false;
    public int getDiamondsComplete = -1;
    public int addDiamondsComplete = -1;

    public double Diamonds;

    public bool getBoostIdReady = false;
    public bool setBoostIdReady = false;
    public int getBoostIdComplete = -1;
    public int setBoostIdComplete = -1;

    public int BoostId;

    public bool getBoostTimeReady = false;
    public bool setBoostTimeReady = false;
    public int getBoostTimeComplete = -1;
    public int setBoostTimeComplete = -1;

    public long BoostTime;

    public bool getBoostReady = false;
    public bool addBoostReady = false;
    public int getBoostComplete = -1;
    public int addBoostComplete = -1;

    public double Boost;

    public static event Action onFloorState;

    public bool go = false;
    DatabaseReference mDatabaseRef;

    protected override void Awake()
    {
        base.Awake();

        DontDestroyOnLoad(gameObject);
    }
    void Start() {

        Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
         Firebase.Auth.FirebaseUser user = auth.CurrentUser;
        Id = user.UserId;
         if (user != null) {
             string playerName = user.DisplayName;
             // The user's Id, unique to the Firebase project.
             // Do NOT use this value to authenticate with your backend server, if you
             // have one; use User.TokenAsync() instead.
             string uid = user.UserId;
         }
        // Set up the Editor before calling into the realtime database.
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://wall-street-idle-66093686.firebaseio.com/");

        // Get the root reference location of the database.
        mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
        List<double> Coins = new List<double>(){5,5,5,5,5};

        //writeUserData(Id, Coins, 8, 5);


        SetAllThings();

    }


    public void SetAllThings()
    {
        SetCurrentBoostId(-1);
        GetCoins(DataCenter.CurrentCity, DataCenter.CurrentBuilding);
        GetBrokersLevel(DataCenter.CurrentCity, DataCenter.CurrentBuilding, 0);
        GetBrokerStars(0, 0);
        GetManagerStars(0, 0);
        GetBrokerCards(0, 0);
        GetManagerCards(0, 0);
        GetManagerLevel(DataCenter.CurrentCity, DataCenter.CurrentBuilding, 0, 0);
        GetDiamonds();
        //GetCurrentBoostId();
        GetBoostTime(0);
        GetBoost(0);
        StartDurationTimer(3f);
    }


    void SetSome()
    {
        mDatabaseRef.Child("users").Child(Id).Child("brokerStars").Child("brokerType_" + 2 + "_brokerId_" + 2).SetValueAsync(2 + 1);
    }

    private void writeUserData(string userId, List<double> coins, double diamonds, int age) {
        //GetComponent<DataCenter>().GetMoneyNum();
        Uxser user = new Uxser(coins, diamonds);
        string json = JsonUtility.ToJson(user);
        mDatabaseRef.Child("users").Child(userId).SetRawJsonValueAsync(json);
        SetSome();
    }

    public void AddBrokerStarsY()
    {
        AddBrokerStars(1, 1);
        GetBrokerStars(1, 1);
    }

        // Update coins


        void StartDurationTimer(float time)
    {

        if (timer != null)
            timer.Stop();

        timer = new Timer(time, OnDurationTimerComplete);
    }

    void OnDurationTimerComplete()
    {
        GetCoins(DataCenter.CurrentCity, DataCenter.CurrentBuilding);
        timer.Stop();
        timer = null;
        StartDurationTimer(3f);
    }








    // BOOST NUM


    public void AddBoost(int boostId, double boostNum)
    {
        double boost = 0;

        mDatabaseRef.Child("users").Child(Id).Child("stuff").Child("boost").Child("boostId_" + boostId).GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                addBoostComplete = 0;
                addBoostReady = true;
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                boost = Convert.ToDouble(snapshot.Value);
                mDatabaseRef.Child("users").Child(Id).Child("stuff").Child("boost").Child("boostId_" + boostId).SetValueAsync(boost + boostNum);
                addBoostComplete = 1;
                addBoostReady = true;
            }
        });

        StartCoroutine(WaitingAddingBoostCoroutine(boostId, boostNum));
    }

    IEnumerator WaitingAddingBoostCoroutine(int boostId, double boostNum)
    {
        yield return new WaitUntil(() => addBoostReady);

        if (addBoostComplete == 1)
        {
            addBoostReady = false;
            addBoostComplete = -1;
            DataCenter.AddBoost(boostId, boostNum);
        }

        else if (addBoostComplete == 0)
        {
            addBoostReady = false;
            addBoostComplete = -1;
            Debug.Log("FAILLL");
        }
    }

    public void GetBoost(int boostId)
    {
        double boost = -1;
        mDatabaseRef.Child("users").Child(Id).Child("stuff").Child("boost").Child("boostId_" + boostId).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                getBoostComplete = 0;
                getBoostReady = true;
            }

            else if (task.IsCompleted)
            {
                DataSnapshot snapshot1 = task.Result;
                boost = Convert.ToDouble(snapshot1.Value);
                Boost = boost;
                getBoostComplete = 1;
                getBoostReady = true;
            }
        });
        StartCoroutine(WaitingGettingBoostCoroutine(boostId));
    }

    IEnumerator WaitingGettingBoostCoroutine(int boostId)
    {

        yield return new WaitUntil(() => getBoostReady);

        if (getBoostComplete == 1)
        {
            getBoostReady = false;
            getBoostComplete = -1;
            SetLocalBoost(boostId);
        }

        else if (getBoostComplete == 0)
        {
            getBoostReady = false;
            getBoostComplete = -1;
            Debug.Log("FAILLL");
        }
    }

    private void SetLocalBoost(int boostId)
    {
        DataCenter.SetLocalBoost(boostId, Boost);
        boostId++;

        if (boostId < 6)
        {
            GetBoost(boostId);
        }
    }


    // BOOST TIME



    public void SetBoostTime(int boostId, long time)
    {
        long boostTime = 0;

        mDatabaseRef.Child("users").Child(Id).Child("stuff").Child("boostTime").Child("boostId_" + boostId).GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                setBoostTimeComplete = 0;
                setBoostTimeReady = true;
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                boostTime = Convert.ToInt64(snapshot.Value);
                mDatabaseRef.Child("users").Child(Id).Child("stuff").Child("boostTime").Child("boostId_" + boostId).SetValueAsync(time);
                setBoostTimeComplete = 1;
                setBoostTimeReady = true;
            }
        });

        StartCoroutine(WaitingSetingBoostTimeCoroutine(boostId, time));
    }

    IEnumerator WaitingSetingBoostTimeCoroutine(int boostId, long time)
    {
        yield return new WaitUntil(() => setBoostTimeReady);

        if (setBoostTimeComplete == 1)
        {
            setBoostTimeReady = false;
            setBoostTimeComplete = -1;
            DataCenter.SetBoostTime(boostId, time);
        }

        else if (setBoostTimeComplete == 0)
        {
            setBoostTimeReady = false;
            setBoostTimeComplete = -1;
            Debug.Log("FAILLL");
        }
    }

    public void GetBoostTime(int boostId)
    {
        long boostTime = -1;
        mDatabaseRef.Child("users").Child(Id).Child("stuff").Child("boostTime").Child("boostId_" + boostId).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                getBoostTimeComplete = 0;
                getBoostTimeReady = true;
            }

            else if (task.IsCompleted)
            {
                DataSnapshot snapshot1 = task.Result;
                boostTime = Convert.ToInt64(snapshot1.Value);
                BoostTime = boostTime;
                getBoostTimeComplete = 1;
                getBoostTimeReady = true;
            }
        });
        StartCoroutine(WaitingGettingBoostTimeCoroutine(boostId));
    }

    IEnumerator WaitingGettingBoostTimeCoroutine(int boostId)
    {

        yield return new WaitUntil(() => getBoostTimeReady);

        if (getBoostTimeComplete == 1)
        {
            getBoostTimeReady = false;
            getBoostTimeComplete = -1;
            SetLocalBoostTime(boostId);
        }

        else if (getBoostTimeComplete == 0)
        {
            getBoostTimeReady = false;
            getBoostTimeComplete = -1;
            Debug.Log("FAILLL");
        }
    }

    private void SetLocalBoostTime(int boostId)
    {
        if (DataCenter.Instance.CheckIsBoostActive(boostId))
        {
            DataCenter.SetBoostTime(boostId, BoostTime);
            SetCurrentBoostId(boostId);
        }

        else if (!DataCenter.Instance.CheckIsBoostActive(boostId))
        {
            SetBoostTime(boostId, 0);
        }

        boostId++;

        if (boostId < 6)
        {
            GetBoostTime(boostId);
        }
    }

    // BOOST ID

    public void SetCurrentBoostId(int newBoostId)
    {

        int boostId = 0;

        mDatabaseRef.Child("users").Child(Id).Child("stuff").Child("boostId").GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                setBoostIdComplete = 0;
                setBoostIdReady = true;
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                boostId = Convert.ToInt32(snapshot.Value);
                mDatabaseRef.Child("users").Child(Id).Child("stuff").Child("boostId").SetValueAsync(newBoostId);
                setBoostIdComplete = 1;
                setBoostIdReady = true;
            }
        });

        StartCoroutine(WaitingSettingBoostIdCoroutine(newBoostId));
    }


    IEnumerator WaitingSettingBoostIdCoroutine(int BoostId)
    {
        yield return new WaitUntil(() => setBoostIdReady);

        if (setBoostIdComplete == 1)
        {
            setBoostIdReady = false;
            setBoostIdComplete = -1;
            DataCenter.SetCurrentBoostId(BoostId);
        }

        else if (setBoostIdComplete == 0)
        {
            setBoostIdReady = false;
            setBoostIdComplete = -1;
            Debug.Log("FAILLL");
        }
    }

    public void GetCurrentBoostId()
    {
        int boostId = -1;
        mDatabaseRef.Child("users").Child(Id).Child("stuff").Child("boostId").GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                getBoostIdComplete = 0;
                getBoostIdReady = true;
            }

            else if (task.IsCompleted)
            {
                DataSnapshot snapshot1 = task.Result;
                boostId = Convert.ToInt32(snapshot1.Value);
                BoostId = boostId;

                if (boostId == 0)
                {
                    mDatabaseRef.Child("users").Child(Id).Child("stuff").Child("boostId").SetValueAsync(1);
                    BoostId = 1;
                }

                getBoostIdComplete = 1;
                getBoostIdReady = true;
            }
        });
        StartCoroutine(WaitingGettingBoostIdCoroutine());
    }

    IEnumerator WaitingGettingBoostIdCoroutine()
    {

        yield return new WaitUntil(() => getBoostIdReady);

        if (getBoostIdComplete == 1)
        {
            getBoostIdReady = false;
            getBoostIdComplete = -1;
            SetLocalCurrentBoostId();
        }

        else if (getBoostIdComplete == 0)
        {
            getBoostIdReady = false;
            getBoostIdComplete = -1;
            Debug.Log("FAILLL");
        }
    }

    private void SetLocalCurrentBoostId()
    {
        DataCenter.SetCurrentBoostId(BoostId);
    }


    //DIAMONDS

    public void AddDiamonds(double diamondsNum)
    {
        double diamonds = 0;

        mDatabaseRef.Child("users").Child(Id).Child("stuff").Child("diamonds").GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                addDiamondsComplete = 0;
                addDiamondsReady = true;
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                diamonds = Convert.ToDouble(snapshot.Value);
                mDatabaseRef.Child("users").Child(Id).Child("stuff").Child("diamonds").SetValueAsync(diamonds + diamondsNum);
                addDiamondsComplete = 1;
                addDiamondsReady = true;
            }
        });

        StartCoroutine(WaitingAddingDiamondsCoroutine(diamondsNum));
    }

    IEnumerator WaitingAddingDiamondsCoroutine(double diamondsNum)
    {
        yield return new WaitUntil(() => addDiamondsReady);

        if (addDiamondsComplete == 1)
        {
            addDiamondsReady = false;
            addDiamondsComplete = -1;
            DataCenter.AddDiamonds(diamondsNum);
        }

        else if (addDiamondsComplete == 0)
        {
            addDiamondsReady = false;
            addDiamondsComplete = -1;
            Debug.Log("FAILLL");
        }
    }

    public void GetDiamonds()
    {
        double diamonds = -1;
        mDatabaseRef.Child("users").Child(Id).Child("stuff").Child("diamonds").GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                getDiamondsComplete = 0;
                getDiamondsReady = true;
            }

            else if (task.IsCompleted)
            {
                DataSnapshot snapshot1 = task.Result;
                diamonds = Convert.ToDouble(snapshot1.Value);
                Diamonds = diamonds;
                getDiamondsComplete = 1;
                getDiamondsReady = true;
            }
        });
        StartCoroutine(WaitingGettingDiamondsCoroutine());
    }

    IEnumerator WaitingGettingDiamondsCoroutine()
    {

        yield return new WaitUntil(() => getDiamondsReady);

        if (getDiamondsComplete == 1)
        {
            getDiamondsReady = false;
            getDiamondsComplete = -1;
            SetLocalDiamonds();
        }

        else if (getDiamondsComplete == 0)
        {
            getDiamondsReady = false;
            getDiamondsComplete = -1;
            Debug.Log("FAILLL");
        }
    }

    private void SetLocalDiamonds()
    {
        DataCenter.SetLocalDiamonds(Diamonds);
    }

    // COINS

    public void AddCoins(int cityId, int buildingId, double coinsNum)
    {
        double coins = 0;

        mDatabaseRef.Child("users").Child(Id).Child("stuff").Child("coins").Child("cityId_" + cityId + "_buildingId_" + buildingId).GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                addCoinsComplete = 0;
                addCoinsReady = true;
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                coins = Convert.ToDouble(snapshot.Value);
                mDatabaseRef.Child("users").Child(Id).Child("stuff").Child("coins").Child("cityId_" + cityId + "_buildingId_" + buildingId).SetValueAsync(coins + coinsNum);
                addCoinsComplete = 1;
                addCoinsReady = true;
            }
        });

        StartCoroutine(WaitingAddingCoinsCoroutine(cityId, buildingId, coinsNum));
    }

    IEnumerator WaitingAddingCoinsCoroutine(int cityId, int buildingId, double coinsNum)
    {
        yield return new WaitUntil(() => addCoinsReady);

        if (addCoinsComplete == 1)
        {
            addCoinsReady = false;
            addCoinsComplete = -1;
            DataCenter.AddCoins(cityId, buildingId, coinsNum);
        }

        else if (addCoinsComplete == 0)
        {
            addCoinsReady = false;
            addCoinsComplete = -1;
            Debug.Log("FAILLL");
        }
    }

    public void GetCoins(int cityId, int buildingId)
    {
        double coins = -1;
        mDatabaseRef.Child("users").Child(Id).Child("stuff").Child("coins").Child("cityId_" + cityId + "_buildingId_" + buildingId).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                getCoinsComplete = 0;
                getCoinsReady = true;
            }

            else if (task.IsCompleted)
            {
                DataSnapshot snapshot1 = task.Result;
                coins = Convert.ToDouble(snapshot1.Value);
                Coins = coins;
                getCoinsComplete = 1;
                getCoinsReady = true;
            }
        });
        StartCoroutine(WaitingGettingCoinsCoroutine(cityId, buildingId));
    }

    IEnumerator WaitingGettingCoinsCoroutine(int cityId, int buildingId)
    {

        yield return new WaitUntil(() => getCoinsReady);

        if (getCoinsComplete == 1)
        {
            getCoinsReady = false;
            getCoinsComplete = -1;
            SetLocalCoins(cityId, buildingId);
        }

        else if (getCoinsComplete == 0)
        {
            getCoinsReady = false;
            getCoinsComplete = -1;
            Debug.Log("FAILLL");
        }
    }

    private void SetLocalCoins(int cityId, int buildingId)
    {
        DataCenter.SetLocalCoins(cityId, buildingId, Coins);
    }

    // BROKERS LEVEL

    public void AddBrokersLevel(int cityId, int buildingId, int floorId, double levelNum)
    {
        double brokersLevel = 0;

        mDatabaseRef.Child("users").Child(Id).Child("brokersLevel").Child("cityId_" + cityId + "_buildingId_" + buildingId + "_floorId_" + floorId).GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                addBrokerLevelComplete = 0;
                addBrokerLevelReady = true;
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                brokersLevel = Convert.ToDouble(snapshot.Value);
                mDatabaseRef.Child("users").Child(Id).Child("brokersLevel").Child("cityId_" + cityId + "_buildingId_" + buildingId + "_floorId_" + floorId).SetValueAsync(brokersLevel + levelNum);
                addBrokerLevelComplete = 1;
                addBrokerLevelReady = true;
            }
        });

        StartCoroutine(WaitingAddingBrokersLevelCoroutine(cityId, buildingId, floorId, levelNum));
    }

    IEnumerator WaitingAddingBrokersLevelCoroutine(int cityId, int buildingId, int floorId, double levelNum)
    {
        yield return new WaitUntil(() => addBrokerLevelReady);

        if (addBrokerLevelComplete == 1)
        {
            addBrokerLevelReady = false;
            addBrokerLevelComplete = -1;
            DataCenter.AddBrokersLevel(cityId, buildingId, floorId, levelNum);
        }

        else if (addBrokerLevelComplete == 0)
        {
            addBrokerLevelReady = false;
            addBrokerLevelComplete = -1;
            Debug.Log("FAILLL");
        }
    }

    public void GetBrokersLevel(int cityId, int buildingId, int floorId)
    {
        double brokersLevel = -1;
        mDatabaseRef.Child("users").Child(Id).Child("brokersLevel").Child("cityId_" + cityId + "_buildingId_" + buildingId + "_floorId_" + floorId).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                getBrokerLevelComplete = 0;
                getBrokerLevelReady = true;
            }

            else if (task.IsCompleted)
            {
                DataSnapshot snapshot1 = task.Result;
                brokersLevel = Convert.ToDouble(snapshot1.Value);
                BrokersLevel = brokersLevel;
                getBrokerLevelComplete = 1;
                getBrokerLevelReady = true;
            }
        });
        StartCoroutine(WaitingGettingBrokersLevelCoroutine(cityId, buildingId, floorId));
    }

    IEnumerator WaitingGettingBrokersLevelCoroutine(int cityId, int buildingId, int floorId)
    {

        yield return new WaitUntil(() => getBrokerLevelReady);

        if (getBrokerLevelComplete == 1)
        {
            getBrokerLevelReady = false;
            getBrokerLevelComplete = -1;
            SetBrokerLocalLevel(cityId, buildingId, floorId);
        }

        else if (getBrokerLevelComplete == 0)
        {
            getBrokerLevelReady = false;
            getBrokerLevelComplete = -1;
            Debug.Log("FAILLL");
        }
    }

    private void SetBrokerLocalLevel(int cityId, int buildingId, int floorId)
    {
        DataCenter.SetLocalBrokersLevel(cityId, buildingId, floorId, BrokersLevel);
        onFloorState?.Invoke();
        floorId++;

        if (floorId < 12)
        {
            GetBrokersLevel(cityId, buildingId, floorId);
        }
    }

    // MANAGERS LEVEL

    public void AddManagerLevel(int cityId, int buildingId, int floorId, int typeId, double levelNum)
    {
        double managerLevel = 0;
        mDatabaseRef.Child("users").Child(Id).Child("managerLevel").Child("cityId_" + cityId + "_buildingId_" + buildingId + "_floorId_" + floorId + "_typeId_" + typeId).GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                addManagerLevelComplete = 0;
                addManagerLevelReady = true;
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                managerLevel = Convert.ToDouble(snapshot.Value);
                mDatabaseRef.Child("users").Child(Id).Child("managerLevel").Child("cityId_" + cityId + "_buildingId_" + buildingId + "_floorId_" + floorId + "_typeId_" + typeId).SetValueAsync(managerLevel + levelNum);
                addManagerLevelComplete = 1;
                addManagerLevelReady = true;
            }
        });

        StartCoroutine(WaitingAddingManagerLevelCoroutine(cityId, buildingId, floorId, typeId, levelNum));
    }

    IEnumerator WaitingAddingManagerLevelCoroutine(int cityId, int buildingId, int floorId, int typeId, double levelNum)
    {
        yield return new WaitUntil(() => addManagerLevelReady);

        if (addManagerLevelComplete == 1)
        {
            addManagerLevelReady = false;
            addManagerLevelComplete = -1;
            DataCenter.AddManagerLevel(cityId, buildingId, floorId, typeId, levelNum);
        }

        else if (addManagerLevelComplete == 0)
        {
            addManagerLevelReady = false;
            addManagerLevelComplete = -1;
            Debug.Log("FAILLL");
        }
    }

    public void GetManagerLevel(int cityId, int buildingId, int floorId, int typeId)
    {
        double managerLevel = -1;
        mDatabaseRef.Child("users").Child(Id).Child("managerLevel").Child("cityId_" + cityId + "_buildingId_" + buildingId + "_floorId_" + floorId + "_typeId_" + typeId).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                getManagerLevelComplete = 0;
                getManagerLevelReady = true;
            }

            else if (task.IsCompleted)
            {
                DataSnapshot snapshot1 = task.Result;
                managerLevel = Convert.ToDouble(snapshot1.Value);
                ManagerLevel = managerLevel;
                getManagerLevelComplete = 1;
                getManagerLevelReady = true;
            }
        });
        StartCoroutine(WaitingGettingManagerLevelCoroutine(cityId, buildingId, floorId, typeId));
    }

    IEnumerator WaitingGettingManagerLevelCoroutine(int cityId, int buildingId, int floorId, int typeId)
    {

        yield return new WaitUntil(() => getManagerLevelReady);

        if (getManagerLevelComplete == 1)
        {
            getManagerLevelReady = false;
            getManagerLevelComplete = -1;
            SetManagerLocalLevel(cityId, buildingId, floorId, typeId);
        }

        else if (getManagerLevelComplete == 0)
        {
            getManagerLevelReady = false;
            getManagerLevelComplete = -1;
            Debug.Log("FAILLL");
        }
    }

    private void SetManagerLocalLevel(int cityId, int buildingId, int floorId, int typeId)
    {
        DataCenter.SetLocalManagerLevel(cityId, buildingId, floorId, typeId, ManagerLevel);
        typeId++;
        if (typeId == 4)
        {
            typeId = 0;
            floorId++;
        }

        if (floorId < 12)
        {
            GetManagerLevel(cityId, buildingId, floorId, typeId);
        }
    }
    // WORKERS STARS


    public void AddBrokerStars(int brokerType, int brokerId)
    {
        if (DataCenter.GetBrokerStars(brokerType, brokerId) < 5)
        {
            int brokerStars = 0;
            int starsNum = 1;
            mDatabaseRef.Child("users").Child(Id).Child("brokerStars").Child("brokerType_" + brokerType + "_brokerId_" + brokerId).GetValueAsync().ContinueWith(task => {
                if (task.IsFaulted)
                {
                    addBrokerStarsComplete = 0;
                    addBrokerStarsReady = true;
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;
                    brokerStars = Convert.ToInt32(snapshot.Value);
                    mDatabaseRef.Child("users").Child(Id).Child("brokerStars").Child("brokerType_" + brokerType + "_brokerId_" + brokerId).SetValueAsync(brokerStars + starsNum);
                    addBrokerStarsComplete = 1;
                    addBrokerStarsReady = true;
                }
            });
            StartCoroutine(WaitingAddingBrokerStarsCoroutine(brokerType, brokerId));
        }
    }

    IEnumerator WaitingAddingBrokerStarsCoroutine(int type, int id)
    {
        yield return new WaitUntil(() => addBrokerStarsReady);

        if (addBrokerStarsComplete == 1)
        {
            addBrokerStarsReady = false;
            addBrokerStarsComplete = -1;
            DataCenter.AddBrokerStars(type, id);
        }

        else if (addBrokerStarsComplete == 0)
        {
            addBrokerStarsReady = false;
            addBrokerStarsComplete = -1;
            Debug.Log("FAILLL");
        }
    }

    public void GetBrokerStars(int brokerType, int brokerId)
    {
        int brokerStars = -1;
        mDatabaseRef.Child("users").Child(Id).Child("brokerStars").Child("brokerType_" + brokerType + "_brokerId_" + brokerId).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                getBrokerStarsComplete = 0;
                getBrokerStarsReady = true;
            }

            else if (task.IsCompleted)
            {
                DataSnapshot snapshot1 = task.Result;
                brokerStars = Convert.ToInt32(snapshot1.Value);
                BrokerStars = brokerStars;
                getBrokerStarsComplete = 1;
                getBrokerStarsReady = true;
            }
        });
        StartCoroutine(WaitingGettingBrokerStarsCoroutine(brokerType, brokerId));
    }

    IEnumerator WaitingGettingBrokerStarsCoroutine(int type, int id)
    {
        yield return new WaitUntil(() => getBrokerStarsReady);

        if (getBrokerStarsComplete == 1)
        {
            getBrokerStarsReady = false;
            getBrokerStarsComplete = -1;
            SetBrokerLocalStars(type, id);
        }

        else if (getBrokerStarsComplete == 0)
        {
            getBrokerStarsReady = false;
            getBrokerStarsComplete = -1;
            Debug.Log("FAILLL");
        }
    }

    private void SetBrokerLocalStars(int type, int id)
    {
        DataCenter.SetLocalBrokerStars(type, id, BrokerStars);
        id++;
        if (id == 3)
        {
            id = 0;
            type++;
        }
        if (type < 4)
        {
            GetBrokerStars(type, id);
        }

        if (type == 4)
        {
            if (DataCenter.GetBrokerStars(0, 0) == 0)
            {
                Debug.Log("LOOOK");
                for (int i = 0; i < 3; i++)
                {
                    mDatabaseRef.Child("users").Child(Id).Child("brokerStars").Child("brokerType_" + 0 + "_brokerId_" + i).SetValueAsync(1);
                    DataCenter.AddBrokerStars(0, i);
                }
            }
        }

    }

    public void AddManagerStars(int managerType, int managerId)
    {
        if (DataCenter.GetManagerStars(managerType, managerId) < 5)
        {
            int managerStars = 0;
            int starsNum = 1;
            mDatabaseRef.Child("users").Child(Id).Child("managerStars").Child("managerType_" + managerType + "_managerId_" + managerId).GetValueAsync().ContinueWith(task => {
                if (task.IsFaulted)
                {
                    addManagerStarsComplete = 0;
                    addManagerStarsReady = true;
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;
                    managerStars = Convert.ToInt32(snapshot.Value);
                    mDatabaseRef.Child("users").Child(Id).Child("managerStars").Child("managerType_" + managerType + "_managerId_" + managerId).SetValueAsync(managerStars + starsNum);
                    addManagerStarsComplete = 1;
                    addManagerStarsReady = true;

                }
            });
            StartCoroutine(WaitingAddingManagerStarsCoroutine(managerType, managerId));
        }
    }

    IEnumerator WaitingAddingManagerStarsCoroutine(int type, int id)
    {
        yield return new WaitUntil(() => addManagerStarsReady);

        if (addManagerStarsComplete == 1)
        {
            addManagerStarsComplete = -1;
            addManagerStarsReady = false;
            DataCenter.AddManagerStars(type, id);
        }

        else if (addManagerStarsComplete == 0)
        {
            addManagerStarsComplete = -1;
            addManagerStarsReady = false;
            Debug.Log("FAILLL");
        }

    }

    public void GetManagerStars(int managerType, int managerId)
    {
        int managerStars = -1;
        mDatabaseRef.Child("users").Child(Id).Child("managerStars").Child("managerType_" + managerType + "_managerId_" + managerId).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                getManagerStarsComplete = 0;
                getManagerStarsReady = true;

            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot1 = task.Result;
                managerStars = Convert.ToInt32(snapshot1.Value);
                ManagerStars = managerStars;
                getManagerStarsComplete = 1;
                getManagerStarsReady = true;

            }

        });
        StartCoroutine(WaitingGettingManagerStarsCoroutine(managerType, managerId));
    }

    IEnumerator WaitingGettingManagerStarsCoroutine(int type, int id)
    {

        yield return new WaitUntil(() => getManagerStarsReady);

        if (getManagerStarsComplete == 1)
        {
            getManagerStarsComplete = -1;
            getManagerStarsReady = false;
            SetManagerLocalStars(type, id);
        }

        else if (getManagerStarsComplete == 0)
        {
            getManagerStarsComplete = -1;
            getManagerStarsReady = false;
            Debug.Log("FAILLL");
        }

    }

    private void SetManagerLocalStars(int type, int id)
    {
        DataCenter.SetManagerStars(type, id, ManagerStars);
        id++;
        if (type > 0 && id == 2)
        {
            id = 0;
            type++;
        }
        if (id == 12 && type == 0)
        {
            id = 0;
            type++;
        }
        if (type < 4)
        {
            GetManagerStars(type, id);
        }

        if (type == 4)
        {
            if (DataCenter.GetManagerStars(0, 0) == 0)
            {
                for (int i = 0; i < 12; i++)
                {
                    mDatabaseRef.Child("users").Child(Id).Child("managerStars").Child("managerType_" + 0 + "_managerId_" + i).SetValueAsync(1);
                    DataCenter.AddManagerStars(0, i);
                }
            }
        }

    }


    // WORKERS CARDS


    public void SetBrokerCards(int brokerType, int brokerId, double cards)
    {
        mDatabaseRef.Child("users").Child(Id).Child("brokerCards").Child("brokerType_" + brokerType + "_brokerId_" + brokerId).SetValueAsync(cards);
    }

    public void AddBrokerCards(int brokerType, int brokerId, double cardsNum)
    {
        double brokerCards = 0;
        mDatabaseRef.Child("users").Child(Id).Child("brokerCards").Child("brokerType_" + brokerType + "_brokerId_" + brokerId).GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                addBrokerCardsComplete = 0;
                addBrokerCardsReady = true;
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                brokerCards = Convert.ToDouble(snapshot.Value);
                mDatabaseRef.Child("users").Child(Id).Child("brokerCards").Child("brokerType_" + brokerType + "_brokerId_" + brokerId).SetValueAsync(brokerCards + cardsNum);
                addBrokerCardsComplete = 1;
                addBrokerCardsReady = true;
            }
        });
        StartCoroutine(WaitingAddingBrokerCardsCoroutine(brokerType, brokerId, cardsNum));
    }

    IEnumerator WaitingAddingBrokerCardsCoroutine(int brokerType, int brokerId, double cardsNum)
    {
        yield return new WaitUntil(() => addBrokerCardsReady);

        if (addBrokerCardsComplete == 1)
        {
            addBrokerCardsComplete = -1;
            addBrokerCardsReady = false;
            DataCenter.AddBrokerCards(brokerType, brokerId, cardsNum);
        }

        else if (addBrokerCardsComplete == 0)
        {
            addBrokerCardsComplete = -1;
            addBrokerCardsReady = false;
            Debug.Log("FAILLL");
        }
    }

    public void GetBrokerCards(int brokerType, int brokerId)
    {
        double brokerCards = -1;
        mDatabaseRef.Child("users").Child(Id).Child("brokerCards").Child("brokerType_" + brokerType + "_brokerId_" + brokerId).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                getBrokerCardsComplete = 0;
                getBrokerCardsReady = true;
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot1 = task.Result;
                brokerCards = Convert.ToDouble(snapshot1.Value);
                BrokerCards = brokerCards;
                getBrokerCardsComplete = 1;
                getBrokerCardsReady = true;
            }

        });
        StartCoroutine(WaitingGettingBrokerCardsCoroutine(brokerType, brokerId));
    }

    IEnumerator WaitingGettingBrokerCardsCoroutine(int type, int id)
    {
        yield return new WaitUntil(() => getBrokerCardsReady);

        if (getBrokerCardsComplete == 1)
        {
            getBrokerCardsComplete = -1;
            getBrokerCardsReady = false;
            SetBrokerLocalCards(type, id);
        }

        else if (getBrokerCardsComplete == 0)
        {
            getBrokerCardsComplete = -1;
            getBrokerCardsReady = false;
            Debug.Log("FAILLL");
        }
    }

    private void SetBrokerLocalCards(int type, int id)
    {
        DataCenter.SetLocalBrokerCards(type, id, BrokerCards);
        id++;
        if (id == 3)
        {
            id = 0;
            type++;
        }
        if (type < 4)
        {
            GetBrokerCards(type, id);
        }
    }

    //

    public void SetManagerCards(int managerType, int managerId, double cards)
    {
        mDatabaseRef.Child("users").Child(Id).Child("managerCards").Child("managerType_" + managerType + "_managerId_" + managerId).SetValueAsync(cards);
    }

    public void AddManagerCards(int managerType, int managerId, double cardsNum)
    {
        double managerCards = 0;
        mDatabaseRef.Child("users").Child(Id).Child("managerCards").Child("managerType_" + managerType + "_managerId_" + managerId).GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                addManagerCardsComplete = 0;
                addManagerCardsReady = true;
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                managerCards = Convert.ToDouble(snapshot.Value);
                mDatabaseRef.Child("users").Child(Id).Child("managerCards").Child("managerType_" + managerType + "_managerId_" + managerId).SetValueAsync(managerCards + cardsNum);
                addManagerCardsComplete = 1;
                addManagerCardsReady = true;
            }
        });
        StartCoroutine(WaitingAddingManagerCardsCoroutine(managerType, managerId, cardsNum));
    }

    IEnumerator WaitingAddingManagerCardsCoroutine(int managerType, int managerId, double cardsNum)
    {
        yield return new WaitUntil(() => addManagerCardsReady);

        if (addManagerCardsComplete == 1)
        {
            addManagerCardsComplete = -1;
            addManagerCardsReady = false;
            DataCenter.AddManagerCards(managerType, managerId, cardsNum);
        }

        else if (addManagerCardsComplete == 0)
        {
            addManagerCardsComplete = -1;
            addManagerCardsReady = false;
            Debug.Log("FAILLL");
        }
    }

    public void GetManagerCards(int managerType, int managerId)
    {
        double managerCards = -1;
        mDatabaseRef.Child("users").Child(Id).Child("managerCards").Child("managerType_" + managerType + "_managerId_" + managerId).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                getManagerCardsComplete = 0;
                getManagerCardsReady = true;
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot1 = task.Result;
                managerCards = Convert.ToDouble(snapshot1.Value);
                ManagerCards = managerCards;
                getManagerCardsComplete = 1;
                getManagerCardsReady = true;
            }

        });
        StartCoroutine(WaitingGettingManagerCardsCoroutine(managerType, managerId));
    }

    IEnumerator WaitingGettingManagerCardsCoroutine(int type, int id)
    {
        yield return new WaitUntil(() => getManagerCardsReady);
        if (getManagerCardsComplete == 1)
        {
            getManagerCardsComplete = -1;
            getManagerCardsReady = false;
            SetManagerLocalCards(type, id);
        }

        else if (getManagerCardsComplete == 0)
        {
            getManagerCardsComplete = -1;
            getManagerCardsReady = false;
            Debug.Log("FAILLL");
        }
    }

    private void SetManagerLocalCards(int type, int id)
    {
        DataCenter.SetLocalManagerCards(type, id, ManagerCards);
        id++;
        if (id == 12 && type == 0)
        {
            id = 0;
            type++;
        }

        if (type > 0 && id == 2)
        {
            id = 0;
            type++;
        }

        if (type < 4)
        {
            GetManagerCards(type, id);
        }
    }

    /*public delegate void GetUserCallback(User user);
    /// <summary>
    /// Retrieves a user from the Firebase Database, given their id
    /// </summary>
    /// <param name="userId"> Id of the user that we are looking for </param>
    /// <param name="callback"> What to do after the user is downloaded successfully </param>
    public static void GetUser(string userId, GetUserCallback callback)
    {
        RestClient.Get<User>($"{databaseURL}users/{userId}.json").Then(user =>
        {
            callback(user);
        });
    }*/

}
public class Uxser {
    public List<double> coins;
    public double diamonds;

    public Uxser() {
    }

    public Uxser(List<double> coins, double diamonds) {
        this.coins = coins;
        this.diamonds = diamonds;
    }
}

public class WorkersCards
{
    public List<double> coins;
    public double diamonds;

    public WorkersCards()
    {
    }

    public WorkersCards(List<double> coins, double diamonds)
    {
        this.coins = coins;
        this.diamonds = diamonds;
    }
}


// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using Firebase;
// using Firebase.Auth;
// using Firebase.Database;
// using Firebase.Unity.Editor;
//
// public class SaveDataa : MonoBehaviour
// {
//     // Start is called before the first frame update
//     public void Start()
//     {
//         Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
//         Firebase.Auth.FirebaseUser user = auth.CurrentUser;
//         if (user != null) {
//             string playerName = user.DisplayName;
//
//             // The user's Id, unique to the Firebase project.
//             // Do NOT use this value to authenticate with your backend server, if you
//             // have one; use User.TokenAsync() instead.
//             string uid = user.UserId;
//         }
//         // Set up the Editor before calling into the realtime database.
//         FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://wall-street-idle-66093686.firebaseio.com/");
//     }
//     
//     private void writeNewUser (string userId) {
//         DatabaseReference mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
//         User user = new User();
//         string json = JsonUtility.ToJson(user);
//
//         mDatabaseRef.Child("users").Child(userId).SetRawJsonValueAsync(json);
//     }
//
//     // Update is called once per frame
//     void Update()
//     {
//         
//     }
// }
