﻿using System;
using UnityEngine;

public static class MathExtensions
{
    public static float Angle2D(Vector3 a, Vector3 b)
    {
        return VectorAngle2D(b - a);
    }

    public static long CeilToLong(double value)
    {
        return (long)Math.Ceiling(value);
    }

    public static Vector3 ClearZ(Vector3 v)
    {
        v.z = 0f;
        return v;
    }

    public static long Max(long value1, long value2)
    {
        if (value1 >= value2)
            return value1;

        return value2;
    }

    public static long Min(long value1, long value2)
    {
        if (value1 >= value2)
            return value2;

        return value1;
    }

    public static Vector2 ToVector2(Vector3 v)
    {
        return new Vector2(v.x, v.y);
    }

    public static Vector3 ToVector3(Vector2 v)
    {
        return new Vector3(v.x, v.y);
    }

    public static float VectorAngle2D(Vector3 v)
    {
        float num = Mathf.Atan(v.y / v.x);

        if (v.x < 0f)
            return (num + 3.141593f);

        if (v.y < 0f)
            return (num + 6.283185f);

        return num;
    }
}