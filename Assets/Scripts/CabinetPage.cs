﻿using UnityEngine;

public class CabinetPage : Page
{
    [SerializeField] private GameObject leftBar;

    public void ShowWorkersWindow()
    {
        leftBar.SetActive(false);
        WorkersWindow workersWindow = WindowManager.Instance.GetWindow<WorkersWindow>();
        workersWindow.onClickCloseButton = () =>
        {
            workersWindow.Hide();
            leftBar.SetActive(true);
        };
        workersWindow.Show();
    }
}
