﻿using UnityEngine;

public class GameSettings
{
    const float SOUND_VOLUME = 0.5f;
    const float MUSIC_VOLUME = 0.5f;
    const float VOICE_VOLUME = 0.5f;

    public static readonly System.Collections.Generic.List<string> LANGUAGES = new System.Collections.Generic.List<string>()
    {
            "EN",
            "RU",
            "de-DE",
            "it-IT",
            "es-ES",
            "es-419",
            "fr-FR",
            "cn-CN",
            "kr-KR",
            "jp-JP",
            "pt-PT",
            "pt-BR",
            "cn-TW",
            "pl-PL",
            "tr-TR",
            "vi",
            "id",
            "ms",
            "th"
    };

    public static readonly System.Collections.Generic.List<string> LANGUAGES_NAME = new System.Collections.Generic.List<string>()
    {
        "English",
        "Русский",
        "Deutsch",
        "Italiano",
        "Español (Europa)",
        "Español (América Latina)",
        "Français",
        "简体中文",
        "한국어",
        "日本語",
        "Português (Europa)",
        "Português (Brasil)",
        "繁體中文",
        "Polski",
        "Türkçe",
        "Tiếng Việt",
        "Indonesia",
        "Melayu",
        "ไทย"
    };

    public static string GetDefaultLanguage(SystemLanguage systemLanguage)
    {
        switch (systemLanguage)
        {
            case SystemLanguage.Russian:
                return "RU";
            case SystemLanguage.German:
                return "de-DE";
            case SystemLanguage.Italian:
                return "it-IT";
            case SystemLanguage.Spanish:
                return "es-ES";
            // "es-419" - spanish lat america
            case SystemLanguage.French:
                return "fr-FR";
            case SystemLanguage.Portuguese:
                return "pt-PT";
            // "pt-BR" - порт браз
            case SystemLanguage.Chinese:
            case SystemLanguage.ChineseSimplified:
                return "cn-CN";
            case SystemLanguage.Korean:
                return "kr-KR";
            case SystemLanguage.Japanese:
                return "jp-JP";
            case SystemLanguage.ChineseTraditional:
                return "cn-TW";
            case SystemLanguage.Polish:
                return "pl-PL";
            case SystemLanguage.Turkish:
                return "tr-TR";
            case SystemLanguage.Vietnamese:
                return "vi";
            case SystemLanguage.Thai:
                return Application.platform == RuntimePlatform.IPhonePlayer ? "EN" : "th";
            case SystemLanguage.Indonesian:
                return "id";
            default:
                return "EN";
        }
    }

    public static int GetDefaultLanguageIndex(SystemLanguage systemLanguage)
    {
        return LANGUAGES.IndexOf(GetDefaultLanguage(systemLanguage));
    }

    public static int GetLanguaneIndex(string language)
    {
        if (LANGUAGES.Contains(language))
            return LANGUAGES.IndexOf(language);
        else
            return 0;
    }

    public static string Language
    {
        get { return PlayerPrefs.GetString("language", ""); }
        set { PlayerPrefs.SetString("language", value); }
    }

    //public static bool Sound
    //{
    //    get { return (AudioManager.Instance.GetVolume(AudioManager.E_AudioChanel.Sound) == 0.0f) ? false : true; }
    //    set { AudioManager.Instance.SetVolume(AudioManager.E_AudioChanel.Sound, value ? SOUND_VOLUME : 0.0f); }
    //}

    //public static bool Music
    //{
    //    get { return (AudioManager.Instance.GetVolume(AudioManager.E_AudioChanel.Music) == 0.0f) ? false : true; }
    //    set { AudioManager.Instance.SetVolume(AudioManager.E_AudioChanel.Music, value ? MUSIC_VOLUME : 0.0f); }
    //}

    //public static bool Voice
    //{
    //    get { return (AudioManager.Instance.GetVolume(AudioManager.E_AudioChanel.Voice) == 0.0f) ? false : true; }
    //    set { AudioManager.Instance.SetVolume(AudioManager.E_AudioChanel.Voice, value ? VOICE_VOLUME : 0.0f); }
    //}

    public static bool Notification
    {
        get { return (PlayerPrefs.GetInt("settings_notification", 1) != 0) ? true : false; }
        set { PlayerPrefs.SetInt("settings_notification", value ? 1 : 0); }
    }

    public static bool UnlockFrameRate
    {
        get { return (PlayerPrefs.GetInt("settings_unlock_framerate", 1) == 1) ? true : false; }
        set { PlayerPrefs.SetInt("settings_unlock_framerate", value ? 1 : 0); }
    }

    public static bool EnemiesAnimation
    {
        get { return (PlayerPrefs.GetInt("settings_enemies_animation", 1) == 1) ? true : false; }
        set { PlayerPrefs.SetInt("settings_enemies_animation", value ? 1 : 0); }
    }

    public static bool DropCurrency
    {
        get { return (PlayerPrefs.GetInt("settings_gold_drop", 1) == 1) ? true : false; }
        set { PlayerPrefs.SetInt("settings_gold_drop", value ? 1 : 0); }
    }

    public static bool FlyingTextPopUp
    {
        get { return (PlayerPrefs.GetInt("settings_flying_text", 1) == 1) ? true : false; }
        set { PlayerPrefs.SetInt("settings_flying_text", value ? 1 : 0); }
    }

    public static bool ChangeLocation
    {
        get { return (PlayerPrefs.GetInt("settings_change_location", 1) == 1) ? true : false; }
        set { PlayerPrefs.SetInt("settings_change_location", value ? 1 : 0); }
    }

    public static bool LockPrivateMessages
    {
        get { return (PlayerPrefs.GetInt("settings_unlock_pm", 0) == 1) ? true : false; }
        set { PlayerPrefs.SetInt("settings_unlock_pm", value ? 1 : 0); }
    }

    public static int AppStartCount
    {
        get { return PlayerPrefs.GetInt("settings_start_count", 0); }
        set { PlayerPrefs.SetInt("settings_start_count", value); }
    }

    public static bool IsAuthorizationRefused
    {
        get { return (PlayerPrefs.GetInt("settings_is_authorization_refused", 0) == 1) ? true : false; }
        set { PlayerPrefs.SetInt("settings_is_authorization_refused", value ? 1 : 0); }
    }
}
