﻿using CodeStage.AntiCheat.ObscuredTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    [SerializeField] private Transform container;

    [SerializeField] private Text time;

    [SerializeField] private Floor[] floors;
    void Start()
    {
        Debug.Log(DataCenter.GetNetworkTime() + "          hohoho");
        time.text = DataCenter.GetNetworkTime().ToString();
        Init();
        //Debug.Log(DataCenter.GetNetworkTime());
        //Debug.Log(DataCenter.OfflineTime.ToDateTime().AddDays(50).Ticks);
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            UpdateEverySecond();
        }

        else
        {
            Debug.Log("ERERERERERER");
        }

        //if (DataCenter.OfflineBonusGold != 0)
        //{
        //    CollectOfflineBonus();
        //}
    }
    private void OnEnable()
    {
        TimeManager.onEverySecondUpdate += UpdateEverySecond;
        WorkerInfo.onSelectWorkerChanged += Init;
    }

    private void OnDisable()
    {
        TimeManager.onEverySecondUpdate -= UpdateEverySecond;
        WorkerInfo.onSelectWorkerChanged -= Init;
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    private void Init()
    {
        int cityId = DataCenter.CurrentCity;
        int buildingId = DataCenter.CurrentBuilding;
        DataCenter.SetBuildingActiveWithId(cityId, buildingId);

        for (int i = 0; i < Config.FLOOR_NUMBER; i++)
            floors[i].Init(cityId, buildingId, i);

    }
    void UpdateEverySecond()
    {
        CheckOfflineBonus();
        //InterfaceController.Instance.ActiveOfflineBonusButton(true);
    }

    // offline bonus
    bool CheckOfflineBonus()
    {
        //if (!TutorialController.IsCompleteBasicTutorial)
        //    return false;

        if (DataCenter.OfflineBonusGold > 0.0f)
            return true;

        if (DataCenter.IsCheckOfflineBonus)
            return false;

        DataCenter.IsCheckOfflineBonus = true;
        Debug.Log(DataCenter.OfflineTime.ToDateTime() + "        5         " + DataCenter.OfflineTime);
        if (DataCenter.OfflineTime.ToDateTime() == DateTime.MinValue)
        {
            DataCenter.OfflineTime = DataCenter.GetNetworkTime().Ticks;

            return false;
        }

        int offlineTimeInSeconds = DataCenter.Instance.GetOfflineTimeSeconds();
        Debug.Log("offlineTimeInSeconds: " + offlineTimeInSeconds);
        Debug.Log("offlineTimeInSeconds: " + offlineTimeInSeconds.ToTimeFormat());

        int notificationOfflineBonusTimeMax = 0;

        for (int loop = 0; loop < GameConfig.Instance.notificationOfflineBonusTimes.Length; loop++)
            if (offlineTimeInSeconds >= GameConfig.Instance.notificationOfflineBonusTimes[loop])
                notificationOfflineBonusTimeMax = GameConfig.Instance.notificationOfflineBonusTimes[loop];

        if (notificationOfflineBonusTimeMax > 0)
        {
            DataCenter.OfflineBonusGold = Calculations.CalculateOfflineBonus(offlineTimeInSeconds, 10);

            Debug.Log("CheckOfflineBonus gold: " + DataCenter.OfflineBonusGold.ConvertForShow() + " time: " + offlineTimeInSeconds.ToTimeFormat());

            return true;
        }

        return false;
    }

    public void CollectOfflineBonus(int goldModifier = 1)
    {

        DataCenter.OfflineBonusGold = 0.0;
        DataCenter.OfflineTime = DataCenter.GetNetworkTime().Ticks;
        Debug.Log("YOU COLLECTED");
    }

    public void GetMoneyPerSecond()
    {
        //Calculations.GetMoneyPerSecondCount();
    }
}
