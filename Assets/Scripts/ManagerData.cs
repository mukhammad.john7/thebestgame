﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ManagerData", menuName = "ScriptableObject/ManagerData", order = 1)]
public class ManagerData : ScriptableObject
{
    [SerializeField] private int id;
    [SerializeField] private double floorCost;
    [SerializeField] private double repairCost;
    [SerializeField] private double brokersPower;
    [SerializeField] private double factorPower;
    [SerializeField] private float earnTime;


    public int Id => id;

    public double FloorCost => floorCost;

    public double RepairCost => repairCost;

    public double BrokersPower => brokersPower;

    public double FactorPower => factorPower;

    public float EarnTime => earnTime;
}
