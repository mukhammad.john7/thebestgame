﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

    // TODO add for vertical
public class PaginalScrollRect : ScrollRect
{
    public event Action<int> onPageChanged;
    public event Action<int> onPageStoped;

    [SerializeField] private bool isAutoInit = true;
    [SerializeField] private List<GameObject> pages;
    [SerializeField] private int homePageIndex;
    [SerializeField] private float lerpSpeed = 10f;
    [SerializeField] private bool swipeEnabled = true;
    [SerializeField] private bool fastSwipeEnabled = true;
    [SerializeField] private float fastSwipeFactorLimit = 0.2f;
    [SerializeField] private float fastSwipeDistanceLimit = 100f;

    private Rect pageRect;

    private Vector3 lerpTargetPosition;
    private bool isLerp;

    private bool isDragged;
    private Vector3 beginDragPosition;
    private float fastSwipeFactor;


    public List<GameObject> Pages { get { return pages; } }

    public int HomePageIndex
    {
        get { return homePageIndex; }
        set { homePageIndex = value; }
    }

    public int CurrPageIndex { get; private set; }

    public int PrevPageIndex { get; private set; }


    protected override void Awake()
    {
        base.Awake();

        if (isAutoInit)
            Init();
    }

    public void Init()
    {
        Canvas.ForceUpdateCanvases();

        CurrPageIndex = PrevPageIndex = homePageIndex;

        pageRect = (pages[homePageIndex].transform as RectTransform).rect;

        ShowPage(homePageIndex, false);
    }

    protected override void LateUpdate()
    {
        base.LateUpdate();

        if (isDragged)
        {
            if (fastSwipeEnabled)
                fastSwipeFactor += Time.deltaTime;
        }
        else if (isLerp)
        {
            content.localPosition = Vector3.Lerp(content.localPosition, lerpTargetPosition, lerpSpeed * Time.deltaTime);

            if (Vector3.Distance(content.localPosition, lerpTargetPosition) < 1.2f)
            {
                isLerp = false;
                content.localPosition = lerpTargetPosition;

                onPageStoped?.Invoke(CurrPageIndex);
            }
        }
    }

    public void ShowPage(int pageIndex, bool withLerp = true)
    {

        PrevPageIndex = CurrPageIndex;
        CurrPageIndex = pageIndex;
        lerpTargetPosition = -pages[CurrPageIndex].transform.localPosition;
        Debug.Log(pages[CurrPageIndex].transform.localPosition);
        onPageChanged?.Invoke(CurrPageIndex);

        if (withLerp)
        {
            isLerp = true;
        }
        else
        {
            isLerp = false;
            content.localPosition = lerpTargetPosition;

            onPageStoped?.Invoke(CurrPageIndex);
        }
    }

    [ContextMenu("ShowNextPage")]
    public void ShowNextPage()
    {
        if ((CurrPageIndex + 1) >= pages.Count)
            return;

        ShowPage(CurrPageIndex + 1);
    }

    [ContextMenu("ShowPrevPage")]
    public void ShowPrevPage()
    {
        if (CurrPageIndex <= 0)
            return;

        ShowPage(CurrPageIndex - 1);
    }

    private int GetPageIndexByPosition(Vector3 position)
    {
        float pos = Mathf.Abs(position.x) - pageRect.width / 2f;
        int pageIndex = Mathf.RoundToInt(pos / pageRect.width);

        return Mathf.Clamp(pageIndex, 0, pages.Count - 1);
    }

    // Subscribers

	public override void OnDrag(PointerEventData eventData)
	{
        if (!swipeEnabled)
        {
            eventData.Use();
            return;
        } 

        base.OnDrag(eventData);

        if (!isDragged)
            OnBeginDrag(eventData);
    }

	public override void OnBeginDrag(PointerEventData eventData)
	{
        if (!swipeEnabled)
        {
            eventData.Use();
            return;
        }

        base.OnBeginDrag(eventData);

        isDragged = true;
        isLerp = false;
        beginDragPosition = content.localPosition;
        fastSwipeFactor = 0f;
    }

	public override void OnEndDrag(PointerEventData eventData)
	{
        if (!swipeEnabled)
        {
            eventData.Use();
            return;
        }

        base.OnEndDrag(eventData);

        float distance;

        if (horizontal)
            distance = beginDragPosition.x - content.localPosition.x;
        else
            distance = beginDragPosition.y - content.localPosition.y;

        isDragged = false;

        if (fastSwipeEnabled && fastSwipeFactor < fastSwipeFactorLimit && Mathf.Abs(distance) > fastSwipeDistanceLimit)
        {
            if (distance > 0)
                ShowNextPage();
            else
                ShowPrevPage();
        }
        else
        {
            ShowPage(GetPageIndexByPosition(content.localPosition));
        }
    }
}
