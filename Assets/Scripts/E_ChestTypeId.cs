﻿public enum E_ChestTypeId
{
    SimpleChest = 0,
    Chest = 1,
    SilverChest = 2,
    GoldenChest = 3,
    EpicChest = 4,
    LegendaryChest = 5

}