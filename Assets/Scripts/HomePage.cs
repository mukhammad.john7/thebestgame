﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public sealed class HomePage : Page
{

    public override void Show()
    {
        base.Show();
        UpdateContent();
    }

    public override void OnUnFocus()
    {
        base.OnUnFocus();
    }



}
