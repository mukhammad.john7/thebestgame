﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TimeManager : SingletonMonoBehaviour<TimeManager>
{
    const float timeHightDelta = 0.2f;
    const float timeMidleDelta = 5f;

    public static event System.Action<float> onFarmeUpdate;
    public static event System.Action onEverySecondUpdate;

    public static event System.Action onTimeHightUpdate;
    public static event System.Action onTimeMidleUpdate;

    public static event Action<bool> onApplicationPause;

    float everySecondFactor;
    DateTime networkDateTime;
    bool isNetworkDateTimeLoaded;

    float timeHightFactor;
    float timeMidleFactor;

    DateTime networkStartTime;
    DateTime applicationDateTime;
    Coroutine checkTimeRoutine;

    public bool IsPause { get; set; }

    public DateTime NetworkDateTime
    {
        get
        {
            return networkDateTime;
        }
        set
        {
            networkDateTime = value;
            isNetworkDateTimeLoaded = true;
        }
    }

    public double NetworkDeltaTime { get; set; }

    public double ApplicationDeltaUtc { get; set; }


    void OnDisable()
    {
        ClearAllEvents();
    }

    void OnApplicationPause(bool isPause)
    {
#if !UNITY_EDITOR
        if (isPause)
        {
            IsPause = true;

            onApplicationPause?.Invoke(isPause);
        }
        else
            StartCoroutine(OnApplicationPauseCoroutine(isPause));
#endif
    }

    IEnumerator OnApplicationPauseCoroutine(bool isPause)
    {
        yield return null;

        IsPause = false;

        onApplicationPause?.Invoke(isPause);

        yield break;
    }

    protected override void Awake()
    {
        base.Awake();

        DontDestroyOnLoad(gameObject);
    }

    public void ClearAllEvents()
    {
        onFarmeUpdate = null;
        onEverySecondUpdate = null;

        onTimeHightUpdate = null;
        onTimeMidleUpdate = null;
    }

    private void Update()
    {
        if (IsPause)
            return;

        UpdateTime(Time.unscaledDeltaTime);
    }

    void UpdateTime(float deltaTime)
    {
        if (isNetworkDateTimeLoaded)
            networkDateTime = networkDateTime.AddSeconds((double)Time.unscaledDeltaTime);

        if (onFarmeUpdate != null)
            onFarmeUpdate(deltaTime);

        everySecondFactor += deltaTime;

        timeHightFactor += deltaTime;
        timeMidleFactor += deltaTime;

        if (everySecondFactor >= 1f)
        {
            everySecondFactor = 0f;

            if (onEverySecondUpdate != null)
                onEverySecondUpdate();
        }

        if (timeHightFactor >= timeHightDelta)
        {
            timeHightFactor = 0f;

            if (onTimeHightUpdate != null)
                onTimeHightUpdate();
        }

        if (timeMidleFactor >= timeMidleDelta)
        {
            timeMidleFactor = 0f;

            if (onTimeMidleUpdate != null)
                onTimeMidleUpdate();
        }
    }

    public void CheckTimeScale()
    {
        if (checkTimeRoutine == null && !IsPause)
            checkTimeRoutine = StartCoroutine(CheckTime());
    }

    IEnumerator CheckTime()
    {
        yield return new WaitForSecondsRealtime(1.5f);

        CheckSuspect();

        checkTimeRoutine = null;

        yield break;
    }

    void CheckSuspect()
    {
        if (applicationDateTime <= DateTime.MinValue || networkStartTime == DateTime.MinValue)
        {
            networkStartTime = NetworkDateTime;
            applicationDateTime = DateTime.UtcNow;
        }
        else
        {
            NetworkDeltaTime = NetworkDateTime.Subtract(networkStartTime).TotalSeconds;
            networkStartTime = NetworkDateTime;

            ApplicationDeltaUtc = DateTime.UtcNow.Subtract(applicationDateTime).TotalSeconds;
            applicationDateTime = DateTime.UtcNow;

            if (ApplicationDeltaUtc > 0 && NetworkDeltaTime > 0 && NetworkDeltaTime < GameConfig.Instance.speedhackEndTime && ApplicationDeltaUtc - NetworkDeltaTime > GameConfig.Instance.speedhackMinLimit)
            {
#if IS_DEBUG
                MessageWindow messageWindow = WindowManager.Instance.GetWindow<MessageWindow>();
                messageWindow.onClickOkButton = () => messageWindow.Hide();
                messageWindow.Show($"You are suspect!\nSpeedhack {(ApplicationDeltaUtc / NetworkDeltaTime).ToString("F2")}");
#endif

                if (!CheatController.IsSuspect || CheatController.SpeedhackAmount < ApplicationDeltaUtc / NetworkDeltaTime)
                {
                    CheatController.IsSuspect = true;
                    CheatController.SpeedhackAmount = ApplicationDeltaUtc / NetworkDeltaTime;

                    //NetworkController.Instance.SetSuspect(
                    //    $"speedhack {CheatController.SpeedhackAmount.ToString("F2")}",
                    //    () =>
                    //    {
                    //        PlayerPrefs.Save();
                    //    },
                    //    null);
                }
            }
        }
    }
}