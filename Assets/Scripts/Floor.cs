﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Floor : MonoBehaviour
{
    [SerializeField] private Text costText;
    [SerializeField] private Text timerText;
    [SerializeField] private Text managerLevelText;
    [SerializeField] private Slider timeBar;
    [SerializeField] private Button button;
    [SerializeField] private GameObject brokers;
    [SerializeField] private Image table;

    [SerializeField] private RectTransform managerPlace;
    [SerializeField] private RectTransform firstBrokerPlace;
    [SerializeField] private RectTransform secondBrokerPlace;
    [SerializeField] private RectTransform thirdBrokerPlace;
    [SerializeField] private Manager manager;
    [SerializeField] public FloorData FloorData;

    public static event Action onShowLeftBar;
    public static event Action onSkipLeftBar;

    private GameObject firstBroker;
    private GameObject secondBroker;
    private GameObject thirdBroker;

    public Action <float, float> onDurationTimerUpdate;

    public int CityId;
    public int BuildingId;
    public int EarTime = 0;
    public Timer timer;

    private void OnEnable()
    {
        DataCenter.onManagerChanged += CheckAutoMode;
        DataCenter.onWorkerLevelChanged += UpdateFloorState;
        SaveData.onFloorState += UpdateFloorState;
    }

    private void OnDisable()
    {
        DataCenter.onManagerChanged -= CheckAutoMode;
        DataCenter.onWorkerLevelChanged -= UpdateFloorState;
        SaveData.onFloorState -= UpdateFloorState;
    }

    public void Init(int cityId, int buildingId, int floorId)
    {
        CityId = cityId;
        BuildingId = buildingId;
        table.sprite = GameResources.GetTableSprite(cityId, buildingId);
        FloorData = GameResources.GetFloorData(cityId, buildingId, floorId);

        if (firstBroker != null)
            Destroy(firstBroker.gameObject);

        if (secondBroker != null)
            Destroy(secondBroker.gameObject);

        if (thirdBroker != null)
            Destroy(thirdBroker.gameObject);

        if (manager != null)
            Destroy(manager.gameObject);

        if (floorId == 0)
        {
            manager = Instantiate(GameResources.GetManagerPrefab(DataCenter.GetCurrentManagerType(cityId, buildingId, floorId), 0), managerPlace);
            firstBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 0), 0), firstBrokerPlace);
            secondBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 1), 1), secondBrokerPlace);
            thirdBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 2), 2), thirdBrokerPlace);
        }


        else if (floorId % 12 == 0)
        {
            manager = Instantiate(GameResources.GetManagerPrefab(DataCenter.GetCurrentManagerType(cityId, buildingId, floorId), 0), managerPlace);
            firstBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 0), 0), firstBrokerPlace);
            secondBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 1), 1), secondBrokerPlace);
            thirdBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 2), 2), thirdBrokerPlace);
        }

        else if (floorId % 11 == 0)
        {
            manager = Instantiate(GameResources.GetManagerPrefab(DataCenter.GetCurrentManagerType(cityId, buildingId, floorId), 11), managerPlace);
            firstBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 0), 0), firstBrokerPlace);
            secondBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 1), 1), secondBrokerPlace);
            thirdBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 2), 2), thirdBrokerPlace);
        }
            
        else if (floorId % 10 == 0)
        {
            manager = Instantiate(GameResources.GetManagerPrefab(DataCenter.GetCurrentManagerType(cityId, buildingId, floorId), 10), managerPlace);
            firstBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 0), 0), firstBrokerPlace);
            secondBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 1), 1), secondBrokerPlace);
            thirdBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 2), 2), thirdBrokerPlace);
        }
            
        else if (floorId % 9 == 0)
        {
            manager = Instantiate(GameResources.GetManagerPrefab(DataCenter.GetCurrentManagerType(cityId, buildingId, floorId), 9), managerPlace);
            firstBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 0), 0), firstBrokerPlace);
            secondBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 1), 1), secondBrokerPlace);
            thirdBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 2), 2), thirdBrokerPlace);
        }

        else if (floorId % 8 == 0)
        {
            manager = Instantiate(GameResources.GetManagerPrefab(DataCenter.GetCurrentManagerType(cityId, buildingId, floorId), 8), managerPlace);
            firstBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 0), 0), firstBrokerPlace);
            secondBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 1), 1), secondBrokerPlace);
            thirdBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 2), 2), thirdBrokerPlace);
        }
            
        else if (floorId % 7 == 0)
        {
            manager = Instantiate(GameResources.GetManagerPrefab(DataCenter.GetCurrentManagerType(cityId, buildingId, floorId), 7), managerPlace);
            firstBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 0), 0), firstBrokerPlace);
            secondBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 1), 1), secondBrokerPlace);
            thirdBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 2), 2), thirdBrokerPlace);
        }
            
        else if (floorId % 6 == 0)
        {
            manager = Instantiate(GameResources.GetManagerPrefab(DataCenter.GetCurrentManagerType(cityId, buildingId, floorId), 6), managerPlace);
            firstBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 0), 0), firstBrokerPlace);
            secondBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 1), 1), secondBrokerPlace);
            thirdBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 2), 2), thirdBrokerPlace);
        }
            
        else if (floorId % 5 == 0)
        {
            manager = Instantiate(GameResources.GetManagerPrefab(DataCenter.GetCurrentManagerType(cityId, buildingId, floorId), 5), managerPlace);
            firstBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 0), 0), firstBrokerPlace);
            secondBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 1), 1), secondBrokerPlace);
            thirdBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 2), 2), thirdBrokerPlace);
        }
            
        else if (floorId % 4 == 0)
        {
            manager = Instantiate(GameResources.GetManagerPrefab(DataCenter.GetCurrentManagerType(cityId, buildingId, floorId), 4), managerPlace);
            firstBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 0), 0), firstBrokerPlace);
            secondBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 1), 1), secondBrokerPlace);
            thirdBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 2), 2), thirdBrokerPlace);
        }
           
        else if (floorId % 3 == 0)
        {
            manager = Instantiate(GameResources.GetManagerPrefab(DataCenter.GetCurrentManagerType(cityId, buildingId, floorId), 3), managerPlace);
            firstBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 0), 0), firstBrokerPlace);
            secondBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 1), 1), secondBrokerPlace);
            thirdBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 2), 2), thirdBrokerPlace);
        }
            
        else if (floorId % 2 == 0)
        {
            manager = Instantiate(GameResources.GetManagerPrefab(DataCenter.GetCurrentManagerType(cityId, buildingId, floorId), 2), managerPlace);
            firstBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 0), 0), firstBrokerPlace);
            secondBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 1), 1), secondBrokerPlace);
            thirdBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 2), 2), thirdBrokerPlace);
        }
            
        else if (floorId % 1 == 0)
        {
            manager = Instantiate(GameResources.GetManagerPrefab(DataCenter.GetCurrentManagerType(cityId, buildingId, floorId), 1), managerPlace);
            firstBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 0), 0), firstBrokerPlace);
            secondBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 1), 1), secondBrokerPlace);
            thirdBroker = Instantiate(GameResources.GetBrokerPrefab(DataCenter.GetCurrentBrokerType(cityId, buildingId, floorId, 2), 2), thirdBrokerPlace);
        }
            
        manager.Init(cityId,buildingId, floorId);

        UpdateFloorState();
        CheckAutoMode();
    }



    private void UpdateFloorState()
    {
        int floorState = 0;
        //Debug.Log(floorState);
        button.onClick.RemoveAllListeners();
        //switch (floorState)
        //{
        //    case Config.FLOOR_SALE_STATE:
        //        costText.gameObject.SetActive(true);
        //        costText.text = Calculations.ConvertForShow(FloorData.FloorCost);
        //        manager.gameObject.SetActive(false);
        //        button.onClick.AddListener(OnClickBuyButton);
        //        break;

        //    case Config.FLOOR_BUYED_STATE:
        //        costText.gameObject.SetActive(false);
        //        brokers.SetActive(true);
        //        manager.gameObject.SetActive(true);
        //        timeBar.gameObject.SetActive(true);
        //        timerText.gameObject.SetActive(true);
        //        button.onClick.AddListener(OnClickEarnButton);
        //        break;

        //    case Config.FLOOR_BROKEN_STATE:
        //        manager.gameObject.SetActive(false);
        //        button.onClick.AddListener(OnClickRepairButton);
        //        break;

        //    case Config.FLOOR_SALE_BROKEN_STATE:
        //        costText.gameObject.SetActive(true);
        //        costText.text = Calculations.ConvertForShow(FloorData.FloorCost);
        //        manager.gameObject.SetActive(false);
        //        button.onClick.AddListener(OnClickBuyButton);
        //        break;
        //}

        if (DataCenter.GetBrokersLevel(CityId, BuildingId, FloorData.Id) != 0)
        {
            floorState = 1;
            costText.gameObject.SetActive(false);
            brokers.SetActive(true);
            manager.gameObject.SetActive(true);
            timeBar.gameObject.SetActive(true);
            timerText.gameObject.SetActive(true);
            button.onClick.AddListener(OnClickEarnButton);
        }
        else
        {
            floorState = 0;
            costText.gameObject.SetActive(true);
            costText.text = Calculations.ConvertForShow(FloorData.FloorCost);
            manager.gameObject.SetActive(false);
            button.onClick.AddListener(OnClickBuyButton);
        }
        

        gameObject.GetComponent<Image>().sprite = GameResources.GetFloorSprite(CityId, BuildingId, floorState);
    }

    private void CheckAutoMode()
    {
        if (DataCenter.GetManagerLevel(CityId, BuildingId, FloorData.Id, 0) != 0)
            if (timer == null)
                StartDurationTimer(Calculations.GetFloorEarnTime(FloorData.EarnTime, DataCenter.GetManagerLevel(CityId, BuildingId, FloorData.Id, DataCenter.GetCurrentManagerType(CityId, BuildingId, FloorData.Id)), Config.WORKER_BONUS((E_WorkerTypeId)DataCenter.GetCurrentManagerType(CityId, BuildingId, FloorData.Id)) * DataCenter.GetManagerStars(DataCenter.GetCurrentManagerType(CityId, BuildingId, FloorData.Id), FloorData.Id)));
    }

    // DurationTimer

    void StartDurationTimer(float time)
    {

        if (timer != null)
            timer.Stop();

        timer = new Timer(time, OnDurationTimerComplete, OnDurationTimerUpdate);
    }

    void OnDurationTimerComplete()
    {
        SaveData.Instance.AddCoins(CityId, BuildingId, Calculations.GetFloorCoins(FloorData.BrokersPower, DataCenter.GetBrokersLevel(CityId, BuildingId, FloorData.Id), Config.WORKER_BONUS((E_WorkerTypeId)DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorData.Id, 0)) * DataCenter.GetBrokerStars(DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorData.Id, 0), 0),  Config.WORKER_BONUS((E_WorkerTypeId)DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorData.Id, 1)) * DataCenter.GetBrokerStars(DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorData.Id, 1), 1), Config.WORKER_BONUS((E_WorkerTypeId)DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorData.Id, 2)) * DataCenter.GetBrokerStars(DataCenter.GetCurrentBrokerType(CityId, BuildingId, FloorData.Id, 2), 2)));

        button.interactable = true;
        timer.Stop();
        Debug.Log(timer.RemainedTime + "   TI mE");
        Debug.Log(timer.PassedTime + "   TI mE");
                Debug.Log(timer.PassedTime + "   TI mE");
        timer = null;
        CheckAutoMode();
    }

    void OnDurationTimerUpdate(float remainedTime, float totalTime)
    {
        if (timeBar != null)
            timeBar.value = 1f - remainedTime / totalTime;

        if (timerText != null)
            timerText.text = remainedTime.ToTimeFormat();
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != "GameScene")
        {
            if (timer!= null)
                timer.Stop();
            timer = null;
        }

            
    }


    // click

    public void OnClickBuyButton()
    {
        if (DataCenter.GetCoins(CityId, BuildingId) >= FloorData.FloorCost)
        {
            SaveData.Instance.AddCoins(CityId, BuildingId, -FloorData.FloorCost);
            int floorState = DataCenter.GetFloorStateWithId(CityId,BuildingId, FloorData.Id) - 1;
            DataCenter.SetFloorStateWithId(CityId,BuildingId, FloorData.Id, floorState);

            //Debug.Log("Buyed    " + floorState + "     " + DataCenter.GetFloorStateWithId(areaId, floorData.Id));
            SaveData.Instance.AddBrokersLevel(CityId, BuildingId, FloorData.Id, 1);
        }

        else
        {

        }

    }
    public void OnClickRepairButton()
    {
        DataCenter.SetFloorStateWithId(CityId,BuildingId, FloorData.Id, 0);
        UpdateFloorState();
    }

    public void OnClickEarnButton()
    {
        if (timer == null)
        {
            button.interactable = false;
            StartDurationTimer(Calculations.GetFloorEarnTime(FloorData.EarnTime, DataCenter.GetManagerLevel(CityId, BuildingId, FloorData.Id, DataCenter.GetCurrentManagerType(CityId, BuildingId, FloorData.Id)), Config.WORKER_BONUS((E_WorkerTypeId)DataCenter.GetCurrentManagerType(CityId, BuildingId, FloorData.Id)) * DataCenter.GetManagerStars(DataCenter.GetCurrentManagerType(CityId, BuildingId, FloorData.Id), FloorData.Id)));

        }
    }

    public void OnBrokersButtonClick()
    {
        onSkipLeftBar?.Invoke();

        BrokersWindow brokersWindow = WindowManager.Instance.GetWindow<BrokersWindow>();
        brokersWindow.onClickCloseButton = () =>
        {
            brokersWindow.Hide();
            onShowLeftBar?.Invoke();
        };
        brokersWindow.Show(CityId, BuildingId, FloorData.Id);
    }

}
