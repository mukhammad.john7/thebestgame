﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceController : SingletonMonoBehaviour<InterfaceController>
{
    [SerializeField] private Text moneyText;
    [SerializeField] private Text crystalsText;
    [SerializeField] private Text boostText;

    [SerializeField] private ParticleSystem partSys;
    [SerializeField] private RectTransform firstThing;
    [SerializeField] private RectTransform topBar;
    [SerializeField] private GameObject leftBar;
    [SerializeField] private Image boostImage;
    BoostWindow boostWindow;

    private int cityId;
    private int buildingId;

    private void OnEnable()
    {
        DataCenter.onStuffChanged += SaveData_OnStuffChanged;
        DataCenter.onCurrentBoostChanged += BoostInfo;
        Manager.onShowLeftBar += ShowLeftBar;
        Manager.onSkipLeftBar += SkipLeftBar;

        Floor.onShowLeftBar += ShowLeftBar;
        Floor.onSkipLeftBar += SkipLeftBar;
    }

    private void OnDisable()
    {
        DataCenter.onStuffChanged -= SaveData_OnStuffChanged;
        DataCenter.onCurrentBoostChanged -= BoostInfo;
        Manager.onShowLeftBar -= ShowLeftBar;
        Manager.onSkipLeftBar -= SkipLeftBar;

        Floor.onShowLeftBar -= ShowLeftBar;
        Floor.onSkipLeftBar -= SkipLeftBar;
    }

    private void Start()
    {
        cityId = DataCenter.CurrentCity;
        buildingId = DataCenter.CurrentBuilding;
        OnClickBoostButton();

        if (ScreenInfo.Instance.IsSuperWide)
        {
            topBar.anchoredPosition = new Vector3(0f, -80f, 0f);
            firstThing.sizeDelta = new Vector2(1440f, 250f);
        }

        SaveData_OnStuffChanged();
        boostWindow.OnClickCloseButton();
        BoostInfo();
    }

    private void BoostInfo()
    {
        Debug.Log(DataCenter.GetCurrentBoostId() + "  BOOOST ID");
        boostImage.sprite = GameResources.GetBoostSprite(DataCenter.GetCurrentBoostId());
        boostText.text = Config.BOOST_NUM(DataCenter.GetCurrentBoostId()) + "x";
    }

    private void SaveData_OnStuffChanged()
    {
        moneyText.text = Calculations.ConvertForShow(DataCenter.GetCoins(cityId, buildingId));
        crystalsText.text = Calculations.ConvertForShow(DataCenter.GetStuffNum(E_StuffTypeId.Crystals));
    }

    public void OnClickStoreButton()
    {
        leftBar.SetActive(false);
        StoreWindow storeWindow = WindowManager.Instance.GetWindow<StoreWindow>();
        storeWindow.onClickCloseButton = () =>
        {
            storeWindow.Hide();
            leftBar.SetActive(true);
        };
        storeWindow.Show();
    }


    public void OnClickBoostButton()
    {
        leftBar.SetActive(false);
        boostWindow = WindowManager.Instance.GetWindow<BoostWindow>();
        boostWindow.onClickCloseButton = () =>
        {
            boostWindow.Hide();
            leftBar.SetActive(true);
        };
        boostWindow.Show();
    }

    public void OnClickSettingsButton()
    {
        leftBar.SetActive(false);
        SettingsWindow settingsWindow = WindowManager.Instance.GetWindow<SettingsWindow>();
        settingsWindow.onClickCloseButton = () =>
        {
            settingsWindow.Hide();
            leftBar.SetActive(true);
        };
        settingsWindow.Show();
    }

    public void ShowCoinParticle(Vector3 pos)
    {
        partSys.gameObject.transform.position = pos;
        partSys.Play();
    }

    public void ShowLeftBar()
    {
        leftBar.SetActive(true);
    }

    public void SkipLeftBar()
    {
        leftBar.SetActive(false);
    }
}
