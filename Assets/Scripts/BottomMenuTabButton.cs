﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BottomMenuTabButton : TabButton
{
    [Space]

    [SerializeField] private Image iconImage;
    [SerializeField] private Text nameText;
    [SerializeField] private float speed = 10f;


    private RectTransform butonRT;

    private readonly Vector2 selectButtonSizeDelta = new Vector2(280f, 170f);
    private readonly Vector2 unselectButtonSizeDelta = new Vector2(200f, 170f);

    private readonly Vector3 selectIconScale = new Vector3(1f, 1f, 1f);
    private readonly Vector3 unselectIconScale = new Vector3(0.76f, 0.76f, 1f);

    private readonly Color selectNameColor = new Color32(237, 194, 74, 255);
    private readonly Color unselectNameColor = new Color32(234, 214, 106, 255);

    private Coroutine animationCoroutine;


    public bool IsSelect { get; private set; }


    private void Awake()
    {
        butonRT = (RectTransform)button.transform;
    }

    protected override void Start()
    {
        button.onClick.AddListener(Button_OnClick);

        if (isOn)
            ApplyOn();
        else
            ApplyOff();
    }

    protected override void ApplyState()
    {
        base.ApplyState();

        if (isOn)
        {
            if (animationCoroutine != null)
                StopCoroutine(animationCoroutine);

            animationCoroutine = StartCoroutine(TurnOnCoroutine());
        }
        else
        {
            if (animationCoroutine != null)
                StopCoroutine(animationCoroutine);

            animationCoroutine = StartCoroutine(TurnOffCoroutine());
        }
    }

    private void ApplyOn()
    {

        butonRT.sizeDelta = selectButtonSizeDelta;
        iconImage.transform.localScale = selectIconScale;
        nameText.gameObject.transform.position = new Vector3(nameText.gameObject.transform.position.x, nameText.gameObject.transform.position.y - 28f, 0f);
        nameText.color = selectNameColor;
    }

    private void ApplyOff()
    {
        butonRT.sizeDelta = unselectButtonSizeDelta;
        iconImage.transform.localScale = unselectIconScale;
        nameText.gameObject.transform.position = new Vector3(nameText.gameObject.transform.position.x, nameText.gameObject.transform.position.y + 28f, 0f);
        nameText.color = unselectNameColor;
    }

    private IEnumerator TurnOnCoroutine()
    {
        float factor = 0f;

        while (factor < 1f)
        {
            butonRT.sizeDelta = Vector2.Lerp(unselectButtonSizeDelta, selectButtonSizeDelta, factor);
            iconImage.transform.localScale = Vector3.Lerp(unselectIconScale, selectIconScale, factor);
            nameText.color = Color.Lerp(unselectNameColor, selectNameColor, factor);

            factor += speed * Time.deltaTime;
            yield return null;
        }

        ApplyOn();

        animationCoroutine = null;

        yield break;
    }

    private IEnumerator TurnOffCoroutine()
    {
        float factor = 0f;

        while (factor < 1f)
        {
            butonRT.sizeDelta = Vector2.Lerp(selectButtonSizeDelta, unselectButtonSizeDelta, factor);
            iconImage.transform.localScale = Vector3.Lerp(selectIconScale, unselectIconScale, factor);
            nameText.color = Color.Lerp(selectNameColor, unselectNameColor, factor);

            factor += speed * Time.deltaTime;
            yield return null;
        }

        ApplyOff();

        animationCoroutine = null;

        yield break;
    }
}
