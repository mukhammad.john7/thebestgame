﻿using CodeStage.AntiCheat.ObscuredTypes;
using UnityEngine;

public static class CryptoController
{
    public static string PrefsCryptoKey
    {
        get { return ObscuredPrefs.GetString("CryptoKey"); }
        set { ObscuredPrefs.SetString("CryptoKey", value); }
    }

    public static void ApplyPrefsCryptoKey()
    {
        ObscuredPrefs.CryptoKey = "e806f6";

        if (string.IsNullOrEmpty(PrefsCryptoKey))
        {
            PrefsCryptoKey = Random.Range(int.MinValue, int.MaxValue).ToString();
            PlayerPrefs.Save();
        }

        ObscuredPrefs.CryptoKey = PrefsCryptoKey;
    }

    public static void GenerateCryptoKeys()
    {
        ObscuredDouble.SetNewCryptoKey((long)Random.Range(int.MinValue, int.MaxValue));
        ObscuredLong.SetNewCryptoKey((long)Random.Range(int.MinValue, int.MaxValue));
        ObscuredInt.SetNewCryptoKey(Random.Range(int.MinValue, int.MaxValue));
        ObscuredFloat.SetNewCryptoKey(Random.Range(int.MinValue, int.MaxValue));
        ObscuredString.SetNewCryptoKey(Random.Range(int.MinValue, int.MaxValue).ToString());
    }
}
