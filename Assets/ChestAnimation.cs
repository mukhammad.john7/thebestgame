﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChestAnimation : MonoBehaviour
{
    [SerializeField] private Button openButton;
    [SerializeField] private Animator animator;
    [SerializeField] private GameObject card;
    private int chestId;
    private void Start()
    {
        openButton.gameObject.SetActive(false);
        card.SetActive(false);
    }

    public void Init(int chestId)
    {
        this.chestId = chestId;
        gameObject.GetComponent<Image>().sprite = GameResources.GetChestSprite(chestId, 0);
    }

    public void ShowButton()
    {
        openButton.gameObject.SetActive(true);
        openButton.onClick.AddListener(StartOpenAnimation);
    }

    public void HideButton()
    {
        openButton.gameObject.SetActive(false);
        card.transform.localScale = new Vector3(0.01f, 0.01f, 1f);
        card.SetActive(false);
    }

    private void StartOpenAnimation()
    {
        openButton.onClick.RemoveAllListeners();
        HideButton();
        animator.SetBool("IsOpen", true);
    }

    public void OpenSprite()
    {
        gameObject.GetComponent<Image>().sprite = GameResources.GetChestSprite(chestId, 1);
    }

    public void StartCardAnimation()
    {
        card.SetActive(true);
    }
}
